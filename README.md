# MHS site deployment system
This provides simple interface to create, update and deploy sites.

## Requirements
* python 3.5+
* ```pip install -r requirements.txt```

## Usage

### Config generator
Config generator needs to create configuration file for new site.
```
python generate_config.py
```
It will ask you several questions about site configuration and put config file to [configs](configs) dir.


### Deployment tool
Deployment tool has three mods:
* create
* deploy

```
usage: deploy.py [-h] [-p PROFILE] [-d]
                 {create,deploy,migrate,update_ggc,update_agent,migrate_template,gen_migrations}
                 ...

positional arguments:
  {create,deploy,migrate,update_ggc,update_agent,migrate_template,gen_migrations}
                        "create", "deploy", "migrate", "update_ggc",
                        "update_agent", "migrate_template", "gen_migrations"
                        site

optional arguments:
  -h, --help            show this help message and exit
  -p PROFILE, --profile PROFILE
                        AWS profile name
  -d, --debug           print debug info
```

#### Create site
Steps:

* Read config file for site
* Create and configure grengrass group and all required dependencies (cert, core, etc.)
* Create AWS S3 bucket and events
* Create database and tables
* Upload greengrass core software and certificates to gateway
* Run grenngrass core
* Create new deployment
* Save some important data (ARNs and Ids for greengrass staff) to config file


#### Deploy site
Steps:

* Read config file for site
* Create new version of functions and resources for greengrass group
* Create new deployment


All steps are listed in [config](config.py) file.


#### RDB migrations

##### Create new migration

```
usage: deploy.py gen_migrations [-h] [-a] {customer,site} message

positional arguments:
  {customer,site}     Migration template target
  message             Message for migration

optional arguments:
  -h, --help          show this help message and exit
  -a, --autogenerate  If set then alembic will try to generate migration based
                      on the current models state. Otherwise - creates empty
                      migration.
```

Example:
```
python deploy.py gen_migrations -a site add_data_constraints
```

##### Apply migration to the template

```
usage: deploy.py migrate_template [-h] {customer,site}

positional arguments:
  {customer,site}  Migration template target

optional arguments:
  -h, --help       show this help message and exit
```

Example:
```
python deploy.py migrate_template site
```

##### Apply migration to the site

```
usage: deploy.py migrate [-h] [-r] site_name

positional arguments:
  site_name       name of site to be processed

optional arguments:
  -h, --help      show this help message and exit
  -r, --rollback  rollback on failure
```

Example:
```
python deploy.py migrate mhs-emulated
```
