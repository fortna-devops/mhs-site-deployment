from .steps_actions import CreateAction, DeployAction, CreateRDB, MigrateAction, UpdateGGCAction, UpdateAgentAction
from .migrations_actions import MigrateTemplateAction, GenerateMigrationsAction, AlembicHistoryAction


CREATE_ACTION_NAME = 'create'
DEPLOY_ACTION_NAME = 'deploy'
CREATE_RDB_ACTION_NAME = 'create_rdb'
MIGRATE_ACTION_NAME = 'migrate'
UPDATE_GGC = 'update_ggc'
UPDATE_GGC_OTA = 'update_agent'
MIGRATE_TEMPLATE = 'migrate_template'
GENERATE_MIGRATIONS = 'gen_migrations'
ALEMBIC_HISTORY_ACTION_NAME = 'alembic_history'


ACTION_STEPS_MAP = {
    CREATE_ACTION_NAME: CreateAction,
    DEPLOY_ACTION_NAME: DeployAction,
    CREATE_RDB_ACTION_NAME: CreateRDB,
    MIGRATE_ACTION_NAME: MigrateAction,
    UPDATE_GGC: UpdateGGCAction,
    UPDATE_GGC_OTA: UpdateAgentAction,
    MIGRATE_TEMPLATE: MigrateTemplateAction,
    GENERATE_MIGRATIONS: GenerateMigrationsAction,
    ALEMBIC_HISTORY_ACTION_NAME: AlembicHistoryAction,
}
