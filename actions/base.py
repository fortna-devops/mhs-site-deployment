import os
import logging
from abc import ABC, abstractmethod
from builtins import super

import boto3

from utils import Config


logger = logging.getLogger()


CONFIG_DIR = 'configs'


__all__ = ['BaseAction', 'BaseSiteAction']


class BaseAction(ABC):

    def __init__(self, profile=None):
        self.__client_cache = {}

        self._profile = profile
        self._artifacts = {}

        self._session = boto3.Session(profile_name=profile)

    @classmethod
    def init_args(cls, parser):
        pass

    @property
    def artifacts(self):
        return self._artifacts

    @property
    def boto_session(self):
        return self._session

    @property
    def aws_profile(self):
        return self._profile

    def get_client(self, service_name):
        if service_name not in self.__client_cache:
            client = self._session.client(service_name)
            self.__client_cache[service_name] = client
        return self.__client_cache[service_name]

    @abstractmethod
    def run(self):
        pass


class BaseSiteAction(BaseAction):

    def __init__(self, site_name, profile=None):
        super(BaseSiteAction, self).__init__(profile=profile)

        self._site_name = site_name
        config_path = os.path.join(CONFIG_DIR, self._site_name+'.json')
        self._config = Config.load(config_path)

    @classmethod
    def init_args(cls, parser):
        parser.add_argument('site_name', help='name of site to be processed')

    @property
    def config(self):
        return self._config
