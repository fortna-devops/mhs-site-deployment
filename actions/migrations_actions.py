import os

from alembic.config import Config as AlembicConfig
from alembic import command as alembic_command

import rdb_migrations
from .base import BaseAction


__all__ = ['MigrateTemplateAction', 'GenerateMigrationsAction', 'AlembicHistoryAction']


class BaseMigrationAction(BaseAction):
    template_options = ('customer', 'site')
    template_db_prefix = 'template_'

    username = 'developer'
    host = 'mhspredict-dev-new.c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
    port = '5432'

    def __init__(self, template, profile=None):
        super(BaseMigrationAction, self).__init__(profile=profile)
        config = AlembicConfig()

        script_location = os.path.join(os.path.dirname(rdb_migrations.__file__), template)

        if self._profile:
            config.set_main_option('profile_name', self._profile)
        config.set_main_option('script_location', script_location)
        config.set_main_option('username', self.username)
        config.set_main_option('host', self.host)
        config.set_main_option('port', self.port)
        config.set_main_option('database', f'{self.template_db_prefix}{template}')
        config.set_main_option('template', template)
        self._config = config

    @classmethod
    def init_args(cls, parser):
        parser.add_argument('template', choices=cls.template_options, help='Migration template target')


class MigrateTemplateAction(BaseMigrationAction):
    revision = 'head'

    def run(self):
        alembic_command.upgrade(self._config, self.revision)


class GenerateMigrationsAction(BaseMigrationAction):

    def __init__(self, message, autogenerate, template, profile=None):
        super(GenerateMigrationsAction, self).__init__(template=template, profile=profile)
        self._message = message
        self._autogenerate = autogenerate

    @classmethod
    def init_args(cls, parser):
        super(GenerateMigrationsAction, cls).init_args(parser)
        parser.add_argument('-a', '--autogenerate', action='store_true', default=False,
                            help='If set then alembic will try to generate migration based on the '
                                 'current models state. Otherwise - creates empty migration.')
        parser.add_argument('message', help='Message for migration')

    def run(self):
        alembic_command.revision(self._config, message=self._message, autogenerate=self._autogenerate)


class AlembicHistoryAction(BaseMigrationAction):

    def run(self):
        alembic_command.history(self._config)
