import logging
from abc import abstractmethod

from utils import StopDeploy
from .base import BaseSiteAction

from steps import greengrass, athena, general, rdb


__all__ = ['CreateAction', 'DeployAction', 'CreateRDB', 'MigrateAction', 'UpdateGGCAction', 'UpdateAgentAction']


logger = logging.getLogger()


class BaseStepsAction(BaseSiteAction):

    def __init__(self, site_name, profile=None, rollback=True):
        super(BaseStepsAction, self).__init__(site_name=site_name, profile=profile)
        self._rollback = rollback

    @classmethod
    def init_args(cls, parser):
        super(BaseStepsAction, cls).init_args(parser)
        parser.add_argument('-r', '--rollback', help='rollback on failure',
                            action='store_true', default=False, dest='rollback')

    @property
    @abstractmethod
    def _steps(self):
        return ()

    def run(self):
        processed_steps = []

        for step_cls in self._steps:
            step = step_cls(self)
            processed_steps.append(step)
            try:
                step.forwards()
            except StopDeploy as e:
                logger.error('Deployment was stopped. Reason: {}'.format(e))
                return
            except BaseException:
                if not self._rollback:
                    raise

                logger.error('An error was occurred. Rollback.')
                for step in reversed(processed_steps):
                    try:
                        step.backwards()
                    except BaseException as e:
                        logger.exception(e)
                raise

        self._config.save()


class CreateAction(BaseStepsAction):
    _steps = (
        general.ValidateCreation,
        greengrass.CreateCert,
        greengrass.CreatePolicy,
        greengrass.CreateThing,
        greengrass.CreateCore,
        greengrass.CreateLoggerDef,
        greengrass.CreateResourceDef,
        greengrass.CreateSubscriptionDef,
        greengrass.CreateFunctionDef,
        greengrass.CreateGroup,
        greengrass.AssignRole,
        greengrass.CreateLoggerVersion,
        greengrass.CreateResourceVersion,
        greengrass.CreateSubscriptionVersion,
        greengrass.CreateFunctionVersion,
        greengrass.CreateGroupVersion,
        athena.CreateS3Bucket,
        athena.CreateDB,
        athena.CreateS3Triggers,
        rdb.CreateDBStep,
        rdb.CreateSiteStep,
        rdb.MigrateCustomerStep,
        rdb.MigrateSiteStep,
        greengrass.PrepareGGC,
        greengrass.StartGGC,
        greengrass.StartOTA,
        greengrass.CreateDeploy,
    )


class DeployAction(BaseStepsAction):
    _steps = (
        greengrass.CreateLoggerVersion,
        greengrass.CreateResourceVersion,
        greengrass.CreateSubscriptionVersion,
        greengrass.CreateFunctionVersion,
        greengrass.CreateGroupVersion,
        greengrass.CreateDeploy,
    )


class CreateRDB(BaseStepsAction):
    _steps = (
        rdb.CreateDBStep,
        rdb.CreateSiteStep,
        rdb.MigrateCustomerStep,
        rdb.MigrateSiteStep,
    )


class MigrateAction(BaseStepsAction):
    _steps = (
        rdb.MigrateCustomerStep,
        rdb.MigrateSiteStep,
    )


class UpdateGGCAction(BaseStepsAction):
    _steps = (
        greengrass.UpdateGGC,
    )


class UpdateAgentAction(BaseStepsAction):
    _steps = (
        greengrass.UpdateAgent,
    )
