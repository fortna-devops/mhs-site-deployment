import logging
import os
from copy import deepcopy

import jinja2


logger = logging.getLogger()


class Generator(object):

    def __init__(self, questions, template_dir='templates',
                 template_file='config.jinja2', output_dir='configs'):
        self._questions = questions
        self._template_dir = template_dir
        self._template_file = template_file
        self._output_dir = output_dir
        self._data = {}

    @property
    def data(self):
        return deepcopy(self._data)

    def run(self):
        for question in self._questions:
            if not question.prepare(**self._data):
                continue

            dest, value = question.ask(**self._data)
            self._data[dest] = value

    def render(self, file_name, **extra):
        logger.info('Render config for "{}" by template'.format(file_name))
        data = dict(self._data, **extra)
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(searchpath=self._template_dir),
            trim_blocks=True, lstrip_blocks=True)
        template = env.get_template(self._template_file)

        if not os.path.exists(self._output_dir):
            os.mkdir(self._output_dir)

        result = template.render(**data)

        result_path = os.path.join(self._output_dir, file_name+'.json')
        with open(result_path, 'w') as f:
            f.write(result)
