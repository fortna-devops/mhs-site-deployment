from abc import ABC, abstractmethod
from distutils.util import strtobool


class ValidationError(Exception):
    pass


class DependedDefault(object):

    def __init__(self, depends_on, prefix='', suffix='', func=None):
        self._depends_on = depends_on
        self._prefix = prefix
        self._suffix = suffix
        self._func = func

    def get_default(self, **ctx):
        value = ctx.get(self._depends_on)
        if self._func:
            value = self._func(value)
        return '{}{}{}'.format(self._prefix, value, self._suffix)


class BaseQuestion(ABC):

    def __init__(self, depends_on=None):
        if depends_on is None:
            self._depends_on = []
        elif not isinstance(depends_on, list):
            self._depends_on = [depends_on]
        else:
            self._depends_on = depends_on

    def prepare(self, **ctx):
        return all([ctx.get(d) for d in self._depends_on])

    @abstractmethod
    def ask(self, **ctx):
        return None, None


class Question(BaseQuestion):

    def __init__(self, dest, help=None, dtype=str, required=True, default=None,
                 choices=None, min=None, max=None, depends_on=None,
                 input_func=input):
        super(Question, self).__init__(depends_on=depends_on)
        self._dest = dest
        self._help = help
        self._dtype = dtype
        self._required = required
        self._default = default
        self._choices = choices
        self._min = min
        self._max = max
        self._input_func = input_func

    def _get_help(self):
        if self._choices:
            choices_help = ' ({})'.format(', '.join(self._choices))
        else:
            choices_help = ''

        if self._default is not None:
            default_help = ' [{}]'.format(self._default)
        else:
            default_help = ''

        return '{}{}{}: '.format(self._help, choices_help, default_help)

    def _validate(self, value):
        # Check default value
        if self._default is not None and not value:
            value = self._default

        # Check if required
        if self._required and not value:
            raise ValidationError('This parameter is required')

        # Check if match choices
        if self._choices and value not in self._choices:
            msg = 'Your answer is not valid. Select one of the next: {}'\
                .format(', '.join(self._choices))
            raise ValidationError(msg)

        # Check if match data type
        try:
            value = self._dtype(value)
        except (TypeError, ValueError) as e:
            raise ValidationError(e)

        # Check min
        if self._min and value < self._min:
            raise ValidationError('Value should be grater than {}'.format(self._min))

        # Check max
        if self._max and value > self._max:
            raise ValidationError('Value should be less than {}'.format(self._max))

        return value

    def prepare(self, **ctx):
        if isinstance(self._default, DependedDefault):
            self._default = self._default.get_default(**ctx)

        return super(Question, self).prepare(**ctx)

    def ask(self, **ctx):
        help = self._get_help()
        while True:
            value = self._input_func(help)

            try:
                value = self._validate(value)
            except ValidationError as e:
                print(str(e))
                continue

            return self._dest, value


class DictQuestion(BaseQuestion):

    def __init__(self, dest, help, questions, depends_on=None):
        super(DictQuestion, self).__init__(depends_on=depends_on)
        self._dest = dest
        self._questions = questions
        self._help = help

    def ask(self, **ctx):
        if self._help:
            print(self._help)

        value = {}

        for question in self._questions:
            dest, _value = question.ask(**value)
            value[dest] = _value

        return self._dest, value


class DynamicDictQuestion(DictQuestion):

    def __init__(self, dest, help, lookup_field, map, depends_on=None):
        super(DynamicDictQuestion, self).__init__(
            dest=dest, help=help, questions=[], depends_on=depends_on)

        self._lookup_field = lookup_field
        self._map = map

    def ask(self, **ctx):
        self._questions = self._map[ctx[self._lookup_field]]
        return super(DynamicDictQuestion, self).ask(**ctx)


class MapQuestion(BaseQuestion):

    def __init__(self, dest, help, more_help, key_question, value_question,
                 limit=None, depends_on=None):
        super(MapQuestion, self).__init__(depends_on=depends_on)

        self._dest = dest
        self._key_question = key_question
        self._value_question = value_question
        self._help = help

        self._limit = limit
        self._more_question = Question('more', more_help, choices=('y', 'n'),
                                       dtype=strtobool, default='n')

    def ask(self, **ctx):
        if self._help:
            print(self._help)

        value = {}

        counter = 0
        while True:
            _, key_ans = self._key_question.ask(**value)
            _, value_ans = self._value_question.ask(**value)
            value[key_ans] = value_ans
            counter += 1

            if self._limit:
                if counter < self._limit:
                    continue
                else:
                    break

            _, more = self._more_question.ask(**value)
            if not more:
                break

        return self._dest, value


class ListQuestion(BaseQuestion):

    def __init__(self, dest, help, more_help, question, limit=None,
                 depends_on=None):
        super(ListQuestion, self).__init__(depends_on=depends_on)
        self._dest = dest
        self._help = help
        self._question = question
        self._limit = limit
        self._more_question = Question('more', more_help, choices=('y', 'n'),
                                       dtype=strtobool, default='n')

    def ask(self, **ctx):
        if self._help:
            print(self._help)

        value = []

        counter = 0
        while True:
            _, _value = self._question.ask(**ctx)
            value.append(_value)
            counter += 1

            if self._limit:
                if counter < self._limit:
                    continue
                else:
                    break

            _, more = self._more_question.ask(**ctx)
            if not more:
                break

        return self._dest, value


class CSVQuestion(Question):

    def __init__(self, dest, help, dtype=str, delimiter=',', required=True,
                 default=None, depends_on=None):
        super(CSVQuestion, self).__init__(
            dest=dest, help=help, dtype=self._csv_dtype, required=required,
            default=default, depends_on=depends_on
        )
        self._el_dtype = dtype
        self._delimiter = delimiter

    def _csv_dtype(self, value):
        return [self._el_dtype(i.strip()) for i in value.split(self._delimiter)]
