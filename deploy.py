#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logging
from functools import partial

import actions


def run(cls, **kwargs):
    cls(**kwargs).run()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    quoted_actions = [f'"{a}"' for a in actions.ACTION_STEPS_MAP.keys()]
    subparsers = parser.add_subparsers(dest='action_name', help=f'{", ".join(quoted_actions)} site')
    subparsers.required = True

    for action_name, action_cls in actions.ACTION_STEPS_MAP.items():
        action_group = subparsers.add_parser(action_name)
        action_group.set_defaults(func=partial(run, cls=action_cls))
        action_cls.init_args(action_group)

    parser.add_argument('-p', '--profile', default=None, help='AWS profile name')
    parser.add_argument('-d', '--debug', action="store_const", dest="log_level",
                        const=logging.DEBUG, default=logging.INFO, help="print debug info")

    args = parser.parse_args()
    args = vars(args)
    del args['action_name']

    log_level = args.pop('log_level')
    logging.basicConfig(
        level=log_level,
        format='[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
    )
    args.pop('func')(**args)
