#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import uuid
import re
from getpass import getpass

from config_generator.question import Question, MapQuestion, DictQuestion, \
    DynamicDictQuestion, ListQuestion, CSVQuestion, DependedDefault
from config_generator.generator import Generator


logger = logging.getLogger()


BAUDRATES = (50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800,
             9600, 19200, 38400, 57600, 115200, 230400, 460800, 500000,
             576000, 921600, 1000000, 1152000, 1500000, 2000000, 2500000,
             3000000, 3500000, 4000000)

BAUDRATE_CHOICES = tuple(str(i) for i in BAUDRATES)


DEVICE_TYPES_QUESTION_MAP = {
    'qm42vt2': [
        Question('timeout', 'Enter modbus timeout', dtype=float, default='0.1', min=0.1),
        Question('baudrate', 'Enter baudrate', dtype=int, default='19200', choices=BAUDRATE_CHOICES)
    ],
    'm12fth3q': [
        Question('timeout', 'Enter modbus timeout', dtype=float, default='0.1', min=0.1),
        Question('baudrate', 'Enter baudrate', dtype=int, default='19200', choices=BAUDRATE_CHOICES)
    ],
    'qm42vt2_tcp': [
        Question('server_host', 'Enter server host', default='192.168.1.11'),
        Question('server_port', 'Enter server port', dtype=int, default='502'),
        Question('timeout', 'Enter socket timeout', dtype=float, default='30', min=0.1),
    ],
    'm12fth3q_tcp': [
        Question('server_host', 'Enter server host', default='192.168.1.11'),
        Question('server_port', 'Enter server port', dtype=int, default='502'),
        Question('timeout', 'Enter socket timeout', dtype=float, default='30', min=0.1),
    ],
    'csv_plc': [
        Question('plc_host', 'Enter PLC address', default='192.168.1.5'),
        Question('plc_port', 'Enter PLC port', dtype=int, default='44818'),
        ListQuestion('route_path', 'Configure route path', '',
                     DictQuestion('route_path', None, [
                         Question('link', 'Enter link', dtype=int, default='1'),
                         Question('port', 'Enter port', dtype=int, default='2')
                     ]), limit=1),
    ],
    'qm42vt2_emulated': [
        Question('error_rate', 'Enter error rate for emulation', dtype=float, min=0, max=1, default='0.01')
    ],
    'm12fth3q_emulated': [
        Question('error_rate', 'Enter error rate for emulation', dtype=float, min=0, max=1, default='0.01')
    ],
    'csv_plc_emulated': [
        Question('error_rate', 'Enter error rate for emulation', dtype=float, min=0, max=1, default='0.01')
    ],
}

AVAILABLE_DEVICE_TYPES = DEVICE_TYPES_QUESTION_MAP.keys()


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format='[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
    )

    questions = [
        Question('site_name', 'Enter site name'),
        Question('customer_name', 'Enter customer name', depends_on='site_name', default=DependedDefault('site_name', func=lambda v: v.split('-')[0])),
        Question('gateway_name', 'Enter gateway name', depends_on='site_name', default=DependedDefault('site_name')),
        Question('bucket_name', 'Enter S3 bucket name for data', depends_on='site_name', default=DependedDefault('site_name', prefix='mhspredict-site-')),
        Question('greengrass_lambda_qualifier', 'Enter lambdas qualifier for greengrass', default='master'),
        Question('etl_lambda_qualifier', 'Enter lambdas qualifier for etl (downsampling)', default='master'),
        Question('role_arn', 'Enter greengrass role arn', default='arn:aws:iam::286214959794:role/AWSGreengrassAccessRole'),
        Question('logger_size', 'Enter greengrass logger size in KB', default='1000000', dtype=int),
        MapQuestion('device_schema', 'Configure port definition', 'Do you want to add more port definitions?',
                    Question('port', 'Enter port name ("enip" for PLC, "/dev/tty..." for serial, "modbus_tcp" for ModBus over TCP)'),
                    ListQuestion('devices', 'Configure devices', 'Do you want to add more devices to this port?',
                                 DictQuestion('devices', '', [
                                     Question('type', 'Enter type', choices=AVAILABLE_DEVICE_TYPES),
                                     Question('interval', 'Enter read interval (in seconds)', dtype=float, min=0.1),
                                     DynamicDictQuestion('options', 'Configure device options', 'type', DEVICE_TYPES_QUESTION_MAP),
                                     CSVQuestion('devices', 'Enter comma-separated list of device addresses')
                                 ]))
                    ),
        DictQuestion('gateway_connection', 'Configure gateway ssh connection', [
            Question('host', 'Enter hostname'),
            Question('port', 'Enter port', default='22', dtype=int),
            Question('user', 'Enter username'),
            Question('pwd', 'Enter password (if exists)', input_func=getpass, required=False),
            Question('key_path', 'Enter ssh key path (if exists)', required=False),
        ]),
        Question('ggc_path', 'Enter path to greengrass core software', default='/opt/greengrass')
    ]

    gen = Generator(questions)
    gen.run()
    extra = {
        'uuid': str(uuid.uuid4()),
        'tty_ports': [p for p in gen.data['device_schema'].keys() if re.match(r'^/dev/tty*', p)],
        'etl_tables': ('sensor', 'plc', 'belt_camera', 'ambient_env'),
    }
    gen.render(gen.data['site_name'], **extra)
