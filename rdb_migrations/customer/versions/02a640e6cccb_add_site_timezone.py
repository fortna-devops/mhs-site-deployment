"""add_site_timezone

Revision ID: 02a640e6cccb
Revises: 9307f34a4ee1
Create Date: 2021-02-09 17:23:36.149242

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '02a640e6cccb'
down_revision = '9307f34a4ee1'
branch_labels = None
depends_on = None

SITE_TZ_MAPPING = {
    'msh-emulated': 'America/New_York',
    'ups-independence': 'America/Chicago',
    'amazon-sdf4': 'America/New_York',
    'fedex-louisville': 'America/New_York',
    'dhl-miami': 'America/New_York',
    'dhl-milano': 'Europe/Rome',
    'dhl-brescia': 'Europe/Rome',
    'dhl-brno': 'Europe/Prague',
}


def upgrade(schema):
    op.add_column('customer_sites', sa.Column('timezone', sa.String(), server_default='Etc/UTC', nullable=False))

    connection = op.get_bind()
    meta = sa.MetaData(bind=connection, schema=schema)
    meta.reflect(only=(
        'customer_sites',
    ))
    customer_sites_table = sa.Table('customer_sites', meta)
    for site_name, timezone in SITE_TZ_MAPPING.items():
        connection.execute(
            sa.update(customer_sites_table).where(customer_sites_table.c.name == site_name).values(timezone=timezone)
        )


def downgrade(schema):
    op.drop_column('customer_sites', 'timezone')
