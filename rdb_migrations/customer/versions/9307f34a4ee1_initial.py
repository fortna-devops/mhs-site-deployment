"""initialll

Revision ID: 9307f34a4ee1
Revises: 
Create Date: 2019-09-10 13:36:46.437809

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9307f34a4ee1'
down_revision = None
branch_labels = None
depends_on = None


def upgrade(schema):
    op.execute('ALTER TABLE alembic_version OWNER to mhs')

    op.create_table(
        'customer_sites',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('nation', sa.String(), nullable=True),
        sa.Column('region', sa.String(), nullable=True),
        sa.Column('facility_name', sa.String(), nullable=False),
        sa.Column('schema_name', sa.String(), nullable=False),
        sa.Column('latitude', sa.Float(), nullable=True),
        sa.Column('longitude', sa.Float(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('name'),
        sa.UniqueConstraint('schema_name')
    )
    op.execute('ALTER TABLE customer_sites OWNER to mhs')


def downgrade(schema):
    op.drop_table('customer_sites')
