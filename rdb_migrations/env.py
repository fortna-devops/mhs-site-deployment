import boto3
from alembic import context
from mhs_rdb.models.base_model import BaseSiteModel, BaseCustomerModel
from mhs_rdb import Connector


config = context.config

template = config.get_main_option('template')

METADATA_MAP = {
    'customer': BaseCustomerModel.metadata,
    'site': BaseSiteModel.metadata
}

target_metadata = METADATA_MAP[template]


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """

    profile_name = config.get_main_option('profile_name')
    host = config.get_main_option('host')
    port = config.get_main_option('port')
    username = config.get_main_option('username')
    database = config.get_main_option('database')
    schema = config.get_main_option('schema')

    connector = Connector(
        boto_session=boto3.Session(profile_name=profile_name),
        host=host,
        port=port,
        username=username
    )

    engine = connector.get_engine(database)

    with engine.connect() as connection:

        if schema:
            connection.execute(f'SET search_path TO {schema}')
        context.configure(
            connection=connection,
            target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations(schema=schema)


run_migrations_online()
