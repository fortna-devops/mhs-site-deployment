"""add_threshold_levels_color

Revision ID: 087460a1dcfd
Revises: 851157e8f27d
Create Date: 2020-11-23 15:02:30.949478

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM


# revision identifiers, used by Alembic.
revision = '087460a1dcfd'
down_revision = '851157e8f27d'
branch_labels = None
depends_on = None


def upgrade(schema):
    colors_enum = ENUM(
        'UNDEFINED',
        'GREEN',
        'YELLOW',
        'RED',
        name='colors_enum',
        create_type=False
    )
    colors_enum.create(op.get_bind())
    op.execute('ALTER TYPE colors_enum OWNER to mhs')

    op.add_column('threshold_levels', sa.Column('color', colors_enum, nullable=True))

    colors = {
        'red': 'RED',
        'orange': 'YELLOW',
        'low_red': 'RED',
        'low_orange': 'YELLOW',
        'high_red': 'RED',
        'high_orange': 'YELLOW'
    }

    names = ','.join([f"'{name}'" for name in colors.keys()])
    op.execute(f"DELETE FROM threshold_levels WHERE name NOT IN ({names})")

    for name, color in colors.items():
        op.execute(f"UPDATE threshold_levels SET color = '{color}' WHERE name = '{name}'")

    op.alter_column('threshold_levels', 'color', nullable=False)


def downgrade(schema):
    raise Exception('Irreversible migration')
