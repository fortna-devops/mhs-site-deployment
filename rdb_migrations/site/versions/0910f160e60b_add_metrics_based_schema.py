"""add_metrics_based_schema

Revision ID: 0910f160e60b
Revises: ac535bcfe12f
Create Date: 2020-08-25 16:41:47.891265

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM


# revision identifiers, used by Alembic.
revision = '0910f160e60b'
down_revision = 'ac535bcfe12f'
branch_labels = None
depends_on = None


def upgrade(schema):
    spare_parts_confidence_level_enum = ENUM(
        'low_risk',
        'medium_risk',
        'high_risk',
        name='spare_parts_confidence_level_enum',
        create_type=False
    )
    conveyor_statuses_enum = ENUM(
        'ON',
        'OFF',
        'INVALID',
        name='conveyor_statuses_enum',
        create_type=False
    )

    op.create_table(
        'alarm_types',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('name')
    )
    op.execute('ALTER TABLE alarm_types OWNER to mhs')

    op.create_table(
        'asset_types',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('manufacturer', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('name')
    )
    op.execute('ALTER TABLE asset_types OWNER to mhs')

    op.create_table(
        'component_types',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('manufacturer', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('name')
    )
    op.execute('ALTER TABLE component_types OWNER to mhs')

    op.create_table(
        'data_source_types',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('manufacturer', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('name')
    )
    op.execute('ALTER TABLE data_source_types OWNER to mhs')

    op.create_table(
        'facility_areas',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE facility_areas OWNER to mhs')

    op.create_table(
        'new_gateways',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('cognito_client_id', sa.String(), nullable=True),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE new_gateways OWNER to mhs')

    op.create_table(
        'threshold_levels',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('name')
    )
    op.execute('ALTER TABLE threshold_levels OWNER to mhs')

    op.create_table(
        'threshold_types',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('name')
    )
    op.execute('ALTER TABLE threshold_types OWNER to mhs')

    op.create_table(
        'assets',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('facility_area_id', sa.Integer(), nullable=False),
        sa.Column('type_name', sa.String(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('install_date', sa.DateTime(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['facility_area_id'], ['facility_areas.id'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['type_name'], ['asset_types.name'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE assets OWNER to mhs')

    op.create_table(
        'new_spare_parts_results',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('mean_time_between_failure', sa.Integer(), nullable=False),
        sa.Column('operating_time_of_interest', sa.String(), nullable=False),
        sa.Column('num_units', sa.Integer(), nullable=False),
        sa.Column('confidence_level', spare_parts_confidence_level_enum, nullable=False),
        sa.Column('component_type_name', sa.String(), nullable=False),
        sa.Column('model', sa.String(), nullable=True),
        sa.Column('manufacture', sa.String(), nullable=True),
        sa.Column('part_costs', sa.Integer(), nullable=True),
        sa.Column('user_name', sa.String(), nullable=True),
        sa.Column('result', sa.JSON(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['component_type_name'], ['component_types.name'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE new_spare_parts_results OWNER to mhs')

    op.create_table(
        'asset_statuses',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('asset_id', sa.Integer(), nullable=False),
        sa.Column('status', conveyor_statuses_enum, nullable=True),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['asset_id'], ['assets.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE asset_statuses OWNER to mhs')

    op.create_table(
        'components',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('asset_id', sa.Integer(), nullable=False),
        sa.Column('type_name', sa.String(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('install_date', sa.DateTime(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['asset_id'], ['assets.id'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['type_name'], ['component_types.name'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE components OWNER to mhs')

    op.create_table(
        'component_health',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('component_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('health', sa.Float(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['component_id'], ['components.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('component_id', 'timestamp')
    )
    op.execute('ALTER TABLE component_health OWNER to mhs')

    op.create_table(
        'data_sources',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('component_id', sa.Integer(), nullable=False),
        sa.Column('type_name', sa.String(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('location', sa.String(), nullable=True),
        sa.Column('install_date', sa.DateTime(), nullable=True),
        sa.Column('address', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['component_id'], ['components.id'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['type_name'], ['data_source_types.name'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE data_sources OWNER to mhs')

    op.create_table(
        'data_sources_gateways_association',
        sa.Column('data_source_id', sa.Integer(), nullable=False),
        sa.Column('gateway_id', sa.Integer(), nullable=False),
        sa.Column('address', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['data_source_id'], ['data_sources.id'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['gateway_id'], ['new_gateways.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('data_source_id', 'gateway_id')
    )
    op.execute('ALTER TABLE data_sources_gateways_association OWNER to mhs')

    op.create_table(
        'metrics',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('data_source_id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('display_name', sa.String(), nullable=False),
        sa.Column('units', sa.String(), nullable=True),
        sa.Column('min_value', sa.Float(), nullable=True),
        sa.Column('max_value', sa.Float(), nullable=True),
        sa.Column('data_table', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['data_source_id'], ['data_sources.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE metrics OWNER to mhs')

    op.create_table(
        'new_alert_black_list',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('alert_user_email', sa.String(), nullable=False),
        sa.Column('data_source_id', sa.Integer(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['alert_user_email'], ['alert_users.email'], ),
        sa.ForeignKeyConstraint(['data_source_id'], ['data_sources.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE new_alert_black_list OWNER to mhs')

    op.create_table(
        'calculated_float_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.REAL(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE calculated_float_data OWNER to mhs')

    op.create_table(
        'calculated_int_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.Integer(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE calculated_int_data OWNER to mhs')

    op.create_table(
        'calculated_string_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE calculated_string_data OWNER to mhs')

    op.create_table(
        'new_alarms',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('data_timestamp', sa.DateTime(), nullable=False),
        sa.Column('severity', sa.Integer(), nullable=False),
        sa.Column('type_name', sa.String(), nullable=False),
        sa.Column('value', sa.Float(), nullable=False),
        sa.Column('criteria', sa.Float(), nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['type_name'], ['alarm_types.name'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('new_alarms_metric_id_type_name_index', 'new_alarms',
                    ['metric_id', 'type_name'], unique=True)
    op.execute('ALTER TABLE new_alarms OWNER to mhs')

    op.create_table(
        'new_thresholds',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('type_name', sa.String(), nullable=False),
        sa.Column('level_name', sa.String(), nullable=False),
        sa.Column('value', sa.Float(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['level_name'], ['threshold_levels.name'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['type_name'], ['threshold_types.name'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE new_thresholds OWNER to mhs')

    op.create_table(
        'raw_float_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.REAL(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE raw_float_data OWNER to mhs')

    op.create_table(
        'raw_int_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.Integer(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE raw_int_data OWNER to mhs')

    op.create_table(
        'raw_string_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE raw_string_data OWNER to mhs')

    op.create_table(
        'new_acknowledgments',
        sa.Column('alarm_id', sa.Integer(), nullable=False),
        sa.Column('work_order_lead', sa.String(), nullable=True),
        sa.Column('work_order', sa.String(), nullable=True),
        sa.Column('downtime_schedule', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['alarm_id'], ['new_alarms.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('alarm_id')
    )
    op.execute('ALTER TABLE new_acknowledgments OWNER to mhs')

    op.create_table(
        'new_alarm_histories',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('alarm_id', sa.Integer(), nullable=False),
        sa.Column('data_timestamp', sa.DateTime(), nullable=False),
        sa.Column('severity', sa.Integer(), nullable=False),
        sa.Column('value', sa.Float(), nullable=False),
        sa.Column('criteria', sa.Float(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['alarm_id'], ['new_alarms.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('alarm_id', 'data_timestamp')
    )
    op.execute('ALTER TABLE new_alarm_histories OWNER to mhs')


def downgrade(schema):
    op.drop_table('new_alarm_histories')
    op.drop_table('new_acknowledgments')
    op.drop_table('raw_string_data')
    op.drop_table('raw_int_data')
    op.drop_table('raw_float_data')
    op.drop_table('new_thresholds')
    op.drop_index('new_alarms_metric_id_type_name_index', table_name='new_alarms')
    op.drop_table('new_alarms')
    op.drop_table('calculated_string_data')
    op.drop_table('calculated_int_data')
    op.drop_table('calculated_float_data')
    op.drop_table('new_alert_black_list')
    op.drop_table('metrics')
    op.drop_table('data_sources_gateways_association')
    op.drop_table('data_sources')
    op.drop_table('component_health')
    op.drop_table('components')
    op.drop_table('asset_statuses')
    op.drop_table('new_spare_parts_results')
    op.drop_table('assets')
    op.drop_table('threshold_types')
    op.drop_table('threshold_levels')
    op.drop_table('new_gateways')
    op.drop_table('facility_areas')
    op.drop_table('data_source_types')
    op.drop_table('component_types')
    op.drop_table('asset_types')
    op.drop_table('alarm_types')
