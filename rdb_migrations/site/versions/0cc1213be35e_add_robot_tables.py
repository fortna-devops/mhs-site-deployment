"""add_robot_tables

Revision ID: 0cc1213be35e
Revises: e06413472826
Create Date: 2020-02-06 15:54:01.377631

"""
from alembic import op
import sqlalchemy as sa

from sqlalchemy.dialects.postgresql import ENUM


# revision identifiers, used by Alembic.
revision = '0cc1213be35e'
down_revision = 'e06413472826'
branch_labels = None
depends_on = None


def upgrade(schema):

    # Create job_state_enum.
    job_state_enum = ENUM(
        'done',
        'failed',
        create_type=False,
        name='job_state_enum'
    )
    job_state_enum.create(op.get_bind())
    op.execute('ALTER TYPE job_state_enum OWNER to mhs')

    # Create action_type_enum.
    action_type_enum = ENUM(
        'charging',
        'cart_docking',
        'charger_docking',
        'staging_move',
        'other_move',
        'cart_placement',
        'relative_move',
        'move_to_entry',
        'move_to_position',
        'raise_lift',
        'lower_lift',
        create_type=False,
        name='action_type_enum'
    )
    action_type_enum.create(op.get_bind())
    op.execute('ALTER TYPE action_type_enum OWNER to mhs')

    # Create robot_missions model.
    op.create_table(
        'robot_missions',
        sa.Column('mission_name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.PrimaryKeyConstraint('mission_name')
    )
    op.execute('ALTER TABLE robot_missions OWNER to mhs')

    # Create robot_jobs table.
    op.create_table(
        'robot_jobs',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('robot_id', sa.Integer(), nullable=False),
        sa.Column('robot_job_id', sa.Integer(), nullable=False),
        sa.Column('mission_name', sa.String(), nullable=False),
        sa.Column('mission_guid', sa.String(), nullable=True),
        sa.Column('message', sa.Text(), nullable=True),
        sa.Column('state', job_state_enum, nullable=False),
        sa.Column('ordered_time', sa.DateTime(), nullable=False),
        sa.Column('start_time', sa.DateTime(), nullable=False),
        sa.Column('finish_time', sa.DateTime(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.ForeignKeyConstraint(['mission_name'], ['robot_missions.mission_name'], ),
        sa.ForeignKeyConstraint(['robot_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('robot_job_id', 'robot_id')
    )
    op.execute('ALTER TABLE robot_jobs OWNER to mhs')

    # Create robot_actions table.
    op.create_table(
        'robot_actions',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('job_id', sa.Integer(), nullable=False),
        sa.Column('action_type', action_type_enum, nullable=False),
        sa.Column('start_time', sa.DateTime(), nullable=False),
        sa.Column('finish_time', sa.DateTime(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.ForeignKeyConstraint(['job_id'], ['robot_jobs.id'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('job_id', 'action_type', 'start_time', 'finish_time')
    )
    op.execute('ALTER TABLE robot_actions OWNER to mhs')

    # Add MiR500 to equipment_enum.
    with op.get_context().autocommit_block():
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'MiR500'")


def downgrade(schema):
    op.drop_table('robot_actions')
    op.drop_table('robot_jobs')
    op.drop_table('robot_missions')

    op.execute('DROP TYPE job_state_enum')
    op.execute('DROP TYPE action_type_enum')
