"""add_new_new_alarms

Revision ID: 0e6ed8d9c25a
Revises: 087460a1dcfd
Create Date: 2020-12-09 13:59:07.871069

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


# revision identifiers, used by Alembic.
revision = '0e6ed8d9c25a'
down_revision = '087460a1dcfd'
branch_labels = None
depends_on = None


def upgrade(schema):
    connection = op.get_bind()
    meta = sa.MetaData(bind=connection, schema=schema)
    colors_enum = postgresql.ENUM(name='colors_enum', create_type=False, metadata=meta)

    op.create_table(
        'alarm_configs',
        sa.Column('to_display', sa.Boolean(), server_default='TRUE', nullable=False),
        sa.Column('type_name', sa.String(), nullable=False),
        sa.Column('color', colors_enum, nullable=False),
        sa.Column('weight', sa.Integer(), server_default='1', nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.ForeignKeyConstraint(['type_name'], ['alarm_types.name'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('type_name', 'color')
    )
    op.execute('ALTER TABLE alarm_configs OWNER to mhs')

    op.create_table(
        'new_new_alarms',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('started_at', sa.DateTime(), nullable=False),
        sa.Column('ended_at', sa.DateTime(), nullable=True),
        sa.Column('type_name', sa.String(), nullable=False),
        sa.Column('color', colors_enum, nullable=False),
        sa.Column('trigger_value', sa.Float(), nullable=False),
        sa.Column('trigger_criteria', sa.Float(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['type_name', 'color'], ['alarm_configs.type_name', 'alarm_configs.color'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE new_new_alarms OWNER to mhs')


def downgrade(schema):
    op.drop_table('new_new_alarms')
    op.drop_table('alarm_configs')
