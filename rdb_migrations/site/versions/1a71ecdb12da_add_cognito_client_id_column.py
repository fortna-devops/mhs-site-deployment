"""add_cognito_client_id_column

Revision ID: 1a71ecdb12da
Revises: a10628b3cc8d
Create Date: 2020-08-03 13:10:43.403695

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1a71ecdb12da'
down_revision = 'a10628b3cc8d'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.add_column('gateways', sa.Column('cognito_client_id', sa.String(), nullable=True))


def downgrade(schema):
    raise Exception('Irreversible migration')

