"""initial

Revision ID: 1c3c9400818f
Revises: 
Create Date: 2019-09-06 12:02:01.961862

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM


# revision identifiers, used by Alembic.
revision = '1c3c9400818f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade(schema):
    op.execute('ALTER TABLE alembic_version OWNER to mhs')

    # Using postgresql ENUM here explicitly to prevent multiple type creation (create_type=False)
    levels_enum = ENUM(
        'facility',
        'facility_area',
        'conveyor',
        'equipment',
        name='levels_enum',
        create_type=False
    )
    levels_enum.create(op.get_bind())
    op.execute('ALTER TYPE levels_enum OWNER to mhs')

    op.create_table(
        'conveyors',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE conveyors OWNER to mhs')

    op.create_table(
        'gateways',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('name')
    )
    op.execute('ALTER TABLE gateways OWNER to mhs')

    op.create_table(
        'sensors',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('gateway_name', sa.String(), nullable=False),
        sa.Column('conveyor_id', sa.Integer(), nullable=True),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('manufacturer', sa.String(), nullable=False),
        sa.Column('location', sa.String(), nullable=True),
        sa.Column('equipment', sa.Enum('M', 'G', 'B', 'PLC', name='equipment_enum'), nullable=True),
        sa.Column('m_sensor', sa.String(), nullable=True),
        sa.Column('standards_score', sa.Integer(), nullable=False),
        sa.Column('facility_organization', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['conveyor_id'], ['conveyors.id'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['gateway_name'], ['gateways.name'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('gateway_name', 'name')
    )
    op.execute('ALTER TABLE sensors OWNER to mhs')
    op.execute('ALTER TYPE equipment_enum OWNER to mhs')

    op.create_table(
        'stats',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('temperature_mean', sa.Float(), nullable=False),
        sa.Column('temperature_perc95', sa.Float(), nullable=False),
        sa.Column('rms_velocity_z_mean', sa.Float(), nullable=False),
        sa.Column('rms_velocity_z_perc95', sa.Float(), nullable=False),
        sa.Column('hf_rms_acceleration_x_mean', sa.Float(), nullable=False),
        sa.Column('hf_rms_acceleration_x_perc95', sa.Float(), nullable=False),
        sa.Column('hf_rms_acceleration_z_mean', sa.Float(), nullable=False),
        sa.Column('hf_rms_acceleration_z_perc95', sa.Float(), nullable=False),
        sa.Column('rms_velocity_x_mean', sa.Float(), nullable=False),
        sa.Column('rms_velocity_x_perc95', sa.Float(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE stats OWNER to mhs')

    op.create_table(
        'thresholds',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('type', sa.Enum('ISO', 'RESIDUAL', name='thresholds_type_enum'), nullable=False),
        sa.Column('metric', sa.Enum('rms_velocity_x', 'rms_velocity_z', 'hf_rms_acceleration_x', 'hf_rms_acceleration_z', 'temperature', name='thresholds_metric_enum'), nullable=False),
        sa.Column('level', levels_enum, nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('value', sa.Float(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE thresholds OWNER to mhs')
    op.execute('ALTER TYPE thresholds_type_enum OWNER to mhs')
    op.execute('ALTER TYPE thresholds_metric_enum OWNER to mhs')

    op.create_table(
        'alert_users',
        sa.Column('email', sa.String(), nullable=False),
        sa.Column('phone', sa.String(), nullable=True),
        sa.Column('active', sa.Boolean(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('email')
    )
    op.execute('ALTER TABLE alert_users OWNER to mhs')

    op.create_table(
        'alert_roles',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('role', sa.Enum('developer', 'support', 'customer', name='alert_roles_enum'), nullable=False),
        sa.Column('alert_user_email', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['alert_user_email'], ['alert_users.email'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('alert_user_email', 'role')
    )
    op.execute('ALTER TABLE alert_roles OWNER to mhs')
    op.execute('ALTER TYPE alert_roles_enum OWNER to mhs')

    op.create_table(
        'conveyor_statuses',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('conveyor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('status', sa.Enum('ON', 'OFF', 'INVALID', name='conveyor_statuses_enum'), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['conveyor_id'], ['conveyors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE conveyor_statuses OWNER to mhs')
    op.execute('ALTER TYPE conveyor_statuses_enum OWNER to mhs')

    op.create_table(
        'mode_transition_detections',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('conveyor_id', sa.Integer(), nullable=False),
        sa.Column('transitions', sa.Integer(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['conveyor_id'], ['conveyors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE mode_transition_detections OWNER to mhs')

    op.create_table(
        'alarms',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('data_timestamp', sa.DateTime(), nullable=False),
        sa.Column('severity', sa.Integer(), nullable=False),
        sa.Column('type', sa.Enum('ISO', 'ML', 'MTD', name='alarms_type_enum'), nullable=True),
        sa.Column('metric', sa.Enum('rms_velocity_x', 'rms_velocity_z', 'temperature', 'transition_number', name='alarms_metric_enum'), nullable=True),
        sa.Column('level', levels_enum, nullable=False),
        sa.Column('value', sa.Float(), nullable=False),
        sa.Column('criteria', sa.Float(), nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE alarms OWNER to mhs')
    op.execute('ALTER TYPE alarms_type_enum OWNER to mhs')
    op.execute('ALTER TYPE alarms_metric_enum OWNER to mhs')

    op.create_table(
        'alert_black_list',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('alert_user_email', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['alert_user_email'], ['alert_users.email'], ),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE alert_black_list OWNER to mhs')

    op.create_table(
        'acknowledgments',
        sa.Column('alarm_id', sa.Integer(), nullable=False),
        sa.Column('work_order_lead', sa.String(), nullable=True),
        sa.Column('work_order', sa.String(), nullable=True),
        sa.Column('downtime_schedule', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['alarm_id'], ['alarms.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('alarm_id'),
    )
    op.execute('ALTER TABLE acknowledgments OWNER to mhs')

    op.create_table(
        'ambient_env_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('port', sa.String(), nullable=False),
        sa.Column('humidity', sa.Float(), nullable=True),
        sa.Column('temperature', sa.Float(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE ambient_env_data OWNER to mhs')

    op.create_table(
        'error_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('port', sa.String(), nullable=False),
        sa.Column('error', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE error_data OWNER to mhs')

    op.create_table(
        'plc_data_hour',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('port', sa.String(), nullable=False),
        sa.Column('belt_speed', sa.Integer(), nullable=True),
        sa.Column('vfd_current', sa.Float(), nullable=True),
        sa.Column('vfd_frequency', sa.Float(), nullable=True),
        sa.Column('vfd_voltage', sa.Float(), nullable=True),
        sa.Column('vfd_fault_code', sa.Integer(), nullable=True),
        sa.Column('sorter_induct_rate', sa.Integer(), nullable=True),
        sa.Column('sorter_reject_rate', sa.Integer(), nullable=True),
        sa.Column('status', sa.String(), nullable=True),
        sa.Column('torque', sa.Integer(), nullable=True),
        sa.Column('hours_running', sa.Integer(), nullable=True),
        sa.Column('power', sa.Integer(), nullable=True),
        sa.Column('rpm', sa.Integer(), nullable=True),
        sa.Column('horsepower_nameplate', sa.Integer(), nullable=True),
        sa.Column('voltage_nameplate', sa.Integer(), nullable=True),
        sa.Column('run_speed_nameplate', sa.Integer(), nullable=True),
        sa.Column('current_nameplate', sa.Float(), nullable=True),
        sa.Column('num_phases_nameplate', sa.Integer(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE plc_data_hour OWNER to mhs')

    op.create_table(
        'plc_data_minute',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('port', sa.String(), nullable=False),
        sa.Column('belt_speed', sa.Integer(), nullable=True),
        sa.Column('vfd_current', sa.Float(), nullable=True),
        sa.Column('vfd_frequency', sa.Float(), nullable=True),
        sa.Column('vfd_voltage', sa.Float(), nullable=True),
        sa.Column('vfd_fault_code', sa.Integer(), nullable=True),
        sa.Column('sorter_induct_rate', sa.Integer(), nullable=True),
        sa.Column('sorter_reject_rate', sa.Integer(), nullable=True),
        sa.Column('status', sa.String(), nullable=True),
        sa.Column('torque', sa.Integer(), nullable=True),
        sa.Column('hours_running', sa.Integer(), nullable=True),
        sa.Column('power', sa.Integer(), nullable=True),
        sa.Column('rpm', sa.Integer(), nullable=True),
        sa.Column('horsepower_nameplate', sa.Integer(), nullable=True),
        sa.Column('voltage_nameplate', sa.Integer(), nullable=True),
        sa.Column('run_speed_nameplate', sa.Integer(), nullable=True),
        sa.Column('current_nameplate', sa.Float(), nullable=True),
        sa.Column('num_phases_nameplate', sa.Integer(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE plc_data_minute OWNER to mhs')

    op.create_table(
        'sensor_data_hour',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('port', sa.String(), nullable=False),
        sa.Column('rms_velocity_z', sa.Float(), nullable=True),
        sa.Column('temperature', sa.Float(), nullable=True),
        sa.Column('rms_velocity_x', sa.Float(), nullable=True),
        sa.Column('peak_acceleration_z', sa.Float(), nullable=True),
        sa.Column('peak_acceleration_x', sa.Float(), nullable=True),
        sa.Column('peak_frequency_z', sa.Float(), nullable=True),
        sa.Column('peak_frequency_x', sa.Float(), nullable=True),
        sa.Column('rms_acceleration_z', sa.Float(), nullable=True),
        sa.Column('rms_acceleration_x', sa.Float(), nullable=True),
        sa.Column('kurtosis_z', sa.Float(), nullable=True),
        sa.Column('kurtosis_x', sa.Float(), nullable=True),
        sa.Column('crest_acceleration_z', sa.Float(), nullable=True),
        sa.Column('crest_acceleration_x', sa.Float(), nullable=True),
        sa.Column('peak_velocity_z', sa.Float(), nullable=True),
        sa.Column('peak_velocity_x', sa.Float(), nullable=True),
        sa.Column('hf_rms_acceleration_z', sa.Float(), nullable=True),
        sa.Column('hf_rms_acceleration_x', sa.Float(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE sensor_data_hour OWNER to mhs')

    op.create_table(
        'sensor_data_minute',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('port', sa.String(), nullable=False),
        sa.Column('rms_velocity_z', sa.Float(), nullable=True),
        sa.Column('temperature', sa.Float(), nullable=True),
        sa.Column('rms_velocity_x', sa.Float(), nullable=True),
        sa.Column('peak_acceleration_z', sa.Float(), nullable=True),
        sa.Column('peak_acceleration_x', sa.Float(), nullable=True),
        sa.Column('peak_frequency_z', sa.Float(), nullable=True),
        sa.Column('peak_frequency_x', sa.Float(), nullable=True),
        sa.Column('rms_acceleration_z', sa.Float(), nullable=True),
        sa.Column('rms_acceleration_x', sa.Float(), nullable=True),
        sa.Column('kurtosis_z', sa.Float(), nullable=True),
        sa.Column('kurtosis_x', sa.Float(), nullable=True),
        sa.Column('crest_acceleration_z', sa.Float(), nullable=True),
        sa.Column('crest_acceleration_x', sa.Float(), nullable=True),
        sa.Column('peak_velocity_z', sa.Float(), nullable=True),
        sa.Column('peak_velocity_x', sa.Float(), nullable=True),
        sa.Column('hf_rms_acceleration_z', sa.Float(), nullable=True),
        sa.Column('hf_rms_acceleration_x', sa.Float(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE sensor_data_minute OWNER to mhs')

    op.create_table(
        'predicted_data_minute',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('rms_velocity_x', sa.Float(), nullable=True),
        sa.Column('rms_velocity_x_residuals', sa.Float(), nullable=True),
        sa.Column('rms_velocity_z', sa.Float(), nullable=True),
        sa.Column('rms_velocity_z_residuals', sa.Float(), nullable=True),
        sa.Column('temperature', sa.Float(), nullable=True),
        sa.Column('temperature_residuals', sa.Float(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE predicted_data_minute OWNER to mhs')

    op.create_table(
        'predicted_data_hour',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('rms_velocity_x', sa.Float(), nullable=True),
        sa.Column('rms_velocity_x_residuals', sa.Float(), nullable=True),
        sa.Column('rms_velocity_z', sa.Float(), nullable=True),
        sa.Column('rms_velocity_z_residuals', sa.Float(), nullable=True),
        sa.Column('temperature', sa.Float(), nullable=True),
        sa.Column('temperature_residuals', sa.Float(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE predicted_data_hour OWNER to mhs')


def downgrade(schema):
    op.drop_table('thresholds')
    op.drop_table('stats')
    op.drop_table('sensors')
    op.drop_table('gateways')
    op.drop_table('conveyors')
    op.drop_table('acknowledgments')
    op.drop_table('alert_black_list')
    op.drop_table('alarms')
    op.drop_table('mode_transition_detections')
    op.drop_table('conveyor_statuses')
    op.drop_table('alert_roles')
    op.drop_table('alert_users')
    op.drop_table('predicted_data_minute')
    op.drop_table('predicted_data_hour')
    op.drop_table('sensor_data_minute')
    op.drop_table('sensor_data_hour')
    op.drop_table('plc_data_minute')
    op.drop_table('plc_data_hour')
    op.drop_table('error_data')
    op.drop_table('ambient_env_data')

    op.execute('DROP TYPE equipment_enum')
    op.execute('DROP TYPE thresholds_type_enum')
    op.execute('DROP TYPE thresholds_metric_enum')
    op.execute('DROP TYPE alert_roles_enum')
    op.execute('DROP TYPE conveyor_statuses_enum')
    op.execute('DROP TYPE alarms_type_enum')
    op.execute('DROP TYPE alarms_metric_enum')
    op.execute('DROP TYPE levels_enum')
