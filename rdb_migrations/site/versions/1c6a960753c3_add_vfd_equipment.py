"""add_vfd_equipment

Revision ID: 1c6a960753c3
Revises: 9531472b01c2
Create Date: 2020-08-03 16:09:16.227312

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1c6a960753c3'
down_revision = '9531472b01c2'
branch_labels = None
depends_on = None


def upgrade(schema):
    with op.get_context().autocommit_block():
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'VFD'")


def downgrade(schema):
    raise Exception('Irreversible migration')
