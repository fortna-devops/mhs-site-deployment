"""remove_gateway_data_port

Revision ID: 240090c1c1de
Revises: 251fff3c3b4b
Create Date: 2019-11-29 11:57:34.015450

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '240090c1c1de'
down_revision = '251fff3c3b4b'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.drop_column('ambient_env_data', 'port')
    op.drop_column('camera_data', 'port')
    op.drop_column('error_data', 'port')
    op.drop_column('plc_data_hour', 'port')
    op.drop_column('plc_data_minute', 'port')
    op.drop_column('sensor_data_hour', 'port')
    op.drop_column('sensor_data_minute', 'port')


def downgrade(schema):
    raise Exception("Irreversible migration")
