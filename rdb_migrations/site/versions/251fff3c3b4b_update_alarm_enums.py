"""update_alarm_enums

Revision ID: 251fff3c3b4b
Revises: 7d1e03ecd658
Create Date: 2019-11-28 12:53:08.602323

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '251fff3c3b4b'
down_revision = '7d1e03ecd658'
branch_labels = None
depends_on = None


def upgrade(schema):
    with op.get_context().autocommit_block():
        op.execute("ALTER TYPE alarms_type_enum RENAME VALUE 'ISO' TO 'ISO_SPIKE'")
        op.execute("ALTER TYPE alarms_type_enum RENAME VALUE 'ML' TO 'ML_SPIKE'")
        op.execute("ALTER TYPE alarms_type_enum ADD VALUE 'ISO_PERSISTENT' AFTER 'ISO_SPIKE'")
        op.execute("ALTER TYPE alarms_type_enum ADD VALUE 'ML_PERSISTENT' AFTER 'ML_SPIKE'")


def downgrade(schema):
    raise Exception("Irreversible migration")
