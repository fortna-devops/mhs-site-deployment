"""add-cos_phi-column-for-vfd-data-table

Revision ID: 26d193f28313
Revises: 9303bc7d5295
Create Date: 2020-10-05 09:31:05.909975

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '26d193f28313'
down_revision = '9303bc7d5295'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.add_column('vfd_data', sa.Column('cos_phi', sa.REAL(), nullable=True))


def downgrade(schema):
    op.drop_column('vfd_data', 'cos_phi')
