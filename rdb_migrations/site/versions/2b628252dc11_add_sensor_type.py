"""add_sensor_type

Revision ID: 2b628252dc11
Revises: abe38fd0154e
Create Date: 2019-11-25 12:43:15.386493

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM


# revision identifiers, used by Alembic.

revision = '2b628252dc11'
down_revision = 'abe38fd0154e'
branch_labels = None
depends_on = None


def upgrade(schema):
    sensors_type_enum = ENUM(
        'vt',
        'ambient',
        'plc',
        'camera',
        create_type=False,
        name='sensors_type_enum'
    )
    sensors_type_enum.create(op.get_bind())
    op.execute('ALTER TYPE sensors_type_enum OWNER to mhs')

    op.add_column('sensors', sa.Column('type', sensors_type_enum, nullable=True))


def downgrade(schema):
    op.drop_column('sensors', 'type')
    op.execute('DROP TYPE sensors_type_enum')
