"""update_new_new_alarms

Revision ID: 2f17b0ca67a9
Revises: 0e6ed8d9c25a
Create Date: 2020-12-14 14:50:56.111337

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import insert
from datetime import datetime

# revision identifiers, used by Alembic.
revision = '2f17b0ca67a9'
down_revision = '0e6ed8d9c25a'
branch_labels = None
depends_on = None


def upgrade(schema):

    op.execute('DELETE FROM new_new_alarms')
    op.alter_column('new_new_alarms', 'ended_at', nullable=False)

    op.execute('DELETE FROM alarm_configs')

    connection = op.get_bind()
    meta = sa.MetaData(bind=connection, schema=schema)
    meta.reflect(only=(
        'alarm_types',
        'alarm_configs'
    ))

    alarm_types_table = sa.Table('alarm_types', meta)
    alarm_configs_table = sa.Table('alarm_configs', meta)

    cur = connection.execute(alarm_types_table.select().order_by(alarm_types_table.c.name.asc()))
    alarm_type_keys = cur.keys()
    alarm_type_rows = cur.fetchall()
    cur.close()

    colors = ('YELLOW', 'RED')
    alarm_configs = []
    for alarm_type_row in alarm_type_rows:

        alarm_type = dict(zip(alarm_type_keys, alarm_type_row))
        for color in colors:

            alarm_configs.append({
                'type_name': alarm_type['name'],
                'color': color,
                'to_display': alarm_type['to_display']
            })

    if alarm_configs:
        connection.execute(
            insert(alarm_configs_table).values(
                [
                    {
                        'type_name': alarm_config['type_name'],
                        'color': alarm_config['color'],
                        'to_display': alarm_config['to_display'],
                        'created_at': datetime.utcnow()
                    }
                    for alarm_config in alarm_configs
                ]
            )
        )


def downgrade(schema):
    raise Exception('Irreversible migration')
