"""fill_thresholds_table

Revision ID: 3ca1b2e9dd92
Revises: 8abd4c462bb2
Create Date: 2020-10-20 12:56:48.452019

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import insert
import logging


# revision identifiers, used by Alembic.
revision = '3ca1b2e9dd92'
down_revision = '6fb748e2ce8a'
branch_labels = None
depends_on = None


logger = logging.getLogger()


def upgrade(schema):
    if schema == 'dhl_brescia':
        logger.warning(f"The migration '{revision}' won't be applied to '{schema}'. Manual data coping expected.")
        return

    op.execute('DELETE FROM threshold_types')
    op.execute('DELETE FROM threshold_levels')

    connection = op.get_bind()
    meta = sa.MetaData(bind=connection, schema=schema)
    meta.reflect(only=(
        'thresholds',
        'metrics',
        'threshold_types',
        'threshold_levels',
        'new_thresholds'
    ))

    thresholds_table = sa.Table('thresholds', meta)
    metrics_table = sa.Table('metrics', meta)
    threshold_types_table = sa.Table('threshold_types', meta)
    threshold_levels_table = sa.Table('threshold_levels', meta)
    new_thresholds_table = sa.Table('new_thresholds', meta)

    cur = connection.execute(thresholds_table.select().order_by(thresholds_table.c.id.asc()))
    threshold_keys = cur.keys()
    threshold_rows = cur.fetchall()
    cur.close()

    data_source_ids = set()
    thresholds = []
    for threshold_row in threshold_rows:

        threshold = dict(zip(threshold_keys, threshold_row))

        data_source_ids.add(threshold['sensor_id'])
        thresholds.append(threshold)

    cur = connection.execute(metrics_table.select().where(metrics_table.c.data_source_id.in_(data_source_ids)))
    metric_keys = cur.keys()
    metric_rows = cur.fetchall()
    cur.close()

    metric_ids = {}
    for metric_row in metric_rows:

        metric = dict(zip(metric_keys, metric_row))
        if metric['data_source_id'] not in metric_ids:
            metric_ids[metric['data_source_id']] = {}

        if metric['name'] not in metric_ids[metric['data_source_id']]:
            metric_ids[metric['data_source_id']][metric['name']] = metric['id']

    threshold_types = {}
    threshold_levels = {}
    new_thresholds = []

    for threshold in thresholds:

        if threshold['sensor_id'] not in metric_ids:
            logger.warning(f"Sensor id '{threshold['sensor_id']}' not found for '{threshold['id']}' threshold.")
            continue

        if threshold['metric'] not in metric_ids[threshold['sensor_id']]:
            logger.warning(f"Metric name '{threshold['metric']}' not found for '{threshold['id']}' threshold.")
            continue

        threshold_type_name = threshold['type'].lower()
        if threshold_type_name not in threshold_types:
            threshold_types[threshold_type_name] = {
                'name': threshold_type_name,
                'created_at': threshold['created_at']
            }

        threshold_level_name = threshold['name'].lower()
        if threshold_level_name not in threshold_levels:
            threshold_levels[threshold_level_name] = {
                'name': threshold_level_name,
                'created_at': threshold['created_at']
            }

        metric_id = metric_ids[threshold['sensor_id']][threshold['metric']]

        new_threshold = {
            'metric_id': metric_id,
            'type_name': threshold_type_name,
            'level_name': threshold_level_name,
            'value': threshold['value'],
            'created_at': threshold['created_at']
        }

        new_thresholds.append(new_threshold)

    for threshold_type in threshold_types.values():
        connection.execute(insert(threshold_types_table).values(threshold_type))

    for threshold_level in threshold_levels.values():
        connection.execute(insert(threshold_levels_table).values(threshold_level))

    if new_thresholds:
        connection.execute(insert(new_thresholds_table).values(new_thresholds))


def downgrade(schema):
    raise Exception('Irreversible migration')
