"""add_kpi_tables

Revision ID: 3cc7be257b86
Revises: b9c7a062cf5c
Create Date: 2021-02-08 16:16:26.483612

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3cc7be257b86'
down_revision = 'b9c7a062cf5c'
branch_labels = None
depends_on = None


def upgrade(schema):

    op.create_table(
        'kpi_groups',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('display_name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE kpi_groups OWNER to mhs')

    op.create_table(
        'kpis',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('asset_id', sa.Integer(), nullable=False),
        sa.Column('kpi_group_id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('display_name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['asset_id'], ['assets.id'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['kpi_group_id'], ['kpi_groups.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE kpis OWNER to mhs')

    op.create_table(
        'metrics_kpis_association',
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('kpis_id', sa.Integer(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['kpis_id'], ['kpis.id'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('metric_id', 'kpis_id')
    )
    op.execute('ALTER TABLE metrics_kpis_association OWNER to mhs')


def downgrade(schema):
    raise Exception('Irreversible migration')
