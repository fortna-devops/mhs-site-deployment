"""update_data_id

Revision ID: 3f3c3c14faab
Revises: 2f17b0ca67a9
Create Date: 2021-01-13 14:58:55.349339

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '3f3c3c14faab'
down_revision = '2f17b0ca67a9'
branch_labels = None
depends_on = None


SITES_TO_RECREATE = ('dhl_brescia', 'ups_independence')


def recreate_tables():
    op.drop_table('raw_float_data')
    op.drop_table('raw_int_data')
    op.drop_table('raw_string_data')

    op.drop_table('calculated_float_data')
    op.drop_table('calculated_int_data')
    op.drop_table('calculated_string_data')

    op.create_table(    
        'raw_float_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.REAL(), nullable=False),
        sa.Column('flags', sa.SmallInteger(), server_default='0', nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE raw_float_data OWNER to mhs')

    op.create_table(
        'raw_int_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.Integer(), nullable=False),
        sa.Column('flags', sa.SmallInteger(), server_default='0', nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE raw_int_data OWNER to mhs')

    op.create_table(
        'raw_string_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.String(), nullable=False),
        sa.Column('flags', sa.SmallInteger(), server_default='0', nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE raw_string_data OWNER to mhs')

    op.create_index('raw_float_data_metric_id_timestamp_minute_flags_index', 'raw_float_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)"), 'flags'])
    op.create_index('raw_float_data_metric_id_timestamp_hour_flags_index', 'raw_float_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)"), 'flags'])

    op.create_index('raw_int_data_metric_id_timestamp_minute_flags_index', 'raw_int_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)"), 'flags'])
    op.create_index('raw_int_data_metric_id_timestamp_hour_flags_index', 'raw_int_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)"), 'flags'])

    op.create_index('raw_string_data_metric_id_timestamp_minute_flags_index', 'raw_string_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)"), 'flags'])
    op.create_index('raw_string_data_metric_id_timestamp_hour_flags_index', 'raw_string_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)"), 'flags'])

    op.execute('ALTER SEQUENCE raw_float_data_id_seq AS BIGINT')
    op.execute('ALTER SEQUENCE raw_int_data_id_seq AS BIGINT')
    op.execute('ALTER SEQUENCE raw_string_data_id_seq AS BIGINT')

    op.create_table(
        'calculated_float_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.REAL(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE calculated_float_data OWNER to mhs')
    
    op.create_table(
        'calculated_int_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.Integer(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE calculated_int_data OWNER to mhs')

    op.create_table(
        'calculated_string_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE calculated_string_data OWNER to mhs')

    op.create_index('calculated_float_data_metric_id_timestamp_minute_index', 'calculated_float_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('calculated_float_data_metric_id_timestamp_hour_index', 'calculated_float_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])

    op.create_index('calculated_int_data_metric_id_timestamp_minute_index', 'calculated_int_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('calculated_int_data_metric_id_timestamp_hour_index', 'calculated_int_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])

    op.create_index('calculated_string_data_metric_id_timestamp_minute_index', 'calculated_string_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('calculated_string_data_metric_id_timestamp_hour_index', 'calculated_string_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])

    op.execute('ALTER SEQUENCE calculated_float_data_id_seq AS BIGINT')
    op.execute('ALTER SEQUENCE calculated_int_data_id_seq AS BIGINT')
    op.execute('ALTER SEQUENCE calculated_string_data_id_seq AS BIGINT')


def update_tables():
    tables = (
        'raw_float_data',
        'raw_int_data',
        'raw_string_data',
        'calculated_float_data',
        'calculated_int_data',
        'calculated_string_data',
    )
    for table_name in tables:
        op.alter_column(table_name, 'id', type_=sa.BigInteger)
        op.execute(f'ALTER SEQUENCE {table_name}_id_seq AS BIGINT')


def upgrade(schema):
    if schema in SITES_TO_RECREATE:
        recreate_tables()
    else:
        update_tables()


def downgrade(schema):
    raise Exception('Irreversible migration')
