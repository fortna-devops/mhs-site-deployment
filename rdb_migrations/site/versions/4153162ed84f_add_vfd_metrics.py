"""add_vfd_metrics

Revision ID: 4153162ed84f
Revises: 1c6a960753c3
Create Date: 2020-08-05 17:18:43.113535

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4153162ed84f'
down_revision = '1c6a960753c3'
branch_labels = None
depends_on = None


def upgrade(schema):
    with op.get_context().autocommit_block():
        op.execute("ALTER TYPE alarms_metric_enum ADD VALUE 'speed'")
        op.execute("ALTER TYPE alarms_metric_enum ADD VALUE 'current'")
        op.execute("ALTER TYPE alarms_metric_enum ADD VALUE 'voltage'")
        op.execute("ALTER TYPE alarms_metric_enum ADD VALUE 'mechanical_power'")

        op.execute("ALTER TYPE thresholds_metric_enum ADD VALUE 'speed'")
        op.execute("ALTER TYPE thresholds_metric_enum ADD VALUE 'current'")
        op.execute("ALTER TYPE thresholds_metric_enum ADD VALUE 'voltage'")
        op.execute("ALTER TYPE thresholds_metric_enum ADD VALUE 'mechanical_power'")


def downgrade(schema):
    raise Exception('Irreversible migration')
