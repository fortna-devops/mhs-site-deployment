"""add_data_status

Revision ID: 43b46370a419
Revises: 0ddfe0e862ec
Create Date: 2020-09-10 14:58:05.005810

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '43b46370a419'
down_revision = '54957534d677'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.add_column('raw_float_data', sa.Column('flags', sa.SmallInteger(), server_default='0', nullable=False))
    op.add_column('raw_int_data', sa.Column('flags', sa.SmallInteger(), server_default='0', nullable=False))
    op.add_column('raw_string_data', sa.Column('flags', sa.SmallInteger(), server_default='0', nullable=False))


def downgrade(schema):
    op.drop_column('raw_string_data', 'flags')
    op.drop_column('raw_int_data', 'flags')
    op.drop_column('raw_float_data', 'flags')
