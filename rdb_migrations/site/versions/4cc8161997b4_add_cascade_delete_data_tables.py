"""add_cascade_delete_data_tables

Revision ID: 4cc8161997b4
Revises: 26d193f28313
Create Date: 2020-10-21 12:36:40.320912

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '4cc8161997b4'
down_revision = '26d193f28313'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.drop_constraint('raw_float_data_metric_id_fkey', 'raw_float_data', type_='foreignkey')
    op.create_foreign_key(None, 'raw_float_data', 'metrics', ['metric_id'], ['id'], ondelete='CASCADE')

    op.drop_constraint('raw_int_data_metric_id_fkey', 'raw_int_data', type_='foreignkey')
    op.create_foreign_key(None, 'raw_int_data', 'metrics', ['metric_id'], ['id'], ondelete='CASCADE')

    op.drop_constraint('raw_string_data_metric_id_fkey', 'raw_string_data', type_='foreignkey')
    op.create_foreign_key(None, 'raw_string_data', 'metrics', ['metric_id'], ['id'], ondelete='CASCADE')

    op.drop_constraint('calculated_float_data_metric_id_fkey', 'calculated_float_data', type_='foreignkey')
    op.create_foreign_key(None, 'calculated_float_data', 'metrics', ['metric_id'], ['id'], ondelete='CASCADE')

    op.drop_constraint('calculated_int_data_metric_id_fkey', 'calculated_int_data', type_='foreignkey')
    op.create_foreign_key(None, 'calculated_int_data', 'metrics', ['metric_id'], ['id'], ondelete='CASCADE')

    op.drop_constraint('calculated_string_data_metric_id_fkey', 'calculated_string_data', type_='foreignkey')
    op.create_foreign_key(None, 'calculated_string_data', 'metrics', ['metric_id'], ['id'], ondelete='CASCADE')


def downgrade(schema):
    raise Exception('Irreversible migration')
