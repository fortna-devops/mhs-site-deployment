"""add_warnings_errors

Revision ID: 54957534d677
Revises: 0910f160e60b
Create Date: 2020-08-28 14:30:00.630732

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '54957534d677'
down_revision = '0910f160e60b'
branch_labels = None
depends_on = None


def upgrade(schema):

    op.create_table(
        'warnings',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('description', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint('name')
    )
    op.execute('ALTER TABLE warnings OWNER to mhs')

    op.create_table(
        'errors',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('description', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint('name')
    )
    op.execute('ALTER TABLE errors OWNER to mhs')

    with op.get_context().autocommit_block():
        op.execute("ALTER TYPE alarms_type_enum ADD VALUE 'WARNING'")
        op.execute("ALTER TYPE alarms_type_enum ADD VALUE 'ERROR'")

        op.execute("ALTER TYPE alarms_metric_enum ADD VALUE 'undefined'")


def downgrade(schema):
    raise Exception('Irreversible migration')
