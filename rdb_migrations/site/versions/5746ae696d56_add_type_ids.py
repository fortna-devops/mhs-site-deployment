"""add_type_ids

Revision ID: 5746ae696d56
Revises: df879e058216
Create Date: 2020-11-11 15:45:21.607043

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import update
from sqlalchemy.schema import Sequence, CreateSequence

# revision identifiers, used by Alembic.
revision = '5746ae696d56'
down_revision = '8abd4c462bb2'
branch_labels = None
depends_on = None


def upgrade(schema):

    op.add_column('asset_types', sa.Column('id', sa.Integer(), autoincrement=True, nullable=True))
    op.add_column('asset_types', sa.Column('model', sa.String(), nullable=True))
    op.add_column('assets', sa.Column('type_id', sa.Integer(), nullable=True))

    op.add_column('component_types', sa.Column('id', sa.Integer(), autoincrement=True, nullable=True))
    op.add_column('component_types', sa.Column('model', sa.String(), nullable=True))
    op.add_column('components', sa.Column('type_id', sa.Integer(), nullable=True))

    op.add_column('data_source_types', sa.Column('id', sa.Integer(), autoincrement=True, nullable=True))
    op.add_column('data_source_types', sa.Column('model', sa.String(), nullable=True))
    op.add_column('data_sources', sa.Column('type_id', sa.Integer(), nullable=True))

    connection = op.get_bind()
    meta = sa.MetaData(bind=connection, schema=schema)
    meta.reflect(only=(
        'asset_types',
        'assets',
        'component_types',
        'components',
        'data_source_types',
        'data_sources',
    ))
    asset_types_table = sa.Table('asset_types', meta)
    assets_table = sa.Table('assets', meta)
    component_types_table = sa.Table('component_types', meta)
    components_table = sa.Table('components', meta)
    data_source_types_table = sa.Table('data_source_types', meta)
    data_sources_table = sa.Table('data_sources', meta)

    cur = connection.execute(asset_types_table.select().order_by(asset_types_table.c.name.asc()))
    asset_types_keys = cur.keys()
    asset_types_rows = cur.fetchall()
    cur.close()

    asset_type_ids = {}
    asset_type_id = 0
    for asset_types_row in asset_types_rows:

        asset_type = dict(zip(asset_types_keys, asset_types_row))
        asset_type_id += 1
        asset_type_ids[asset_type['name']] = asset_type_id

    cur = connection.execute(assets_table.select())
    assets_keys = cur.keys()
    assets_rows = cur.fetchall()
    cur.close()

    assets = []
    for asset_row in assets_rows:

        asset = dict(zip(assets_keys, asset_row))
        asset['type_id'] = asset_type_ids[asset['type_name']]
        assets.append(asset)

    for asset_type_name, asset_type_id in asset_type_ids.items():
        connection.execute(
            update(asset_types_table).where(asset_types_table.c.name == asset_type_name).values(id=asset_type_id)
        )

    for asset in assets:
        connection.execute(
            update(assets_table).where(assets_table.c.id == asset['id']).values(type_id=asset['type_id'])
        )

    op.drop_constraint('assets_type_name_fkey', 'assets')
    op.drop_constraint('asset_types_pkey', 'asset_types')

    op.alter_column('asset_types', 'id', nullable=False)
    op.alter_column('assets', 'type_id', nullable=False)

    op.create_primary_key('asset_types_id_pkey', 'asset_types', ['id', ])
    op.create_foreign_key('asset_type_id_fkey', 'assets', 'asset_types', ['type_id'], ['id'], ondelete='CASCADE')

    op.execute(CreateSequence(Sequence('asset_types_id_seq')))
    op.execute('ALTER SEQUENCE asset_types_id_seq OWNER to mhs')
    op.alter_column('asset_types', 'id', server_default=sa.text("nextval('asset_types_id_seq'::regclass)"))

    cur = connection.execute(component_types_table.select().order_by(component_types_table.c.name.asc()))
    component_types_keys = cur.keys()
    component_types_rows = cur.fetchall()
    cur.close()

    component_type_ids = {}
    component_type_id = 0
    for component_types_row in component_types_rows:
        component_type = dict(zip(component_types_keys, component_types_row))
        component_type_id += 1
        component_type_ids[component_type['name']] = component_type_id

    cur = connection.execute(components_table.select())
    components_keys = cur.keys()
    components_rows = cur.fetchall()
    cur.close()

    components = []
    for components_row in components_rows:
        component = dict(zip(components_keys, components_row))
        component['type_id'] = component_type_ids[component['type_name']]
        components.append(component)

    for component_type_name, component_type_id in component_type_ids.items():
        connection.execute(
            update(component_types_table).where(
                component_types_table.c.name == component_type_name
            ).values(
                id=component_type_id
            )
        )

    for component in components:
        connection.execute(
            update(components_table).where(
                components_table.c.id == component['id']
            ).values(
                type_id=component['type_id']
            )
        )

    op.drop_constraint('new_spare_parts_results_component_type_name_fkey', 'new_spare_parts_results',
                       type_='foreignkey')
    op.drop_constraint('components_type_name_fkey', 'components')
    op.drop_constraint('component_types_pkey', 'component_types')

    op.alter_column('component_types', 'id', nullable=False)
    op.alter_column('components', 'type_id', nullable=False)

    op.create_primary_key('component_types_id_pkey', 'component_types', ['id', ])
    op.create_foreign_key('component_type_id_fkey', 'components', 'component_types', ['type_id'], ['id'],
                          ondelete='CASCADE')

    op.execute(CreateSequence(Sequence('component_types_id_seq')))
    op.execute('ALTER SEQUENCE component_types_id_seq OWNER to mhs')
    op.alter_column('component_types', 'id', server_default=sa.text("nextval('component_types_id_seq'::regclass)"))

    cur = connection.execute(data_source_types_table.select().order_by(data_source_types_table.c.name.asc()))
    data_source_types_keys = cur.keys()
    data_source_types_rows = cur.fetchall()
    cur.close()

    data_source_type_ids = {}
    data_source_type_id = 0
    for data_source_types_row in data_source_types_rows:
        data_source_type = dict(zip(data_source_types_keys, data_source_types_row))
        data_source_type_id += 1
        data_source_type_ids[data_source_type['name']] = data_source_type_id

    cur = connection.execute(data_sources_table.select())
    data_sources_keys = cur.keys()
    data_sources_rows = cur.fetchall()
    cur.close()

    data_sources = []
    for data_sources_row in data_sources_rows:
        data_source = dict(zip(data_sources_keys, data_sources_row))
        data_source['type_id'] = data_source_type_ids[data_source['type_name']]
        data_sources.append(data_source)

    for data_source_type_name, data_source_type_id in data_source_type_ids.items():
        connection.execute(
            update(data_source_types_table).where(
                data_source_types_table.c.name == data_source_type_name
            ).values(
                id=data_source_type_id
            )
        )

    for data_source in data_sources:
        connection.execute(
            update(data_sources_table).where(
                data_sources_table.c.id == data_source['id']
            ).values(
                type_id=data_source['type_id']
            )
        )

    op.drop_constraint('data_sources_type_name_fkey', 'data_sources')
    op.drop_constraint('data_source_types_pkey', 'data_source_types')

    op.alter_column('data_source_types', 'id', nullable=False)
    op.alter_column('data_sources', 'type_id', nullable=False)

    op.create_primary_key('data_sources_id_pkey', 'data_source_types', ['id', ])
    op.create_foreign_key('data_sources_type_id_fkey', 'data_sources', 'data_source_types', ['type_id'], ['id'],
                          ondelete='CASCADE')

    op.execute(CreateSequence(Sequence('data_source_types_id_seq')))
    op.execute('ALTER SEQUENCE data_source_types_id_seq OWNER to mhs')
    op.alter_column('data_source_types', 'id', server_default=sa.text("nextval('data_source_types_id_seq'::regclass)"))

    sequences = [
        'asset_types_id_seq',
        'component_types_id_seq',
        'data_source_types_id_seq',
    ]

    for sequence in sequences:
        table_name = '_'.join(sequence.split('_')[:-2])
        op.execute(f"SELECT setval('{sequence}', (SELECT max(id) FROM {table_name}))")


def downgrade(schema):
    raise Exception('Irreversible migration')
