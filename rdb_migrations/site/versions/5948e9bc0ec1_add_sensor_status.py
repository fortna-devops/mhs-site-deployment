"""add_sensor_status

Revision ID: 5948e9bc0ec1
Revises: 95a548ad2176
Create Date: 2020-01-27 16:04:31.052990

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM


# revision identifiers, used by Alembic.
revision = '5948e9bc0ec1'
down_revision = '95a548ad2176'
branch_labels = None
depends_on = None


def upgrade(schema):
    sensors_statuses_enum = ENUM(
        'ACTIVE',
        'INACTIVE',
        'MALFUNCTIONING',
        create_type=False,
        name='sensors_statuses_enum'
    )
    sensors_statuses_enum.create(op.get_bind())
    op.execute('ALTER TYPE sensors_statuses_enum OWNER to mhs')
    op.add_column('sensors', sa.Column('status', sensors_statuses_enum, server_default='ACTIVE', nullable=False))


def downgrade(schema):
    op.drop_column('sensors', 'status')
    op.execute('DROP TYPE sensors_statuses_enum')
