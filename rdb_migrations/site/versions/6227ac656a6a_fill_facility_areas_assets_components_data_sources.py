"""fill_facility_areas_assets_components_data_sources_tables

Revision ID: 6227ac656a6a
Revises: 26d193f28313
Create Date: 2020-10-13 12:52:17.677308

"""
from alembic import op
from sqlalchemy import insert
import sqlalchemy as sa
import logging

# revision identifiers, used by Alembic.
revision = '6227ac656a6a'
down_revision = '4cc8161997b4'
branch_labels = None
depends_on = None


logger = logging.getLogger()


def upgrade(schema):

    if schema == 'dhl_brescia':
        logger.warning(f"The migration '{revision}' won't be applied to '{schema}'. Manual data coping expected.")
        return

    component_type_names = {
        'm': 'motor',
        'g': 'gearbox',
        'b': 'bearing'
    }

    data_source_type_names = {
        'vt': 'qm42vt2',
        'ambient': 'm12fth3q'
    }

    data_source_manufacturers = {
        'qm42vt2': 'banner',
        'm12fth3q': 'banner'
    }

    op.execute('DELETE FROM data_source_types')
    op.execute('DELETE FROM component_types')
    op.execute('DELETE FROM asset_types')
    op.execute('DELETE FROM facility_areas')

    connection = op.get_bind()
    meta = sa.MetaData(bind=connection, schema=schema)
    meta.reflect(only=(
        'sensors',
        'conveyors',
        'data_source_types',
        'data_sources',
        'component_types',
        'components',
        'asset_types',
        'assets',
        'facility_areas'
    ))

    sensors_table = sa.Table('sensors', meta)
    conveyors_table = sa.Table('conveyors', meta)
    data_source_types_table = sa.Table('data_source_types', meta)
    data_sources_table = sa.Table('data_sources', meta)
    component_types_table = sa.Table('component_types', meta)
    components_table = sa.Table('components', meta)
    asset_types_table = sa.Table('asset_types', meta)
    assets_table = sa.Table('assets', meta)
    facility_areas_table = sa.Table('facility_areas', meta)

    cur = connection.execute(sensors_table.select().order_by(sensors_table.c.id.asc()))
    sensor_keys = cur.keys()
    sensor_rows = cur.fetchall()
    cur.close()

    facility_areas = {}
    facility_area_id = 0
    asset_facility_area_ids = {}
    component_types = {}
    components = {}
    component_id = 0
    data_source_types = {}
    data_sources = {}
    for sensor_row in sensor_rows:

        sensor = dict(zip(sensor_keys, sensor_row))

        facility_area_name = sensor['facility_organization']
        if facility_area_name not in facility_areas:

            facility_area_id += 1
            facility_areas[facility_area_name] = {
                'id': facility_area_id,
                'name': facility_area_name,
                'created_at': sensor['created_at']
            }

        if sensor['conveyor_id'] is None:
            logger.warning(f"Conveyor id is None for sensor '{sensor['id']}'. "
                           "This sensor won't be converted to data source.")
            continue

        facility_area = facility_areas[facility_area_name]
        asset_facility_area_ids[sensor['conveyor_id']] = facility_area['id']

        component_type_name = sensor['equipment'] if sensor['equipment'] is not None else 'undefined'
        component_type_name = component_type_name.lower()
        if component_type_name in component_type_names:
            component_type_name = component_type_names[component_type_name]

        if component_type_name not in component_types:
            component_types[component_type_name] = {
                'name': component_type_name,
                'created_at': sensor['created_at']
            }

        if sensor['conveyor_id'] not in components:
            components[sensor['conveyor_id']] = {}

        if component_type_name not in components[sensor['conveyor_id']]:

            component_id += 1
            components[sensor['conveyor_id']][component_type_name] = {
                'id': component_id,
                'asset_id': sensor['conveyor_id'],
                'type_name': component_type_name,
                'name': component_type_name.capitalize().replace('_', ' '),
                'created_at': sensor['created_at']
            }

        data_source_type_name = sensor['type'] if sensor['type'] is not None else 'undefined'
        if data_source_type_name in data_source_type_names:
            data_source_type_name = data_source_type_names[data_source_type_name]

        if data_source_type_name not in data_source_types:
            data_source_types[data_source_type_name] = {
                'name': data_source_type_name,
                'manufacturer': data_source_manufacturers.get(data_source_type_name),
                'created_at': sensor['created_at']
            }

        data_sources[sensor['id']] = {
            'id': sensor['id'],
            'component_id': component_id,
            'type_name': data_source_type_name,
            'name': f'{component_type_name} {data_source_type_name}',
            'location': sensor['location'],
            'address': sensor['name'],
            'created_at': sensor['created_at']
        }

    cur = connection.execute(conveyors_table.select().order_by(conveyors_table.c.id.asc()))
    conveyor_keys = cur.keys()
    conveyor_rows = cur.fetchall()
    cur.close()

    asset_types = {}
    assets = {}
    for conveyor_row in conveyor_rows:

        conveyor = dict(zip(conveyor_keys, conveyor_row))

        asset_type_name = conveyor['conveyor_type'] if conveyor['conveyor_type'] is not None else 'undefined'
        if asset_type_name not in asset_types:
            asset_types[asset_type_name] = {
                'name': asset_type_name,
                'created_at': conveyor['created_at']
            }

        if conveyor['id'] not in asset_facility_area_ids:
            continue

        asset = {
            'id': conveyor['id'],
            'facility_area_id': asset_facility_area_ids[conveyor['id']],
            'type_name': asset_type_name,
            'name': conveyor['name'],
            'created_at': conveyor['created_at'],
            'updated_at': conveyor['updated_at']
        }
        assets[conveyor['id']] = asset

    for facility_area in facility_areas.values():
        connection.execute(insert(facility_areas_table).values(facility_area))

    for asset_type in asset_types.values():
        connection.execute(insert(asset_types_table).values(asset_type))

    for asset in assets.values():
        connection.execute(insert(assets_table).values(asset))

    for component_type in component_types.values():
        connection.execute(insert(component_types_table).values(component_type))

    for component_types in components.values():
        for components in component_types.values():
            connection.execute(insert(components_table).values(components))

    for data_source_type in data_source_types.values():
        connection.execute(insert(data_source_types_table).values(data_source_type))

    for data_source in data_sources.values():
        connection.execute(insert(data_sources_table).values(data_source))

    sequences = [
        'facility_areas_id_seq',
        'assets_id_seq',
        'components_id_seq',
        'data_sources_id_seq'
    ]

    for sequence in sequences:
        table_name = '_'.join(sequence.split('_')[:-2])
        op.execute(f"SELECT setval('{sequence}', (SELECT max(id) FROM {table_name}))")


def downgrade(schema):
    raise Exception('Irreversible migration')
