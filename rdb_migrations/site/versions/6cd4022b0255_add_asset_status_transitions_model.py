"""add_asset_status_transitions_model

Revision ID: 6cd4022b0255
Revises: bb9445b53c9f
Create Date: 2020-10-01 12:31:40.817516

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '6cd4022b0255'
down_revision = 'bb9445b53c9f'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.drop_table('asset_statuses')

    op.create_table(
        'asset_status_transitions',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('asset_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('status', postgresql.ENUM('ON', 'OFF', name='asset_statuses_enum'), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['asset_id'], ['assets.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE asset_status_transitions OWNER to mhs')


def downgrade(schema):
    op.drop_table('asset_status_transitions')

    op.create_table(
        'asset_statuses',
        sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
        sa.Column('asset_id', sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column('status', postgresql.ENUM('ON', 'OFF', 'INVALID', name='conveyor_statuses_enum', create_type=False), autoincrement=False, nullable=True),
        sa.Column('timestamp', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
        sa.Column('created_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
        sa.ForeignKeyConstraint(['asset_id'], ['assets.id'], name='asset_statuses_asset_id_fkey'),
        sa.PrimaryKeyConstraint('id', name='asset_statuses_pkey')
    )
    op.execute('ALTER TABLE asset_statuses OWNER to mhs')
