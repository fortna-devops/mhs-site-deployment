"""fill_metrics_data

Revision ID: 6fb748e2ce8a
Revises: 26d193f28313
Create Date: 2020-10-13 12:52:08.007085

"""
import logging
from datetime import datetime

from alembic import op
import sqlalchemy as sa


logger = logging.getLogger()


# revision identifiers, used by Alembic.
revision = '6fb748e2ce8a'
down_revision = 'c06daaa72c2d'
branch_labels = None
depends_on = None


INSERT_BATCH_SIZE = 1000

VT_METRICS = (
    {
        'name': 'rms_velocity_z',
        'units': 'in/sec',
        'display_name': 'RMS Vibrational Velocity Z',
        'to_display': True,
        'to_analyze': True,
        'min_value': 0.,
        'max_value': 4.,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'temperature',
        'units': '°F',
        'display_name': 'Temperature',
        'to_display': True,
        'to_analyze': True,
        'min_value': 0.,
        'max_value': 300.,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'rms_velocity_x',
        'units': 'in/sec',
        'display_name': 'RMS Vibrational Velocity X',
        'to_display': True,
        'to_analyze': True,
        'min_value': 0.,
        'max_value': 4.,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'peak_acceleration_z',
        'units': 'G',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'peak_acceleration_x',
        'units': 'G',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'peak_frequency_z',
        'units': 'Hz',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'peak_frequency_x',
        'units': 'Hz',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'rms_acceleration_z',
        'units': 'G',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'rms_acceleration_x',
        'units': 'G',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'kurtosis_z',
        'units': '',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'kurtosis_x',
        'units': '',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'crest_acceleration_z',
        'units': '',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'crest_acceleration_x',
        'units': '',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'peak_velocity_z',
        'units': 'in/sec',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'peak_velocity_x',
        'units': 'in/sec',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'hf_rms_acceleration_z',
        'units': 'G',
        'display_name': 'High Frequency RMS Acceleration Z',
        'to_display': False,
        'to_analyze': True,
        'min_value': 0.,
        'max_value': 20.,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'hf_rms_acceleration_x',
        'units': 'G',
        'display_name': 'High Frequency RMS Acceleration X',
        'to_display': False,
        'to_analyze': True,
        'min_value': 0.,
        'max_value': 20.,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
)

AMBIENT_METRICS = (
    {
        'name': 'humidity',
        'units': '%RH',
        'display_name': 'Humidity',
        'to_display': False,
        'to_analyze': False,
        'min_value': 0.,
        'max_value': 100.,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'temperature',
        'units': '°F',
        'display_name': 'Temperature',
        'to_display': False,
        'to_analyze': False,
        'min_value': 0.,
        'max_value': 300.,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
)

VFD_MOTOR_METRICS = (
    {
        'name': 'speed',
        'display_name': 'Speed',
        'units': 'RPM',
        'to_display': True,
        'to_analyze': True,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'current',
        'display_name': 'Current',
        'units': 'A',
        'to_display': True,
        'to_analyze': True,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'voltage',
        'display_name': 'Voltage',
        'units': 'V',
        'to_display': True,
        'to_analyze': True,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'mechanical_power',
        'display_name': 'Mechanical power',
        'units': 'KW',
        'to_display': True,
        'to_analyze': True,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'cos_phi',
        'display_name': 'Cosine Phi',
        'units': '',
        'to_display': True,
        'to_analyze': True,
        'min_value': 0.,
        'max_value': 1.,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
)

VFD_METRICS = (
    {
        'name': 'temperature',
        'units': '°C',
        'display_name': 'Temperature',
        'to_display': True,
        'to_analyze': True,
        'raw_data_table': 'raw_float_data',
        'calculated_data_table': 'calculated_float_data',
    },
    {
        'name': 'operating_time',
        'units': 'Hours',
        'display_name': 'Operating time',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
    },
    {
        'name': 'running_time',
        'units': 'Hours',
        'display_name': 'Running time',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
    },
    {
        'name': 'status_word',
        'units': 'Hours',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_string_data',
    },
    {
        'name': 'warning',
        'units': '',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_string_data',
    },
    {
        'name': 'fault',
        'units': '',
        'display_name': '',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_string_data',
    },
)

# Not sure that we will ever need those metrics but we have them for dhl-miami and fedex-louisville.
# Looks like this data can be useful.
PLC_METRICS = (
    {
        'name': 'belt_speed',
        'units': '',
        'display_name': 'Belt Speed',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_int_data',
    },
    {
        'name': 'vfd_current',
        'units': 'A',
        'display_name': 'Current',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
    },
    {
        'name': 'vfd_frequency',
        'units': 'Hz',
        'display_name': 'Frequency',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
    },
    {
        'name': 'vfd_voltage',
        'units': 'V',
        'display_name': 'Voltage',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_float_data',
    },
    {
        'name': 'vfd_fault_code',
        'units': '',
        'display_name': 'Fault Code',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_int_data',
    },
    {
        'name': 'sorter_induct_rate',
        'units': '',
        'display_name': 'Sorter Induct Rate',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_int_data',
    },
    {
        'name': 'sorter_reject_rate',
        'units': '',
        'display_name': 'Sorter Reject Rate',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_int_data',
    },
    {
        'name': 'status',
        'units': '',
        'display_name': 'Status',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_string_data',
    },
    {
        'name': 'torque',
        'units': '',
        'display_name': 'Torque',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_int_data',
    },
    {
        'name': 'hours_running',
        'units': 'H',
        'display_name': 'Running time',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_int_data',
    },
    {
        'name': 'power',
        'units': '',
        'display_name': 'Power',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_int_data',
    },
    {
        'name': 'rpm',
        'units': 'RPM',
        'display_name': 'Speed',
        'to_display': False,
        'to_analyze': False,
        'raw_data_table': 'raw_int_data',
    },
    # Here also should be "nameplate" fields. Have no idea why do we need to process nameplate data as time series.
    # I decided not to include those fields at all. So those data won't be saved to RDB.
    # List of skipped fields: horsepower_nameplate, voltage_nameplate, run_speed_nameplate, current_nameplate, num_phases_nameplate
)


def build_metrics(ds_id, ds_type, c_type):
    metric_templates = []
    if ds_type == 'qm42vt2':
        metric_templates = VT_METRICS
    elif ds_type == 'm12fth3q':
        metric_templates = AMBIENT_METRICS
    elif ds_type == 'vfd':
        if c_type == 'motor':
            metric_templates = VFD_MOTOR_METRICS
        elif c_type == 'vfd':
            metric_templates = VFD_METRICS
    elif ds_type == 'plc':
        metric_templates = PLC_METRICS

    if not metric_templates:
        logger.warning(f'Can\'t build metrics for data source type: {ds_type}, component type: {c_type}')

    metrics = []
    for metric_template in metric_templates:
        metric = metric_template.copy()

        if not metric.get('display_name'):
            metric['display_name'] = metric['name'].replace('_', ' ').capitalize()

        if 'min_value' not in metric:
            metric['min_value'] = None

        if 'max_value' not in metric:
            metric['max_value'] = None

        if 'raw_data_table' not in metric:
            metric['raw_data_table'] = None

        if 'calculated_data_table' not in metric:
            metric['calculated_data_table'] = None

        metric['data_source_id'] = ds_id
        metric['created_at'] = datetime.utcnow()
        metrics.append(metric)

    return metrics


def upgrade(schema):
    if schema == 'dhl_brescia':
        logger.warning(f"The migration '{revision}' won't be applied to '{schema}'. Manual data coping expected.")
        return

    connection = op.get_bind()
    meta = sa.MetaData(bind=connection, schema=schema)
    meta.reflect(only=(
        'metrics',
        'data_sources',
        'components'
    ))
    metrics_table = sa.Table('metrics', meta)
    data_sources_table = sa.Table('data_sources', meta)
    components_table = sa.Table('components', meta)

    connection.execute(metrics_table.delete())

    query = sa.select([
            data_sources_table.c.id,
            data_sources_table.c.type_name,
            components_table.c.type_name
        ])\
        .where(components_table.c.id == data_sources_table.c.component_id)\
        .order_by(data_sources_table.c.id.asc())

    data_sources_cur = connection.execute(query)
    data_sources_res = data_sources_cur.fetchall()
    data_sources_cur.close()

    metrics = []
    for ds_id, ds_type, c_type in data_sources_res:
        metrics.extend(build_metrics(ds_id, ds_type, c_type))

    logger.info(f'{len(metrics)} new metrics to insert')
    for i in range(0, len(metrics), INSERT_BATCH_SIZE):
        batch = metrics[i:i + INSERT_BATCH_SIZE]
        logger.info(f'Inserting metrics from {i} to {i + len(batch)}')
        connection.execute(metrics_table.insert(), batch)


def downgrade(schema):
    raise Exception("Irreversible migration")
