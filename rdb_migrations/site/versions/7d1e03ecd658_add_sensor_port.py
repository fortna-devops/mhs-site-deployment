"""add_sensor_port

Revision ID: 7d1e03ecd658
Revises: 2b628252dc11
Create Date: 2019-11-25 18:22:46.645811

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7d1e03ecd658'
down_revision = '2b628252dc11'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.add_column('sensors', sa.Column('port', sa.String(), nullable=True))
    op.drop_constraint('sensors_gateway_name_name_key', 'sensors', type_='unique')
    op.create_unique_constraint(None, 'sensors', ['gateway_name', 'port', 'name'])


def downgrade(schema):
    op.drop_constraint(None, 'sensors', type_='unique')
    op.create_unique_constraint('sensors_gateway_name_name_key', 'sensors', ['gateway_name', 'name'])
    op.drop_column('sensors', 'port')
