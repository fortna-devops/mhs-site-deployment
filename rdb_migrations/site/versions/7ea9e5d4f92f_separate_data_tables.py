"""separate_data_tables

Revision ID: 7ea9e5d4f92f
Revises: 812986205a0a
Create Date: 2020-09-21 17:23:41.031370

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import column, or_


# revision identifiers, used by Alembic.
revision = '7ea9e5d4f92f'
down_revision = '812986205a0a'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.alter_column('metrics', 'data_table', new_column_name='raw_data_table', nullable=True)
    op.add_column('metrics', sa.Column('calculated_data_table', sa.String(), nullable=True))

    op.create_check_constraint(
        'data_table_check', 'metrics',
        or_(
            column('raw_data_table').in_(('raw_float_data', 'raw_int_data', 'raw_string_data')),
            column('calculated_data_table').in_(('calculated_float_data', 'calculated_int_data', 'calculated_string_data'))
        )
    )


def downgrade(schema):
    op.alter_column('metrics', 'raw_data_table', new_column_name='data_table', nullable=False)
    op.drop_column('metrics', 'calculated_data_table')

    op.drop_constraint('data_table_check', 'metrics')
