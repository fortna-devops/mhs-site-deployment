"""add_display_analyze_flags

Revision ID: 812986205a0a
Revises: dd84421bf253
Create Date: 2020-09-21 13:37:52.169822

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '812986205a0a'
down_revision = 'dd84421bf253'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.add_column('alarm_types', sa.Column('to_display', sa.Boolean(), server_default='TRUE', nullable=False))
    op.add_column('metrics', sa.Column('to_analyze', sa.Boolean(), server_default='TRUE', nullable=False))
    op.add_column('metrics', sa.Column('to_display', sa.Boolean(), server_default='TRUE', nullable=False))


def downgrade(schema):
    op.drop_column('metrics', 'to_display')
    op.drop_column('metrics', 'to_analyze')
    op.drop_column('alarm_types', 'to_display')
