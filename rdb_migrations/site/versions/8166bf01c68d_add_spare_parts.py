"""add_spare_parts

Revision ID: 8166bf01c68d
Revises: 827430474c43
Create Date: 2019-11-19 14:28:56.300648

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM

# revision identifiers, used by Alembic.
revision = '8166bf01c68d'
down_revision = '827430474c43'
branch_labels = None
depends_on = None


def upgrade(schema):

    equipments_enum = ENUM(
        'M',
        'G',
        'B',
        'PLC',
        name='equipment_enum',
        create_type=False
    )

    op.create_table(
        'spare_parts_results',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('mean_time_between_failure', sa.Integer(), nullable=False),
        sa.Column('operating_time_of_interest', sa.String(), nullable=False),
        sa.Column('num_units', sa.Integer(), nullable=False),
        sa.Column('confidence_level', sa.Enum('low_risk', 'medium_risk', 'high_risk', name='spare_parts_confidence_level_enum'), nullable=False),
        sa.Column('equipment_type', equipments_enum, nullable=True),
        sa.Column('model', sa.String(), nullable=True),
        sa.Column('manufacture', sa.String(), nullable=True),
        sa.Column('part_costs', sa.Integer(), nullable=True),
        sa.Column('user_name', sa.String(), nullable=True),
        sa.Column('result', sa.JSON(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE spare_parts_results OWNER to mhs')
    op.execute('ALTER TYPE spare_parts_confidence_level_enum OWNER to mhs')


def downgrade(schema):
    op.drop_table('spare_parts_results')
    op.execute('DROP TYPE spare_parts_confidence_level_enum')
