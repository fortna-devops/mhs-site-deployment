"""add_asset_health

Revision ID: 827430474c43
Revises: 8c2783165c65
Create Date: 2019-11-14 15:00:39.886252

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '827430474c43'
down_revision = '8c2783165c65'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.create_table(
        'asset_health',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('asset_health', sa.Float(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.PrimaryKeyConstraint('id'),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.UniqueConstraint('sensor_id', 'timestamp')
    )

    op.execute('ALTER TABLE asset_health OWNER to mhs')


def downgrade(schema):
    op.drop_table('asset_health')
