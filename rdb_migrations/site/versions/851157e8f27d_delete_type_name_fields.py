"""delete_type_name_fields

Revision ID: 851157e8f27d
Revises: 5746ae696d56
Create Date: 2020-11-17 12:38:53.321757

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '851157e8f27d'
down_revision = '5746ae696d56'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.drop_column('assets', 'type_name')
    op.drop_column('components', 'type_name')
    op.drop_column('data_sources', 'type_name')


def downgrade(schema):
    op.add_column('data_sources', sa.Column('type_name', sa.VARCHAR(), autoincrement=False, nullable=False))
    op.add_column('components', sa.Column('type_name', sa.VARCHAR(), autoincrement=False, nullable=False))
    op.add_column('assets', sa.Column('type_name', sa.VARCHAR(), autoincrement=False, nullable=False))
