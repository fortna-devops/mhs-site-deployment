"""update_data_index

Revision ID: 854324df89f8
Revises: 86a8eb245263
Create Date: 2020-03-24 12:48:32.223886

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '854324df89f8'
down_revision = '86a8eb245263'
branch_labels = None
depends_on = None


def upgrade(schema):
    # Drop Sensor data
    op.drop_index('sensor_data_minute_sensor_id_timestamp_idx', table_name='sensor_data_minute')
    op.drop_index('sensor_data_hour_sensor_id_timestamp_idx', table_name='sensor_data_hour')
    op.drop_constraint('sensor_data_hour_sensor_id_timestamp_key', 'sensor_data_hour', type_='unique')

    # Drop Predicted data
    op.drop_index('predicted_data_minute_sensor_id_timestamp_idx', table_name='predicted_data_minute')
    op.drop_index('predicted_data_hour_sensor_id_timestamp_idx', table_name='predicted_data_hour')
    op.drop_constraint('predicted_data_hour_sensor_id_timestamp_key', 'predicted_data_hour', type_='unique')

    # Create Sensor data
    op.create_index('sensor_data_hour_sensor_id_timestamp_desc_index', 'sensor_data_hour',
                    ['sensor_id', sa.text('timestamp DESC')],
                    unique=True)
    op.create_index('sensor_data_minute_sensor_id_timestamp_desc_index', 'sensor_data_minute',
                    ['sensor_id', sa.text('timestamp DESC')],
                    unique=False)

    # Create Predicted data
    op.create_index('predicted_data_hour_sensor_id_timestamp_desc_index', 'predicted_data_hour',
                    ['sensor_id', sa.text('timestamp DESC')],
                    unique=True)
    op.create_index('predicted_data_minute_sensor_id_timestamp_desc_index', 'predicted_data_minute',
                    ['sensor_id', sa.text('timestamp DESC')],
                    unique=False)


def downgrade(schema):
    raise Exception("Irreversible migration")
