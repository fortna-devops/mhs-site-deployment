"""add_plc_dynamic_data_table

Revision ID: 86a8eb245263
Revises: c3b47208becc
Create Date: 2020-02-17 11:01:33.267983

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '86a8eb245263'
down_revision = 'c3b47208becc'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.create_table('plc_data_dynamic',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('tag', sa.String(), nullable=False),
        sa.Column('float_value', sa.Float(), nullable=True),
        sa.Column('string_value', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.PrimaryKeyConstraint('id'),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], )
    )
    op.execute('ALTER TABLE plc_data_dynamic OWNER to mhs')


def downgrade(schema):
    op.drop_table('plc_data_dynamic')
