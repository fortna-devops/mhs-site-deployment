"""add_conveyor_type

Revision ID: 86fdb8ae47a5
Revises: 240090c1c1de
Create Date: 2019-12-11 14:10:59.486198

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM


# revision identifiers, used by Alembic.
revision = '86fdb8ae47a5'
down_revision = '240090c1c1de'
branch_labels = None
depends_on = None


def upgrade(schema):

    conveyor_types_enum = ENUM(
        'belted_roller_bed_front',
        'belted_roller_bed_center',
        'belted_slider_bed_front',
        'belted_slider_bed_center',
        'live_roller_bed_straight',
        'live_roller_bed_curve',
        'gapper_single_motor',
        'gapper_multi_motor',
        'powered_belt_curve_underslung',
        'powered_belt_curve_shaft_mounted',
        'slat_sorter',
        'strip_belt_sorter',
        'spiral_tower',
        name='conveyor_types_enum',
        create_type=False
    )
    conveyor_types_enum.create(op.get_bind())
    op.execute('ALTER TYPE conveyor_types_enum OWNER to mhs')

    op.add_column('conveyors', sa.Column('conveyor_type', conveyor_types_enum, nullable=True))
    op.add_column('conveyors', sa.Column('nameplate_info', sa.JSON(), nullable=True))
    op.add_column('sensors', sa.Column('nameplate_info', sa.JSON(), nullable=True))


def downgrade(schema):
    op.drop_column('sensors', 'nameplate_info')
    op.drop_column('conveyors', 'nameplate_info')
    op.drop_column('conveyors', 'conveyor_type')

    op.execute('DROP TYPE conveyor_types_enum')
