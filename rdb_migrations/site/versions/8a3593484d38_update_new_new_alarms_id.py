"""update_new_new_alarms_id

Revision ID: 8a3593484d38
Revises: 3f3c3c14faab
Create Date: 2021-01-21 15:08:54.400497

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8a3593484d38'
down_revision = '3f3c3c14faab'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.add_column('new_new_alarms', sa.Column('static_description', sa.String(), nullable=True))

    op.alter_column('new_new_alarms', 'id', type_=sa.BigInteger)
    op.execute('ALTER SEQUENCE new_new_alarms_id_seq AS BIGINT')


def downgrade(schema):
    raise Exception('Irreversible migration')
