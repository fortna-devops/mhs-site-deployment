"""fill_alarms_alarm_histories_tables

Revision ID: 8abd4c462bb2
Revises: 6227ac656a6a
Create Date: 2020-10-15 15:28:28.815844

"""
from alembic import op
from sqlalchemy import insert
from datetime import datetime

import logging
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8abd4c462bb2'
down_revision = '3ca1b2e9dd92'
branch_labels = None
depends_on = None


logger = logging.getLogger()


def upgrade(schema):

    alarm_types = {
        'iso_spike': False,
        'iso_persistent': True,
        'error': True,
        'warning': True
    }

    op.execute('DELETE FROM alarm_types')

    connection = op.get_bind()
    meta = sa.MetaData(bind=connection, schema=schema)
    meta.reflect(only=(
        'alarms',
        'metrics',
        'alarm_types',
        'new_alarms'
    ))

    alarms_table = sa.Table('alarms', meta)
    alarm_types_table = sa.Table('alarm_types', meta)
    metrics_table = sa.Table('metrics', meta)
    new_alarms_table = sa.Table('new_alarms', meta)

    cur = connection.execute(alarms_table.select().order_by(alarms_table.c.id.asc()))
    alarm_keys = cur.keys()
    alarm_rows = cur.fetchall()
    cur.close()

    data_source_ids = set()
    alarms = []
    for alarm_row in alarm_rows:

        alarm = dict(zip(alarm_keys, alarm_row))

        if alarm['type'].lower() not in alarm_types.keys():
            logger.warning(f"Unexpected '{alarm['type']}' alarm type found for '{alarm['id']}' alarm.")
            continue

        data_source_ids.add(alarm['sensor_id'])
        alarms.append(alarm)

    cur = connection.execute(metrics_table.select().where(metrics_table.c.data_source_id.in_(data_source_ids)))
    metric_keys = cur.keys()
    metric_rows = cur.fetchall()
    cur.close()

    metric_ids = {}
    for metric_row in metric_rows:

        metric = dict(zip(metric_keys, metric_row))
        if metric['data_source_id'] not in metric_ids:
            metric_ids[metric['data_source_id']] = {}

        if metric['name'] not in metric_ids[metric['data_source_id']]:
            metric_ids[metric['data_source_id']][metric['name']] = metric['id']

    new_alarm_ids = set()
    new_alarms = []
    for alarm in alarms:

        if alarm['sensor_id'] not in metric_ids:
            logger.warning(f"Sensor id '{alarm['sensor_id']}' not found for '{alarm['id']}' alarm.")
            continue

        if alarm['metric'] not in metric_ids[alarm['sensor_id']]:
            logger.warning(f"Metric name '{alarm['metric']}' not found for '{alarm['id']}' alarm.")
            continue

        metric_id = metric_ids[alarm['sensor_id']][alarm['metric']]

        new_alarm = {
            'id': alarm['id'],
            'metric_id': metric_id,
            'data_timestamp': alarm['data_timestamp'],
            'severity': alarm['severity'],
            'type_name': alarm['type'].lower(),
            'value': alarm['value'],
            'criteria': alarm['criteria'],
            'description': alarm['description'],
            'created_at': alarm['created_at'],
            'updated_at': alarm['updated_at']
        }
        new_alarm_ids.add(str(new_alarm['id']))
        new_alarms.append(new_alarm)

    connection.execute(
        insert(alarm_types_table).values(
            [
                {
                    'name': name,
                    'to_display': to_display,
                    'created_at': datetime.utcnow()
                }
                for name, to_display in alarm_types.items()
            ]
        )
    )

    if new_alarms:
        connection.execute(insert(new_alarms_table).values(new_alarms))

    if new_alarm_ids:
        connection.execute(f"""
            INSERT INTO new_alarm_histories
            SELECT
                id, alarm_id, data_timestamp, severity, value, criteria, created_at
            FROM
                alarms_history
            WHERE alarm_id IN ({','.join(new_alarm_ids)})""")

    sequences = [
        'new_alarms_id_seq',
        'new_alarm_histories_id_seq'
    ]

    for sequence in sequences:
        table_name = '_'.join(sequence.split('_')[:-2])
        op.execute(f"SELECT setval('{sequence}', (SELECT max(id) FROM {table_name}))")


def downgrade(schema):
    raise Exception("Irreversible migration")
