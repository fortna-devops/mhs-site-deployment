"""add_criticality

Revision ID: 8c2783165c65
Revises: c12a706199f2
Create Date: 2019-11-05 15:03:47.402983

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM


# revision identifiers, used by Alembic.
revision = '8c2783165c65'
down_revision = 'c12a706199f2'
branch_labels = None
depends_on = None


def upgrade(schema):
    criticality_enum = ENUM(
        'low',
        'medium',
        'high',
        name='criticality_enum',
        create_type=False
    )
    criticality_enum.create(op.get_bind())
    op.execute('ALTER TYPE criticality_enum OWNER to mhs')

    op.add_column('conveyors', sa.Column('criticality', criticality_enum, nullable=True))
    op.add_column('sensors', sa.Column('criticality', criticality_enum, nullable=True))


def downgrade(schema):
    op.drop_column('sensors', 'criticality')
    op.drop_column('conveyors', 'criticality')

    op.execute('DROP TYPE criticality_enum')
