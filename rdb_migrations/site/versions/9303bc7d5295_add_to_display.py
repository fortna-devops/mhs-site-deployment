"""add_to_display

Revision ID: 9303bc7d5295
Revises: 94fb0b5fc739
Create Date: 2020-10-02 17:13:31.337755

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9303bc7d5295'
down_revision = '94fb0b5fc739'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.add_column('assets', sa.Column('to_display', sa.Boolean(), server_default='TRUE', nullable=False))
    op.add_column('components', sa.Column('to_display', sa.Boolean(), server_default='TRUE', nullable=False))
    op.add_column('facility_areas', sa.Column('to_display', sa.Boolean(), server_default='TRUE', nullable=False))


def downgrade(schema):
    op.drop_column('facility_areas', 'to_display')
    op.drop_column('components', 'to_display')
    op.drop_column('assets', 'to_display')
