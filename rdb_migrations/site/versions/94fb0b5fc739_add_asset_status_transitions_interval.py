"""add_asset_status_transitions_interval

Revision ID: 94fb0b5fc739
Revises: 6cd4022b0255
Create Date: 2020-10-02 14:16:08.864144

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '94fb0b5fc739'
down_revision = '6cd4022b0255'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.add_column('asset_status_transitions', sa.Column('interval', sa.Interval(), server_default='0', nullable=False))
    op.create_index('asset_status_transitions_asset_id_status_timestamp_desc_index', 'asset_status_transitions', ['asset_id', 'status', sa.text('timestamp DESC')], unique=False)


def downgrade(schema):
    op.drop_index('asset_status_transitions_asset_id_status_timestamp_desc_index', table_name='asset_status_transitions')
    op.drop_column('asset_status_transitions', 'interval')
