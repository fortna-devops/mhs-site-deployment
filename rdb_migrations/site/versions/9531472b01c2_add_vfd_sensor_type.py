"""add_vfd_sensor_type

Revision ID: 9531472b01c2
Revises: 1a71ecdb12da
Create Date: 2020-08-03 13:38:38.112797

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9531472b01c2'
down_revision = '1a71ecdb12da'
branch_labels = None
depends_on = None


def upgrade(schema):
    with op.get_context().autocommit_block():
        op.execute("ALTER TYPE sensors_type_enum ADD VALUE 'vfd'")


def downgrade(schema):
    raise Exception('Irreversible migration')
