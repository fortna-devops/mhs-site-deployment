"""add_cmms_reference_column_sensors_conveyors_tables

Revision ID: 95a548ad2176
Revises: 86fdb8ae47a5
Create Date: 2020-01-16 16:13:00.389009

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '95a548ad2176'
down_revision = '86fdb8ae47a5'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.add_column('conveyors', sa.Column('cmms_reference', sa.String(), nullable=True))
    op.create_unique_constraint(None, 'conveyors', ['cmms_reference'])
    op.add_column('sensors', sa.Column('cmms_reference', sa.String(), nullable=True))
    op.create_unique_constraint(None, 'sensors', ['cmms_reference'])


def downgrade(schema):
    op.drop_constraint(None, 'sensors', type_='unique')
    op.drop_column('sensors', 'cmms_reference')
    op.drop_constraint(None, 'conveyors', type_='unique')
    op.drop_column('conveyors', 'cmms_reference')
