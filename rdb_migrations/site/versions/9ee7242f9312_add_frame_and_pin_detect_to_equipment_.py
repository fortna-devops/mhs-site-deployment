"""add_frame_and_pin_detect_to_equipment_enum

Revision ID: 9ee7242f9312
Revises: 0cc1213be35e
Create Date: 2020-02-11 12:32:53.653473

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9ee7242f9312'
down_revision = '0cc1213be35e'
branch_labels = None
depends_on = None


def upgrade(schema):
    with op.get_context().autocommit_block():
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'FRAME'")
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'PIN_DIVERT'")


def downgrade(schema):
    raise Exception("Irreversible migration")
