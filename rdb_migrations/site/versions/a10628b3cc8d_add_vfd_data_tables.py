"""add_vfd_data_tables

Revision ID: a10628b3cc8d
Revises: 854324df89f8
Create Date: 2020-08-03 12:05:23.943554

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a10628b3cc8d'
down_revision = '854324df89f8'
branch_labels = None
depends_on = None


def upgrade(schema):

    op.create_table(
        'vfd_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('speed', sa.REAL(), nullable=True),
        sa.Column('current', sa.REAL(), nullable=True),
        sa.Column('voltage', sa.REAL(), nullable=True),
        sa.Column('mechanical_power', sa.REAL(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE vfd_data OWNER to mhs')

    op.create_index('vfd_data_sensor_id_timestamp_desc_index',
                    'vfd_data', ['sensor_id', sa.text('timestamp DESC')], unique=False)

    op.create_table(
        'vfd_lifetime_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('operating_time', sa.REAL(), nullable=True),
        sa.Column('running_time', sa.REAL(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE vfd_lifetime_data OWNER to mhs')

    op.create_index('vfd_lifetime_data_sensor_id_timestamp_desc_index',
                    'vfd_lifetime_data', ['sensor_id', sa.text('timestamp DESC')], unique=False)

    op.create_table(
        'vfd_temperature_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('temperature', sa.REAL(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE vfd_temperature_data OWNER to mhs')

    op.create_index('vfd_temperature_data_sensor_id_timestamp_desc_index',
                    'vfd_temperature_data', ['sensor_id', sa.text('timestamp DESC')], unique=False)

    op.add_column('plc_data_dynamic', sa.Column('integer_value', sa.Integer(), nullable=True))


def downgrade(schema):
    raise Exception('Irreversible migration')
