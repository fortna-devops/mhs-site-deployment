"""Brescia-static-data

Revision ID: aa8b140369d8
Revises: 4153162ed84f
Create Date: 2020-08-21 12:01:33.257028

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'aa8b140369d8'
down_revision = '4153162ed84f'
branch_labels = None
depends_on = None


def upgrade(schema):
    with op.get_context().autocommit_block():
        op.execute("ALTER TYPE sensors_type_enum ADD VALUE 'strain_gauge'")
        op.execute("ALTER TYPE sensors_type_enum ADD VALUE 'cam_maintenance'")
        op.execute("ALTER TYPE sensors_type_enum ADD VALUE 'cam_vision'")
        op.execute("ALTER TYPE sensors_type_enum ADD VALUE 'acoustic'")
        op.execute("ALTER TYPE sensors_type_enum ADD VALUE 'proximity'")
        op.execute("ALTER TYPE sensors_type_enum ADD VALUE 'air_pressure'")
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'VFD_WITH_MOTOR'")
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'VFD_AB_525'")
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'VFD_AB_755'")
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'FEED_AIR'")
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'PHOTO_EYE'")
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'CHAIN'")
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'PANEL'")
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'AMBIENT'")
        op.execute("ALTER TYPE conveyor_types_enum ADD VALUE 'loop_sorter'")
        op.execute("ALTER TYPE conveyor_types_enum ADD VALUE 'motor_driven_roller'")
        op.execute("ALTER TYPE conveyor_types_enum ADD VALUE 'singulator'")
        op.execute("ALTER TYPE conveyor_types_enum ADD VALUE 'transfer_conveyor'")
        op.execute("ALTER TYPE conveyor_types_enum ADD VALUE 'transnorm_curve'")
        op.execute("ALTER TYPE conveyor_types_enum ADD VALUE 'agv_mir500'")
        op.execute("ALTER TYPE conveyor_types_enum ADD VALUE 'gravity_conveyor'")
        op.execute("ALTER TYPE conveyor_types_enum ADD VALUE 'line_sorter'")
        op.execute("ALTER TYPE conveyor_types_enum ADD VALUE 'roller_spur_infeeder'")
        op.execute("ALTER TYPE conveyor_types_enum ADD VALUE 'switch_wheel_sorter'")
        op.execute("ALTER TYPE conveyor_types_enum ADD VALUE 'belt_conveyor_scale'")

def downgrade(schema):
    raise Exception('Irreversible migration')
