"""update_enums_for_camera_data

Revision ID: abe38fd0154e
Revises: b7f274f57f19
Create Date: 2019-11-21 18:59:55.117672

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'abe38fd0154e'
down_revision = 'b7f274f57f19'
branch_labels = None
depends_on = None


def upgrade(schema):
    with op.get_context().autocommit_block():
        op.execute("ALTER TYPE alarms_type_enum ADD VALUE 'CAMERA'")
        op.execute("ALTER TYPE alarms_metric_enum ADD VALUE 'belt_gap'")
        op.execute("ALTER TYPE equipment_enum ADD VALUE 'BELT'")
        op.execute("ALTER TYPE thresholds_metric_enum ADD VALUE 'belt_gap'")


def downgrade(schema):
    raise Exception("Irreversible migration")
