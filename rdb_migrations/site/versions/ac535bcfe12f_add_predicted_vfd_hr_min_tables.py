"""add_predicted_vfd_hr_min_tables

Revision ID: ac535bcfe12f
Revises: c71e98343ee9
Create Date: 2020-08-23 18:27:33.617343

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'ac535bcfe12f'
down_revision = 'c71e98343ee9'
branch_labels = None
depends_on = None


def upgrade(schema):

    # Drop old vfd predicted data tables.
    op.drop_index('predicted_vfd_data_sensor_id_timestamp_desc_index',
                  table_name='predicted_vfd_data')
    op.drop_table('predicted_vfd_data')
    op.drop_index('predicted_vfd_temperature_data_sensor_id_timestamp_desc_index',
                  table_name='predicted_vfd_temperature_data')
    op.drop_table('predicted_vfd_temperature_data')
    
    # Create predicted_vfd_data_hour table.
    op.create_table('predicted_vfd_data_hour',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('speed', sa.REAL(), nullable=True),
        sa.Column('speed_residuals', sa.REAL(), nullable=True),
        sa.Column('current', sa.REAL(), nullable=True),
        sa.Column('current_residuals', sa.REAL(), nullable=True),
        sa.Column('voltage', sa.REAL(), nullable=True),
        sa.Column('voltage_residuals', sa.REAL(), nullable=True),
        sa.Column('mechanical_power', sa.REAL(), nullable=True),
        sa.Column('mechanical_power_residuals', sa.REAL(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE predicted_vfd_data_hour OWNER to mhs')
    op.create_index(
        'predicted_vfd_data_hour_sensor_id_timestamp_desc_index',
        'predicted_vfd_data_hour', ['sensor_id', sa.text('timestamp DESC')], unique=True
    )

    # Create predicted_vfd_data_minute table.
    op.create_table('predicted_vfd_data_minute',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('speed', sa.REAL(), nullable=True),
        sa.Column('speed_residuals', sa.REAL(), nullable=True),
        sa.Column('current', sa.REAL(), nullable=True),
        sa.Column('current_residuals', sa.REAL(), nullable=True),
        sa.Column('voltage', sa.REAL(), nullable=True),
        sa.Column('voltage_residuals', sa.REAL(), nullable=True),
        sa.Column('mechanical_power', sa.REAL(), nullable=True),
        sa.Column('mechanical_power_residuals', sa.REAL(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE predicted_vfd_data_minute OWNER to mhs')
    op.create_index(
        'predicted_vfd_data_minute_sensor_id_timestamp_desc_index',
        'predicted_vfd_data_minute', ['sensor_id', sa.text('timestamp DESC')], unique=False)

    # Create predicted_vfd_temp_data_hour table.
    op.create_table('predicted_vfd_temp_data_hour',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('temperature', sa.REAL(), nullable=True),
        sa.Column('temperature_residuals', sa.REAL(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE predicted_vfd_temp_data_hour OWNER to mhs')
    op.create_index(
        'predicted_vfd_temp_data_hour_sensor_id_timestamp_desc_index',
        'predicted_vfd_temp_data_hour', ['sensor_id', sa.text('timestamp DESC')], unique=True
    )

    # Create predicted_vfd_temp_data_minute table.
    op.create_table('predicted_vfd_temp_data_minute',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('temperature', sa.REAL(), nullable=True),
        sa.Column('temperature_residuals', sa.REAL(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE predicted_vfd_temp_data_minute OWNER to mhs')
    op.create_index(
        'predicted_vfd_temp_data_minute_sensor_id_timestamp_desc_index',
        'predicted_vfd_temp_data_minute', ['sensor_id', sa.text('timestamp DESC')], unique=False
    )


def downgrade(schema):
    op.create_table('predicted_vfd_temperature_data',
        sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
        sa.Column('sensor_id', sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column('timestamp', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
        sa.Column('temperature', sa.REAL(), autoincrement=False, nullable=True),
        sa.Column('temperature_residuals', sa.REAL(), autoincrement=False, nullable=True),
        sa.Column('created_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'],
            name='predicted_vfd_temperature_data_sensor_id_fkey'
        ),
        sa.PrimaryKeyConstraint('id', name='predicted_vfd_temperature_data_pkey')
    )
    op.create_index(
        'predicted_vfd_temperature_data_sensor_id_timestamp_desc_index',
        'predicted_vfd_temperature_data', ['sensor_id', 'timestamp'], unique=False)
    op.create_table('predicted_vfd_data',
        sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
        sa.Column('sensor_id', sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column('timestamp', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
        sa.Column('speed', sa.REAL(), autoincrement=False, nullable=True),
        sa.Column('speed_residuals', sa.REAL(), autoincrement=False, nullable=True),
        sa.Column('current', sa.REAL(), autoincrement=False, nullable=True),
        sa.Column('current_residuals', sa.REAL(), autoincrement=False, nullable=True),
        sa.Column('voltage', sa.REAL(), autoincrement=False, nullable=True),
        sa.Column('voltage_residuals', sa.REAL(), autoincrement=False, nullable=True),
        sa.Column('mechanical_power', sa.REAL(), autoincrement=False, nullable=True),
        sa.Column('mechanical_power_residuals', sa.REAL(), autoincrement=False, nullable=True),
        sa.Column('created_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], name='predicted_vfd_data_sensor_id_fkey'),
        sa.PrimaryKeyConstraint('id', name='predicted_vfd_data_pkey')
    )
    op.create_index(
        'predicted_vfd_data_sensor_id_timestamp_desc_index',
        'predicted_vfd_data', ['sensor_id', 'timestamp'], unique=False)
    op.drop_index('predicted_vfd_temp_data_minute_sensor_id_timestamp_desc_index',
        table_name='predicted_vfd_temp_data_minute')
    op.drop_table('predicted_vfd_temp_data_minute')
    op.drop_index('predicted_vfd_temp_data_hour_sensor_id_timestamp_desc_index',
        table_name='predicted_vfd_temp_data_hour')
    op.drop_table('predicted_vfd_temp_data_hour')
    op.drop_index('predicted_vfd_data_minute_sensor_id_timestamp_desc_index',
        table_name='predicted_vfd_data_minute')
    op.drop_table('predicted_vfd_data_minute')
    op.drop_index('predicted_vfd_data_hour_sensor_id_timestamp_desc_index',
        table_name='predicted_vfd_data_hour')
    op.drop_table('predicted_vfd_data_hour')
