"""add_camera_data

Revision ID: b7f274f57f19
Revises: 8166bf01c68d
Create Date: 2019-11-21 18:02:14.193259

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b7f274f57f19'
down_revision = '8166bf01c68d'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.create_table(
        'camera_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('port', sa.String(), nullable=False),
        sa.Column('belt_gap', sa.Float(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.PrimaryKeyConstraint('id'),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.UniqueConstraint('sensor_id', 'timestamp')
    )
    op.execute('ALTER TABLE camera_data OWNER to mhs')


def downgrade(schema):
    op.drop_table('camera_data')
