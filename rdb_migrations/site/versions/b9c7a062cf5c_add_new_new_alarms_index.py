"""add_new_new_alarms_index

Revision ID: b9c7a062cf5c
Revises: 8a3593484d38
Create Date: 2021-02-03 13:06:05.413454

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b9c7a062cf5c'
down_revision = '8a3593484d38'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.create_index('new_new_alarms_tsrange_metric_id_type_name_started_at_index', 'new_new_alarms',
                    [sa.text("tsrange(started_at, ended_at, '[]')"), 'metric_id', 'type_name', 'started_at'],
                    postgresql_using='gist')


def downgrade(schema):
    raise Exception('Irreversible migration')
