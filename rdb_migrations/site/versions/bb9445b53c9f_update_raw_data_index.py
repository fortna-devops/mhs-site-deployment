"""update_raw_data_index

Revision ID: bb9445b53c9f
Revises: f1fb38fa092c
Create Date: 2020-09-24 14:52:52.287674

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bb9445b53c9f'
down_revision = 'f1fb38fa092c'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.drop_index('raw_float_data_metric_id_timestamp_minute_index', table_name='raw_float_data')
    op.drop_index('raw_float_data_metric_id_timestamp_hour_index', table_name='raw_float_data')

    op.drop_index('raw_int_data_metric_id_timestamp_minute_index', table_name='raw_int_data')
    op.drop_index('raw_int_data_metric_id_timestamp_hour_index', table_name='raw_int_data')

    op.drop_index('raw_string_data_metric_id_timestamp_minute_index', table_name='raw_string_data')
    op.drop_index('raw_string_data_metric_id_timestamp_hour_index', table_name='raw_string_data')


    op.create_index('raw_float_data_metric_id_timestamp_minute_flags_index', 'raw_float_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)"), 'flags'])
    op.create_index('raw_float_data_metric_id_timestamp_hour_flags_index', 'raw_float_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)"), 'flags'])

    op.create_index('raw_int_data_metric_id_timestamp_minute_flags_index', 'raw_int_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)"), 'flags'])
    op.create_index('raw_int_data_metric_id_timestamp_hour_flags_index', 'raw_int_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)"), 'flags'])

    op.create_index('raw_string_data_metric_id_timestamp_minute_flags_index', 'raw_string_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)"), 'flags'])
    op.create_index('raw_string_data_metric_id_timestamp_hour_flags_index', 'raw_string_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)"), 'flags'])


def downgrade(schema):
    op.drop_index('raw_float_data_metric_id_timestamp_minute_flags_index', table_name='raw_float_data')
    op.drop_index('raw_float_data_metric_id_timestamp_hour_flags_index', table_name='raw_float_data')

    op.drop_index('raw_int_data_metric_id_timestamp_minute_flags_index', table_name='raw_int_data')
    op.drop_index('raw_int_data_metric_id_timestamp_hour_flags_index', table_name='raw_int_data')

    op.drop_index('raw_string_data_metric_id_timestamp_minute_flags_index', table_name='raw_string_data')
    op.drop_index('raw_string_data_metric_id_timestamp_hour_flags_index', table_name='raw_string_data')

    op.create_index('raw_float_data_metric_id_timestamp_minute_flags_index', 'raw_float_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('raw_float_data_metric_id_timestamp_hour_flags_index', 'raw_float_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])

    op.create_index('raw_int_data_metric_id_timestamp_minute_flags_index', 'raw_int_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('raw_int_data_metric_id_timestamp_hour_flags_index', 'raw_int_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])

    op.create_index('raw_string_data_metric_id_timestamp_minute_flags_index', 'raw_string_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('raw_string_data_metric_id_timestamp_hour_flags_index', 'raw_string_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])