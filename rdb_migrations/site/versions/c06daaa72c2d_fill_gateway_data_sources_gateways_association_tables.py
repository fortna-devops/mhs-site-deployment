"""fill_gateway_data_sources_gateways_association_tables

Revision ID: c06daaa72c2d
Revises: 8abd4c462bb2
Create Date: 2020-10-21 13:48:11.198250

"""
import logging
from datetime import datetime

from alembic import op
import sqlalchemy as sa
from sqlalchemy import insert


logger = logging.getLogger()


# revision identifiers, used by Alembic.
revision = 'c06daaa72c2d'
down_revision = '6227ac656a6a'
branch_labels = None
depends_on = None


def upgrade(schema):

    if schema == 'dhl_brescia':
        logger.warning(f"The migration '{revision}' won't be applied to '{schema}'. Manual data coping expected.")
        return

    op.execute('DELETE FROM new_gateways')

    connection = op.get_bind()
    meta = sa.MetaData(bind=connection, schema=schema)
    meta.reflect(only=(
        'gateways',
        'sensors',
        'data_sources',
        'data_sources_gateways_association',
        'new_gateways',
    ))

    gateways_table = sa.Table('gateways', meta)
    sensors_table = sa.Table('sensors', meta)
    data_sources_table = sa.Table('data_sources', meta)
    data_sources_gateways_association_table = sa.Table('data_sources_gateways_association', meta)
    new_gateways_table = sa.Table('new_gateways', meta)

    cur = connection.execute(gateways_table.select().order_by(gateways_table.c.name.asc()))
    gateway_keys = cur.keys()
    gateway_rows = cur.fetchall()
    cur.close()

    new_gateway_id = 0
    new_gateways = {}
    for gateway_row in gateway_rows:

        gateway = dict(zip(gateway_keys, gateway_row))
        new_gateway_id += 1
        new_gateway = {
            'id': new_gateway_id,
            'name': gateway['name'],
            'description': gateway['description'],
            'cognito_client_id': gateway['cognito_client_id'],
            'created_at': gateway['created_at']
        }

        new_gateways[gateway['name']] = new_gateway

    cur = connection.execute(data_sources_table.select())
    data_source_keys = cur.keys()
    data_source_rows = cur.fetchall()
    cur.close()

    data_source_ids = set()
    for data_source_row in data_source_rows:

        data_source = dict(zip(data_source_keys, data_source_row))
        data_source_ids.add(data_source['id'])

    cur = connection.execute(sensors_table.select().order_by(sensors_table.c.id.asc()))
    sensor_keys = cur.keys()
    sensor_rows = cur.fetchall()
    cur.close()

    data_sources_gateways_associations = []
    for sensor_row in sensor_rows:

        sensor = dict(zip(sensor_keys, sensor_row))
        if sensor['id'] not in data_source_ids:
            logger.warning(f"Sensor '{sensor['id']}' not found in the data sources."
                           "This sensor won't be add to associations.")
            continue

        data_sources_gateways_association = {
            'gateway_id': new_gateways[sensor['gateway_name']]['id'],
            'data_source_id': sensor['id'],
            'address': sensor['port'],
            'created_at': datetime.utcnow()
        }
        data_sources_gateways_associations.append(data_sources_gateways_association)

    for new_gateway in new_gateways.values():
        connection.execute(insert(new_gateways_table).values(new_gateway))

    if data_sources_gateways_associations:
        connection.execute(insert(data_sources_gateways_association_table).values(data_sources_gateways_associations))


def downgrade(schema):
    raise Exception('Irreversible migration')
