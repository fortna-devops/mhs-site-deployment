"""add_alarms_history

Revision ID: c3b47208becc
Revises: 0cc1213be35e
Create Date: 2020-02-11 13:47:03.131661

"""
import logging

from alembic import op
import sqlalchemy as sa
from sqlalchemy import Table, MetaData
from sqlalchemy.dialects.postgresql import ENUM, insert


logger = logging.getLogger()


# revision identifiers, used by Alembic.
revision = 'c3b47208becc'
down_revision = '9ee7242f9312'
branch_labels = None
depends_on = None


ALARMS_QUERY_BATCH_SIZE = 1000


def upgrade(schema):
    # rename alarms table to alarms_tmp table
    op.rename_table('alarms', 'alarms_tmp')

    # get alarms_tmp table definition
    connection = op.get_bind()
    meta = MetaData(bind=connection, schema=schema)
    meta.reflect(only=('alarms_tmp', 'acknowledgments'))
    alarms_tmp_table = Table('alarms_tmp', meta)
    acknowledgments_tmp_table = Table('acknowledgments', meta)

    # define existing enums
    types_enum = ENUM(name='alarms_type_enum', create_type=False, metadata=meta)
    metrics_enum = ENUM(name='alarms_metric_enum', create_type=False, metadata=meta)
    levels_enum = ENUM(name='levels_enum', create_type=False, metadata=meta)

    # create alarms table
    alarms_table = op.create_table(
        'alarms',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('data_timestamp', sa.DateTime(), nullable=False),
        sa.Column('severity', sa.Integer(), nullable=False),
        sa.Column('type', types_enum, nullable=True),
        sa.Column('metric', metrics_enum, nullable=True),
        sa.Column('level', levels_enum, nullable=False),
        sa.Column('value', sa.Float(), nullable=False),
        sa.Column('criteria', sa.Float(), nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id'),
    )
    op.create_index('alarms_sensor_id_type_metric_level_index', 'alarms', ['sensor_id', 'type', 'metric', 'level'], unique=True)
    op.execute('ALTER TABLE alarms OWNER to mhs')

    # create alarms_history table
    alarms_history_table = op.create_table(
        'alarms_history',
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('alarm_id', sa.Integer(), nullable=False),
        sa.Column('data_timestamp', sa.DateTime(), nullable=False),
        sa.Column('severity', sa.Integer(), nullable=False),
        sa.Column('value', sa.Float(), nullable=False),
        sa.Column('criteria', sa.Float(), nullable=False),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['alarm_id'], ['alarms.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('alarm_id', 'data_timestamp', name='alarms_history_alarm_id_data_timestamp_key')
    )
    op.execute('ALTER TABLE alarms_history OWNER to mhs')

    # migrate data
    cur = connection.execute(alarms_tmp_table.select().order_by(alarms_tmp_table.c.created_at.asc()))
    counter = cur.rowcount
    keys = cur.keys()
    logger.info(f'Total number of alarms: {cur.rowcount}')
    while counter > 0:
        rows = cur.fetchmany(size=ALARMS_QUERY_BATCH_SIZE)

        for row in rows:
            data = dict(zip(keys, row))
            data['updated_at'] = data['created_at']

            insert_statement = insert(alarms_table)\
                .values(data)\
                .on_conflict_do_update(
                index_elements=['sensor_id', 'type', 'metric', 'level'],
                    set_={
                        'data_timestamp': data['data_timestamp'],
                        'severity': data['severity'],
                        'value': data['value'],
                        'criteria': data['criteria'],
                        'description': data['description'],
                        'updated_at': data['updated_at']
                    },
                    where=(alarms_table.c.updated_at < data['updated_at'])
                )\
                .returning(alarms_table.c.id)
            result = list(connection.execute(insert_statement))
            if not result:
                continue

            history_data = {
                'alarm_id': result[0]['id'],
                'data_timestamp': data['data_timestamp'],
                'severity': data['severity'],
                'value': data['value'],
                'criteria': data['criteria'],
                'created_at': data['created_at']
            }

            insert_statement = insert(alarms_history_table)\
                .values(history_data)\
                .on_conflict_do_update(
                    constraint='alarms_history_alarm_id_data_timestamp_key',
                    set_={
                        'severity': history_data['severity'],
                        'value': history_data['value'],
                        'criteria': history_data['criteria']
                    }
                )
            connection.execute(insert_statement)

        counter -= len(rows)
        logger.info(f'Processed alarms: {cur.rowcount-counter}/{cur.rowcount}')

    cur.close()
    op.drop_constraint('acknowledgments_alarm_id_fkey', 'acknowledgments', type_='foreignkey')
    op.drop_table('alarms_tmp')
    op.create_foreign_key('acknowledgments_alarm_id_fkey', 'acknowledgments', 'alarms', ['alarm_id'], ['id'])


def downgrade(schema):
    raise Exception("Irreversible migration")
