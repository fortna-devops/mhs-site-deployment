"""create_vfd_predicted_tables

Revision ID: c71e98343ee9
Revises: aa8b140369d8
Create Date: 2020-08-22 13:09:14.361721

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c71e98343ee9'
down_revision = 'aa8b140369d8'
branch_labels = None
depends_on = None


def upgrade(schema):
    # Create predicted_vfd_data table.
    op.create_table('predicted_vfd_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('speed', sa.REAL(), nullable=True),
        sa.Column('speed_residuals', sa.REAL(), nullable=True),
        sa.Column('current', sa.REAL(), nullable=True),
        sa.Column('current_residuals', sa.REAL(), nullable=True),
        sa.Column('voltage', sa.REAL(), nullable=True),
        sa.Column('voltage_residuals', sa.REAL(), nullable=True),
        sa.Column('mechanical_power', sa.REAL(), nullable=True),
        sa.Column('mechanical_power_residuals', sa.REAL(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE predicted_vfd_data OWNER to mhs')
    op.create_index(
        'predicted_vfd_data_sensor_id_timestamp_desc_index',
        'predicted_vfd_data', ['sensor_id', sa.text('timestamp DESC')], unique=False
    )

    # Create predicted_vfd_temperature_data table.
    op.create_table('predicted_vfd_temperature_data',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('sensor_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('temperature', sa.REAL(), nullable=True),
        sa.Column('temperature_residuals', sa.REAL(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['sensor_id'], ['sensors.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('ALTER TABLE predicted_vfd_temperature_data OWNER to mhs')
    op.create_index(
        'predicted_vfd_temperature_data_sensor_id_timestamp_desc_index',
        'predicted_vfd_temperature_data', ['sensor_id', sa.text('timestamp DESC')], unique=False
    )


def downgrade(schema):
    op.drop_index(
        'predicted_vfd_temperature_data_sensor_id_timestamp_desc_index',
        table_name='predicted_vfd_temperature_data'
    )
    op.drop_index(
        'predicted_vfd_data_sensor_id_timestamp_desc_index',
        table_name='predicted_vfd_data'
    )
    op.drop_table('predicted_vfd_temperature_data')
    op.drop_table('predicted_vfd_data')
