"""add_data_index

Revision ID: dcffc830a0be
Revises: 5948e9bc0ec1
Create Date: 2020-01-28 14:20:04.446233

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'dcffc830a0be'
down_revision = '5948e9bc0ec1'
branch_labels = None
depends_on = None


def upgrade(schema):

    # Sensor data
    op.create_index('sensor_data_hour_sensor_id_timestamp_idx', 'sensor_data_hour', ['sensor_id', 'timestamp'], unique=False, postgresql_using='brin')
    op.create_index('sensor_data_minute_sensor_id_timestamp_idx', 'sensor_data_minute', ['sensor_id', 'timestamp'], unique=False, postgresql_using='brin')

    # Predicted data
    op.create_index('predicted_data_hour_sensor_id_timestamp_idx', 'predicted_data_hour', ['sensor_id', 'timestamp'], unique=False, postgresql_using='brin')
    op.create_index('predicted_data_minute_sensor_id_timestamp_idx', 'predicted_data_minute', ['sensor_id', 'timestamp'], unique=False, postgresql_using='brin')


def downgrade(schema):

    # Sensor data
    op.drop_index('sensor_data_minute_sensor_id_timestamp_idx', table_name='sensor_data_minute')
    op.drop_index('sensor_data_hour_sensor_id_timestamp_idx', table_name='sensor_data_hour')

    # Predicted data
    op.drop_index('predicted_data_minute_sensor_id_timestamp_idx', table_name='predicted_data_minute')
    op.drop_index('predicted_data_hour_sensor_id_timestamp_idx', table_name='predicted_data_hour')
