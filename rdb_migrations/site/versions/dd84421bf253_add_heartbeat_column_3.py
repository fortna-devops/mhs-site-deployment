"""add_heartbeat_column_3

Revision ID: dd84421bf253
Revises: 43b46370a419
Create Date: 2020-09-17 13:01:31.784877

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'dd84421bf253'
down_revision = '43b46370a419'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.add_column('new_gateways', sa.Column('heartbeat_timestamp', sa.DateTime(), nullable=True))


def downgrade(schema):
    op.drop_column('new_gateways', 'heartbeat_timestamp')
