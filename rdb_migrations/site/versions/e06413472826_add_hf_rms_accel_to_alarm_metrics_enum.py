"""add_hf_rms_accel_to_alarm_metrics_enum

Revision ID: e06413472826
Revises: dcffc830a0be
Create Date: 2020-02-04 13:31:13.390489

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e06413472826'
down_revision = 'dcffc830a0be'
branch_labels = None
depends_on = None


def upgrade(schema):
    with op.get_context().autocommit_block():
        op.execute("ALTER TYPE alarms_metric_enum ADD VALUE 'hf_rms_acceleration_x'")
        op.execute("ALTER TYPE alarms_metric_enum ADD VALUE 'hf_rms_acceleration_z'")


def downgrade(schema):
    raise Exception("Irreversible migration")
