"""read-heartbeat-status-from-new-gateway

Revision ID: eda79f0bbdce
Revises: 7ea9e5d4f92f
Create Date: 2020-09-22 09:31:53.423704

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'eda79f0bbdce'
down_revision = '7ea9e5d4f92f'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.add_column('new_gateways', sa.Column('last_message_status', sa.String(), nullable=True))
    op.add_column('new_gateways', sa.Column('last_message_timestamp', sa.DateTime(), nullable=True))
    op.alter_column('new_gateways', 'description', existing_type=sa.Text(), type_=sa.String())


def downgrade(schema):
    op.drop_column('new_gateways', 'last_message_timestamp')
    op.drop_column('new_gateways', 'last_message_status')
    op.alter_column('new_gateways', 'description', existing_type=sa.String(), type_=sa.Text())
