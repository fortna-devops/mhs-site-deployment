"""add_data_tables_indexes

Revision ID: f1fb38fa092c
Revises: eda79f0bbdce
Create Date: 2020-09-22 18:54:46.756892

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f1fb38fa092c'
down_revision = 'eda79f0bbdce'
branch_labels = None
depends_on = None


def upgrade(schema):
    # raw data index
    op.create_index('raw_float_data_metric_id_timestamp_minute_index', 'raw_float_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('raw_float_data_metric_id_timestamp_hour_index', 'raw_float_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    
    op.create_index('raw_int_data_metric_id_timestamp_minute_index', 'raw_int_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('raw_int_data_metric_id_timestamp_hour_index', 'raw_int_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    
    op.create_index('raw_string_data_metric_id_timestamp_minute_index', 'raw_string_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('raw_string_data_metric_id_timestamp_hour_index', 'raw_string_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    
    # calculated data index
    op.create_index('calculated_float_data_metric_id_timestamp_minute_index', 'calculated_float_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('calculated_float_data_metric_id_timestamp_hour_index', 'calculated_float_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    
    op.create_index('calculated_int_data_metric_id_timestamp_minute_index', 'calculated_int_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('calculated_int_data_metric_id_timestamp_hour_index', 'calculated_int_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    
    op.create_index('calculated_string_data_metric_id_timestamp_minute_index', 'calculated_string_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('calculated_string_data_metric_id_timestamp_hour_index', 'calculated_string_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])


def downgrade(schema):
    # raw data index
    op.drop_index('raw_float_data_metric_id_timestamp_minute_index', table_name='raw_float_data')
    op.drop_index('raw_float_data_metric_id_timestamp_hour_index', table_name='raw_float_data')
    
    op.drop_index('raw_int_data_metric_id_timestamp_minute_index', table_name='raw_int_data')
    op.drop_index('raw_int_data_metric_id_timestamp_hour_index', table_name='raw_int_data')
    
    op.drop_index('raw_string_data_metric_id_timestamp_minute_index', table_name='raw_string_data')
    op.drop_index('raw_string_data_metric_id_timestamp_hour_index', table_name='raw_string_data')
    
    # calculated data index
    op.drop_index('calculated_float_data_metric_id_timestamp_minute_index', table_name='calculated_float_data')
    op.drop_index('calculated_float_data_metric_id_timestamp_hour_index', table_name='calculated_float_data')
    
    op.drop_index('calculated_int_data_metric_id_timestamp_minute_index', table_name='calculated_int_data')
    op.drop_index('calculated_int_data_metric_id_timestamp_hour_index', table_name='calculated_int_data')
    
    op.drop_index('calculated_string_data_metric_id_timestamp_minute_index', table_name='calculated_string_data')
    op.drop_index('calculated_string_data_metric_id_timestamp_hour_index', table_name='calculated_string_data')
