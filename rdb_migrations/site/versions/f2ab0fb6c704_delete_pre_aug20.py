"""delete_pre_aug20

Revision ID: f2ab0fb6c704
Revises: 3cc7be257b86
Create Date: 2021-03-01 16:04:11.239333

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = 'f2ab0fb6c704'
down_revision = '3cc7be257b86'
branch_labels = None
depends_on = None


def upgrade(schema):
    op.drop_table('new_acknowledgments')
    op.drop_table('predicted_vfd_data_minute')
    op.drop_table('vfd_data')
    op.drop_table('predicted_data_minute')
    op.drop_table('plc_data_minute')
    op.drop_table('acknowledgments')
    op.drop_table('predicted_vfd_temp_data_hour')
    op.drop_table('plc_data_hour')
    op.drop_table('spare_parts_results')
    op.drop_table('robot_actions')
    op.drop_table('robot_jobs')
    op.drop_table('robot_missions')
    op.drop_table('stats')
    op.drop_table('predicted_data_hour')
    op.drop_table('asset_health')
    op.drop_table('ambient_env_data')
    op.drop_table('predicted_vfd_data_hour')
    op.drop_table('error_data')
    op.drop_table('predicted_vfd_temp_data_minute')
    op.drop_table('new_alarm_histories')
    op.drop_table('new_alarms')
    op.drop_table('sensor_data_hour')
    op.drop_table('alarms_history')
    op.drop_table('new_spare_parts_results')
    op.drop_table('vfd_lifetime_data')
    op.drop_table('vfd_temperature_data')
    op.drop_table('sensor_data_minute')
    op.drop_table('camera_data')
    op.drop_table('plc_data_dynamic')

    op.drop_column('alarm_types', 'to_display')

    op.drop_table('alert_black_list')
    op.rename_table('new_alert_black_list', 'alert_black_list')

    op.drop_table('thresholds')
    op.rename_table('new_thresholds', 'thresholds')

    op.drop_table('alarms')
    op.rename_table('new_new_alarms', 'alarms')

    op.drop_table('sensors')
    op.drop_table('conveyor_statuses')
    op.drop_table('mode_transition_detections')
    op.drop_table('conveyors')

    op.drop_table('gateways')
    op.rename_table('new_gateways', 'gateways')

    op.execute('DROP TYPE action_type_enum')
    op.execute('DROP TYPE alarms_metric_enum')
    op.execute('DROP TYPE alarms_type_enum')
    op.execute('DROP TYPE conveyor_statuses_enum')
    op.execute('DROP TYPE conveyor_types_enum')
    op.execute('DROP TYPE criticality_enum')
    op.execute('DROP TYPE equipment_enum')
    op.execute('DROP TYPE job_state_enum')
    op.execute('DROP TYPE levels_enum')
    op.execute('DROP TYPE sensors_statuses_enum')
    op.execute('DROP TYPE sensors_type_enum')
    op.execute('DROP TYPE spare_parts_confidence_level_enum')
    op.execute('DROP TYPE thresholds_metric_enum')
    op.execute('DROP TYPE thresholds_type_enum')


def downgrade(schema):
    raise Exception("Irreversible migration")
