import os
import time
import logging

from .base import BaseStep


logger = logging.getLogger()


class DBError(Exception):
    pass


class CreateS3Bucket(BaseStep):

    def forwards(self):
        logger.info('Create S3 bucket')
        client = self.action.get_client('s3')
        bucket_name = self.action.config.get('general', 'bucket_name')

        client.create_bucket(
            ACL='private',
            Bucket=bucket_name,
        )

        client.put_bucket_encryption(
            Bucket=bucket_name,
            ServerSideEncryptionConfiguration=self.action.config.get(
                's3', 'encryption')
        )

    def backwards(self):
        logger.info('Remove s3 bucket')
        resource = self.action.boto_session.resource('s3')
        bucket_name = self.action.config.get('general', 'bucket_name')

        bucket = resource.Bucket(bucket_name)
        bucket.objects.all().delete()
        bucket.delete()


class CreateDB(BaseStep):

    sql_dir = 'templates/sql'
    queries = [
        'create_db.sql',
        'sensor.sql',
        'plc.sql',
        'error.sql',
        'sensor_hour.sql',
        'sensor_minute.sql',
        'ambient_env.sql',
        'location.sql',
        'stats.sql',
        'timeseries_predictions.sql',
        'timeseries_predictions_hour.sql',
        'alarms.sql',
        'alert_users.sql',
        'alert_users_data.sql',
        'conveyor_status.sql',
        'mode_transition_detection.sql',
        'acknowledgements.sql'
    ]

    def __init__(self, action):
        super(CreateDB, self).__init__(action)
        self._athena_client = self.action.get_client('athena')
        bucket_name = self.action.config.get('general', 'bucket_name')
        self._result_config = {
            'OutputLocation': 's3://{}/logs/'.format(bucket_name)
        }

    def wait_for_result(self, query_exec_id, wait_timeout=.1, max_retries=5):
        retry = 0

        while retry < max_retries:
            time.sleep(wait_timeout)
            res = self._athena_client.get_query_execution(
                QueryExecutionId=query_exec_id
            )
            state = res['QueryExecution']['Status']['State']
            if state == 'SUCCEEDED':
                break

            if state == 'FAILED':
                reason = res['QueryExecution']['Status']['StateChangeReason']
                msg = 'Query status: {}; Reason: {}'.format(state, reason)
                raise DBError(msg)

            if state == 'CANCELLED':
                raise DBError('Query has been canceled')

            retry += 1

    def run_query(self, query_string, wait_for_result=True):
        query = self._athena_client.start_query_execution(
            QueryString=query_string,
            ResultConfiguration=self._result_config
        )

        if wait_for_result:
            self.wait_for_result(query['QueryExecutionId'])

    def get_template(self, name):
        path = os.path.join(self.sql_dir, name)
        with open(path) as f:
            template = f.read()

        return template

    def forwards(self):
        for q_name in self.queries:
            logger.info('Run query "%s"', q_name)
            template = self.get_template(q_name)
            query_string = template % self.action.config.get('general')
            self.run_query(query_string)

    def backwards(self):
        template = 'DROP DATABASE IF EXISTS `%(bucket_name)s` CASCADE'
        query_string = template % self.action.config.get('general')
        self.run_query(query_string)


class CreateS3Triggers(BaseStep):

    def forwards(self):
        logger.info('Create S3 triggers')
        bucket_name = self.action.config.get('general', 'bucket_name')
        resource = self.action.boto_session.resource('s3')
        bucket_notification = resource.BucketNotification(bucket_name)
        lambda_client = self.action.get_client('lambda')

        for perm in self.action.config.get('s3', 'lambda_permissions'):
            try:
                lambda_client.add_permission(**perm)
            except lambda_client.exceptions.ResourceConflictException:
                logger.warning(f"Permission for \"{perm['FunctionName']}\" already exists. Skip.")

        bucket_notification.put(
            NotificationConfiguration=self.action.config.get('s3', 'events')
        )
