from abc import ABC, abstractmethod
from utils import StopDeploy


__all__ = ['BaseStep']


class BaseStep(ABC):

    def __init__(self, action):
        self.action = action

    @abstractmethod
    def forwards(self):
        raise NotImplementedError

    def backwards(self):
        pass

    def stop(self, reason):
        raise StopDeploy(reason)
