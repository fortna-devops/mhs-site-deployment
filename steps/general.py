from .base import BaseStep


class ValidateCreation(BaseStep):

    def forwards(self):
        definitions = self.action.config.get('greengrass', 'definitions')
        if any(definitions.values()):
            self.stop('Definitions are already filled up for this site. '
                      'Creation step is already has been done.')

        gateway_name = self.action.config.get('general', 'gateway_name')
        gg_client = self.action.get_client('greengrass')
        for group in gg_client.list_groups()['Groups']:
            if group['Name'] == gateway_name:
                self.stop('Group with name "{}" already exists'.format(
                    gateway_name))

        bucket_name = self.action.config.get('general', 'bucket_name')
        s3_client = self.action.get_client('s3')
        for bucket in s3_client.list_buckets()['Buckets']:
            if bucket['Name'] == bucket_name:
                self.stop('Bucket with name "{}" already exists'.format(
                    bucket_name))
