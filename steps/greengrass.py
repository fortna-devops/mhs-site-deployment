import copy
import json
import os
import posixpath as path
import uuid
import logging
from abc import abstractmethod

from contextlib import contextmanager

from fabric import Config, Connection

from .base import BaseStep


logger = logging.getLogger()


class GGError(Exception):
    pass


class CreateCert(BaseStep):

    def save_certs(self, cert):
        if not os.path.exists('certs'):
            os.makedirs('certs')
        site_name = self.action.config.get('general', 'site_name')
        with open(os.path.join('certs',  f'{site_name}_keys.json'), 'w') as f:
            json.dump(cert['keyPair'], f)

    def forwards(self):
        logger.info('Create certificate')
        iot_client = self.action.get_client('iot')
        cert = iot_client.create_keys_and_certificate(setAsActive=True)
        self.action.artifacts['gg_cert'] = cert

        # save for any reason we might need this (mostly for testing)
        self.save_certs(cert)

        cert_id = cert['certificateId'][:10]
        self.action.config.set('greengrass', 'gateway_hardware', 'ggc_config',
                               'coreThing', 'certPath',
                               value=f'{cert_id}.cert.pem')
        self.action.config.set('greengrass', 'gateway_hardware', 'ggc_config',
                               'coreThing', 'keyPath',
                               value=f'{cert_id}.private.key')
        self.action.config.set('greengrass', 'gateway_hardware', 'cert_id',
                               value=cert_id)

    def backwards(self):
        logger.info('Remove certificate')
        iot_client = self.action.get_client('iot')

        cert_id = self.action.artifacts['gg_cert']['certificateId']

        iot_client.update_certificate(certificateId=cert_id,
                                      newStatus='INACTIVE')
        iot_client.delete_certificate(certificateId=cert_id, forceDelete=True)


class CreatePolicy(BaseStep):

    def __init__(self, *args, **kwargs):
        super(CreatePolicy, self).__init__(*args, **kwargs)
        _thing_name = self.action.config.get('iot', 'thing_name')
        self._policy_name = f'{_thing_name}-policy'

    def forwards(self):
        logger.info('Create and attach policy')
        iot_client = self.action.get_client('iot')
        iot_client.create_policy(
            policyName=self._policy_name,
            policyDocument=json.dumps(
                self.action.config.get('iot', 'cert_policy')
            )
        )

        iot_client.attach_policy(
            policyName=self._policy_name,
            target=self.action.artifacts['gg_cert']['certificateArn']
        )

    def backwards(self):
        logger.info('Remove policy')
        iot_client = self.action.get_client('iot')
        try:
            iot_client.detach_policy(
                policyName=self._policy_name,
                target=self.action.artifacts['gg_cert']['certificateArn']
            )
        except iot_client.exceptions.InvalidRequestException:
            pass
        iot_client.delete_policy(policyName=self._policy_name)


class CreateThing(BaseStep):

    def __init__(self, *args, **kwargs):
        super(CreateThing, self).__init__(*args, **kwargs)
        self._thing_name = self.action.config.get('iot', 'thing_name')
        self._thing_type_name = 'EdgeGateway'
        self._policy_name = f'{self._thing_name}-policy'

    def forwards(self):
        logger.info('Create thing')
        iot_client = self.action.get_client('iot')
        thing = iot_client.create_thing(
            thingName=self._thing_name,
            thingTypeName=self._thing_type_name
        )

        iot_client.attach_thing_principal(
            thingName=self._thing_name,
            principal=self.action.artifacts['gg_cert']['certificateArn']
        )
        self.action.artifacts['gg_thing'] = thing
        self.action.config.set('greengrass', 'gateway_hardware', 'ggc_config',
                               'coreThing', 'thingArn', value=thing['thingArn'])

    def backwards(self):
        logger.info('Remove thing')
        iot_client = self.action.get_client('iot')
        iot_client.detach_thing_principal(
            thingName=self._thing_name,
            principal=self.action.artifacts['gg_cert']['certificateArn']
        )
        iot_client.delete_thing(thingName=self._thing_name)


class CreateCore(BaseStep):

    def forwards(self):
        logger.info('Create core')
        gateway_name = self.action.config.get('general', 'gateway_name')
        gg_client = self.action.get_client('greengrass')
        core_name =  f'{gateway_name}_Core'
        thing = self.action.artifacts['gg_thing']

        core_def = gg_client.create_core_definition(Name=core_name)
        self.action.config.set('greengrass', 'definitions', 'core_def_id', 
                              value=core_def['Id'])

        gg_client.update_connectivity_info(
            ConnectivityInfo=self.action.config.get('greengrass',
                                                   'thing_connectivity'),
            ThingName=thing['thingName']
        )

        cert_arn = self.action.artifacts['gg_cert']['certificateArn']
        core = gg_client.create_core_definition_version(
            CoreDefinitionId=core_def['Id'],
            Cores=[
                {
                    'CertificateArn': cert_arn,
                    'Id': str(uuid.uuid4()),
                    'SyncShadow': False,
                    'ThingArn': thing['thingArn']
                },
            ]
        )
        self.action.config.set('greengrass', 'definitions',
                              'core_def_version_arn', value=core['Arn'])

    def backwards(self):
        logger.info('Remove core')
        gg_client = self.action.get_client('greengrass')
        gg_client.delete_core_definition(
            CoreDefinitionId=self.action.config.get('greengrass', 'definitions',
                                                   'core_def_id')
        )


class CreateLoggerDef(BaseStep):

    def forwards(self):
        logger.info('Create logger definition')
        gg_client = self.action.get_client('greengrass')
        gateway_name = self.action.config.get('general', 'gateway_name')
        logger_def_name = f'{gateway_name}_gateway_logger'

        logger_def = gg_client.create_logger_definition(
            Name=logger_def_name
        )
        self.action.config.set('greengrass', 'definitions', 'logger_def_id',
                              value=logger_def['Id'])

    def backwards(self):
        logger.info('Remove logger definition')
        gg_client = self.action.get_client('greengrass')
        gg_client.delete_logger_definition(
            LoggerDefinitionId=self.action.config.get(
                'greengrass', 'definitions', 'logger_def_id')
        )


class CreateLoggerVersion(BaseStep):

    def forwards(self):
        logger.info('Create logger version')
        gg_client = self.action.get_client('greengrass')

        logger_def_v = gg_client.create_logger_definition_version(
            LoggerDefinitionId=self.action.config.get(
                'greengrass', 'definitions', 'logger_def_id'),
            Loggers=self.action.config.get('greengrass', 'loggers')
        )
        self.action.artifacts['gg_loggers'] = logger_def_v


class CreateResourceDef(BaseStep):

    def forwards(self):
        logger.info('Create resource definition')
        gg_client = self.action.get_client('greengrass')
        gateway_name = self.action.config.get('general', 'gateway_name')
        resource_def_name = f'{gateway_name}_gateway_resources'

        resource_def = gg_client.create_resource_definition(
            Name=resource_def_name
        )
        self.action.config.set('greengrass', 'definitions', 'resource_def_id',
                              value=resource_def['Id'])

    def backwards(self):
        logger.info('Remove resource definition')
        gg_client = self.action.get_client('greengrass')
        gg_client.delete_resource_definition(
            ResourceDefinitionId=self.action.config.get(
                'greengrass', 'definitions', 'resource_def_id')
        )


class CreateResourceVersion(BaseStep):

    def forwards(self):
        logger.info('Create resource version')
        gg_client = self.action.get_client('greengrass')

        resource_def_v = gg_client.create_resource_definition_version(
            ResourceDefinitionId=self.action.config.get(
                'greengrass', 'definitions', 'resource_def_id'),
            Resources=self.action.config.get('greengrass', 'resources')
        )
        self.action.artifacts['gg_resources'] = resource_def_v


class CreateSubscriptionDef(BaseStep):

    def forwards(self):
        logger.info('Create subscription definition')
        gateway_name = self.action.config.get('general', 'gateway_name')
        gg_client = self.action.get_client('greengrass')
        subscription_def_name = f'{gateway_name}_subscriptions'

        subscription_def = gg_client.create_subscription_definition(
            Name=subscription_def_name
        )
        self.action.config.set('greengrass', 'definitions',
                               'subscription_def_id',
                               value=subscription_def['Id'])

    def backwards(self):
        logger.info('Remove subscription definition')
        gg_client = self.action.get_client('greengrass')
        gg_client.delete_subscription_definition(
            SubscriptionDefinitionId=self.action.config.get(
                'greengrass', 'definitions', 'subscription_def_id')
        )


class CreateSubscriptionVersion(BaseStep):

    def forwards(self):
        logger.info('Create subscription version')
        gg_client = self.action.get_client('greengrass')

        subscription_def_v = gg_client.create_subscription_definition_version(
            SubscriptionDefinitionId=self.action.config.get(
                'greengrass', 'definitions', 'subscription_def_id'),
            Subscriptions=self.action.config.get('greengrass', 'subscriptions')
        )
        self.action.artifacts['gg_subscriptions'] = subscription_def_v


class CreateFunctionDef(BaseStep):

    def forwards(self):
        logger.info('Create function definition')
        gateway_name = self.action.config.get('general', 'gateway_name')
        gg_client = self.action.get_client('greengrass')
        function_def_name = f'{gateway_name}_functions'

        function_def = gg_client.create_function_definition(
            Name=function_def_name
        )
        self.action.config.set('greengrass', 'definitions', 'function_def_id',
                              value=function_def['Id'])

    def backwards(self):
        logger.info('Remove function definition')
        gg_client = self.action.get_client('greengrass')
        gg_client.delete_function_definition(
            FunctionDefinitionId=self.action.config.get(
                'greengrass', 'definitions', 'function_def_id')
        )


class CreateFunctionVersion(BaseStep):

    def _prepare_functions(self):
        # prepare variables
        functions = copy.deepcopy(self.action.config.get('greengrass', 'functions'))
        for func in functions:
            for name, value in func['FunctionConfiguration']['Environment']['Variables'].items():
                if isinstance(value, str):
                    continue
                func['FunctionConfiguration']['Environment']['Variables'][name] = json.dumps(value)
        return functions

    def forwards(self):
        logger.info('Create function version')
        gg_client = self.action.get_client('greengrass')

        function_def_v = gg_client.create_function_definition_version(
            FunctionDefinitionId=self.action.config.get(
                'greengrass', 'definitions', 'function_def_id'),
            Functions=self._prepare_functions()
        )
        self.action.artifacts['gg_functions'] = function_def_v


class CreateGroup(BaseStep):

    def forwards(self):
        logger.info('Create group')
        gg_client = self.action.get_client('greengrass')
        group_name = self.action.config.get('general', 'gateway_name')

        group = gg_client.create_group(Name=group_name)
        self.action.config.set('greengrass', 'definitions', 'group_id',
                              value=group['Id'])

    def backwards(self):
        logger.info('Remove group definition')
        gg_client = self.action.get_client('greengrass')
        gg_client.delete_group(
            GroupId=self.action.config.get(
                'greengrass', 'definitions', 'group_id')
        )


class CreateGroupVersion(BaseStep):

    def forwards(self):
        logger.info('Create group version')
        gg_client = self.action.get_client('greengrass')

        logger_arn = self.action.artifacts['gg_loggers']['Arn']
        resource_arn = self.action.artifacts['gg_resources']['Arn']
        subscription_arn = self.action.artifacts['gg_subscriptions']['Arn']
        function_arn = self.action.artifacts['gg_functions']['Arn']

        group_v = gg_client.create_group_version(
            GroupId=self.action.config.get(
                'greengrass', 'definitions', 'group_id'),
            CoreDefinitionVersionArn=self.action.config.get(
                'greengrass', 'definitions', 'core_def_version_arn'),
            LoggerDefinitionVersionArn=logger_arn,
            ResourceDefinitionVersionArn=resource_arn,
            SubscriptionDefinitionVersionArn=subscription_arn,
            FunctionDefinitionVersionArn=function_arn
        )

        self.action.artifacts['gg_group_version_id'] = group_v['Version']


class AssignRole(BaseStep):

    def forwards(self):
        logger.info('Associate role to group')
        gg_client = self.action.get_client('greengrass')
        gg_client.associate_role_to_group(
            GroupId=self.action.config.get(
                'greengrass', 'definitions', 'group_id'),
            RoleArn=self.action.config.get('greengrass', 'role_arn')
        )


class BaseSSHStep(BaseStep):

    @contextmanager
    def get_ssh_client(self):
        conn_config = self.action.config.get('greengrass', 'gateway_hardware',
                                             'connection')

        connect_kwargs = {
            'password': conn_config['pwd'],
            'key_filename': conn_config['key_path']
        }
        config = Config(overrides={'sudo': {'password': conn_config['pwd']}})
        client = Connection(host=conn_config['host'], user=conn_config['user'],
                            port=conn_config['port'], config=config,
                            connect_kwargs=connect_kwargs)
        try:
            yield client
        finally:
            client.close()


class PrepareGGC(BaseSSHStep):

    def forwards(self):
        logger.info('Upload certificates to gateway')
        gateway_config = self.action.config.get('greengrass', 'gateway_hardware')
        ggc_path = gateway_config['ggc_path']
        certs = self.action.artifacts['gg_cert']
        cert_id = certs['certificateId'][:10]

        with self.get_ssh_client() as client:
            sftp = client.sftp()

            # define necessary paths
            tmp_dir = path.join(path.sep, 'tmp', 'mhs-ci-tmp')
            tmp_config_dir = path.join(tmp_dir, 'config')
            tmp_certs_dir = path.join(tmp_dir, 'certs')

            ggc_config = path.join(ggc_path, 'config')
            ggc_cert = path.join(ggc_path, 'certs')

            config_wildcard = path.join(tmp_config_dir, '*')
            certs_wildcard = path.join(tmp_certs_dir, '*')

            # cleanup
            client.run(f'rm -rf "{tmp_dir}"')

            # create temp dir
            client.run(f'mkdir -p {tmp_config_dir}')
            client.run(f'mkdir -p {tmp_certs_dir}')

            # write config
            config_remote_path = path.join(tmp_config_dir, 'config.json')
            with sftp.open(config_remote_path, 'w') as f:
                json.dump(gateway_config['ggc_config'], f, indent=2)

            # write cert
            cert_remote_path = path.join(tmp_certs_dir, f'{cert_id}.cert.pem')
            with sftp.open(cert_remote_path, 'w') as f:
                f.write(certs['certificatePem'])

            # write private key
            key_remote_path = path.join(tmp_certs_dir, f'{cert_id}.private.key')
            with sftp.open(key_remote_path, 'w') as f:
                f.write(certs['keyPair']['PrivateKey'])

            # write public key
            key_remote_path = path.join(tmp_certs_dir, f'{cert_id}.public.key')
            with sftp.open(key_remote_path, 'w') as f:
                f.write(certs['keyPair']['PublicKey'])

            # copy files to ggc
            client.sudo(f'cp {config_wildcard} {ggc_config}', hide='stderr')
            client.sudo(f'cp {certs_wildcard} {ggc_cert}', hide='stderr')

            client.sudo(f'chown -R ggc_user:ggc_group {ggc_cert}', hide='stderr')

            # remove temp dir
            client.run(f'rm -rf "{tmp_dir}"')


class CreateDeploy(BaseStep):
    
    def forwards(self):
        logger.info('Create deployment')
        gg_client = self.action.get_client('greengrass')
        gg_client.create_deployment(
            DeploymentType='NewDeployment',
            GroupVersionId=self.action.artifacts['gg_group_version_id'],
            GroupId=self.action.config.get(
                'greengrass', 'definitions', 'group_id')
        )


class BaseSystemdStep(BaseSSHStep):

    start_action = 'start'
    stop_action = 'stop'
    restart_action = 'restart'
    actions = (start_action, stop_action, restart_action)

    @property
    @abstractmethod
    def service_name(self):
        return ''

    def service_action(self, action):
        with self.get_ssh_client() as client:
            if action not in self.actions:
                raise GGError(f'Unsupported ggc action "{action}"')

            client.sudo(f'service {self.service_name} {action}', hide='stderr')


class BaseServiceStartStep(BaseSystemdStep):

    def forwards(self):
        logger.info(f'Start {self.service_name} core')
        self.service_action(self.restart_action)

    def backwards(self):
        logger.info(f'Stop {self.service_name} core')
        self.service_action(self.stop_action)


class StartGGC(BaseServiceStartStep):
    service_name = 'greengrass'


class StartOTA(BaseServiceStartStep):
    service_name = 'greengrass_ota'


class BaseUpdate(BaseStep):
    _access_role = 'arn:aws:iam::286214959794:role/GreengrassOTAUpdate'
    _log_level = 'INFO'
    _arch = 'x86_64'
    _os = 'ubuntu'

    @property
    @abstractmethod
    def _software_to_update(self):
        return ''

    def forwards(self):
        logger.info(f'Updating greengrass {self._software_to_update}')
        gg_client = self.action.get_client('greengrass')
        target = self.action.config.get('greengrass', 'gateway_hardware',
                                        'ggc_config', 'coreThing', 'thingArn')
        gg_client.create_software_update_job(
            S3UrlSignerRole=self._access_role,
            SoftwareToUpdate=self._software_to_update,
            UpdateAgentLogLevel=self._log_level,
            UpdateTargets=[target],
            UpdateTargetsArchitecture=self._arch,
            UpdateTargetsOperatingSystem=self._os
        )


class UpdateGGC(BaseUpdate):
    _software_to_update = 'core'


class UpdateAgent(BaseUpdate):
    _software_to_update = 'ota_agent'
