import logging
import os
from abc import ABC, abstractmethod
from contextlib import contextmanager

from alembic.config import Config as AlembicConfig
from alembic import command as alembic_command


from mhs_rdb import Connector

import rdb_migrations
from .base import BaseStep


logger = logging.getLogger()


__all__ = ['CreateDBStep', 'CreateSiteStep', 'MigrateSiteStep', 'MigrateCustomerStep']


MHS_ROLE_NAME = 'mhs'
TABLEAU_ROLE_NAME = 'tableau'
CUSTOMER_SCHEMA_NAME = 'customer'


class BaseRDBStep(BaseStep):

    def __init__(self, action):
        super(BaseRDBStep, self).__init__(action=action)
        db_config = self.action.config.get('database')
        self._connector = Connector(boto_session=self.action.boto_session, **db_config)

    @contextmanager
    def get_connection(self, database):
        engine = self._connector.get_engine(database)
        with engine.connect() as conn:
            conn.execute('COMMIT')
            yield conn


class CreateDBStep(BaseRDBStep):

    def check_db_exists(self, conn, database_name):
        return bool(conn.scalar(f"SELECT 1 FROM pg_database WHERE datname = \'{database_name}\'"))

    def check_schema_exists(self, conn, schema_name):
        return bool(conn.scalar(f"SELECT 1 FROM information_schema.schemata WHERE schema_name = \'{schema_name}\'"))

    def forwards(self):
        logger.info('Create database')
        customer_name = self.action.config.get('general', 'customer_name')
        with self.get_connection('postgres') as conn:
            if not self.check_db_exists(conn, customer_name):
                logger.info(f'Creating database for customer {customer_name}')
                conn.execute(f'CREATE DATABASE {customer_name} WITH OWNER = {MHS_ROLE_NAME}')
                conn.execute(f'GRANT CONNECT ON DATABASE {customer_name} TO {TABLEAU_ROLE_NAME};')
            else:
                logger.info(f'Database with name {customer_name} already exists')

        with self.get_connection(customer_name) as conn:
            if not self.check_schema_exists(conn, 'customer'):
                logger.info(f'Creating "customer" schema for customer {customer_name}')
                conn.execute(f"""
                    ALTER DEFAULT PRIVILEGES GRANT USAGE ON TYPES TO {TABLEAU_ROLE_NAME};
                    ALTER DEFAULT PRIVILEGES GRANT SELECT ON TABLES TO {TABLEAU_ROLE_NAME};
                """)
                conn.execute(f'CREATE SCHEMA {CUSTOMER_SCHEMA_NAME} AUTHORIZATION {MHS_ROLE_NAME};')
                conn.execute(f'GRANT USAGE ON SCHEMA {CUSTOMER_SCHEMA_NAME} TO {TABLEAU_ROLE_NAME};')
            else:
                logger.info(f'Customer schema already exists')


class CreateSiteStep(BaseRDBStep):

    def forwards(self):
        logger.info('Create site')
        customer_name = self.action.config.get('general', 'customer_name')
        schema = self.action.config.get('database', 'schema')
        with self.get_connection(customer_name) as conn:
            conn.execute(f'CREATE SCHEMA {schema} AUTHORIZATION {MHS_ROLE_NAME};')
            conn.execute(f'GRANT USAGE ON SCHEMA {schema} TO {TABLEAU_ROLE_NAME};')

    def backwards(self):
        customer_name = self.action.config.get('general', 'customer_name')
        schema = self.action.config.get('database', 'schema')
        with self.get_connection(customer_name) as conn:
            conn.execute(f'DROP SCHEMA {schema}')


class BaseAlembicStep(BaseStep, ABC):

    @property
    @abstractmethod
    def template_name(self):
        return ''

    @abstractmethod
    def get_schema(self):
        return ''
    
    @contextmanager
    def alembic_config(self):
        config = AlembicConfig()

        script_location = os.path.join(os.path.dirname(rdb_migrations.__file__), self.template_name)

        if self.action.aws_profile:
            config.set_main_option('profile_name', self.action.aws_profile)
        config.set_main_option('script_location', script_location)
        config.set_main_option('username', self.action.config.get('database', 'username'))
        config.set_main_option('host', self.action.config.get('database', 'host'))
        config.set_main_option('port', self.action.config.get('database', 'port'))
        config.set_main_option('database', self.action.config.get('general', 'customer_name'))
        config.set_main_option('schema', self.get_schema())
        config.set_main_option('template', self.template_name)
        yield config


class BaseMigrationStep(BaseAlembicStep):
    revision = 'head'

    def forwards(self):
        logger.info(f'Migrate {self.template_name}')
        with self.alembic_config() as config:
            alembic_command.upgrade(config, self.revision)


class MigrateSiteStep(BaseMigrationStep):
    template_name = 'site'

    def get_schema(self):
        return self.action.config.get('database', 'schema')


class MigrateCustomerStep(BaseMigrationStep):
    template_name = 'customer'

    def get_schema(self):
        return CUSTOMER_SCHEMA_NAME
