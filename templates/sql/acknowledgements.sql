CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`acknowledgements` (
    `sensor` STRING,
    `conveyor` STRING,
    `equipment` STRING,
    `severity` INT,
    `type` STRING,
    `key` STRING,
    `value` FLOAT,
    `criteria` STRING,
    `alarm_timestamp` TIMESTAMP,
    `alarm_description` STRING,
    `work_order_lead` STRING,
    `work_order` STRING,
    `downtime_schedule` STRING,
    `timestamp` TIMESTAMP
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES ('separatorChar' = ',', 'quoteChar' = '`', 'skip.header.line.count' = '1')
LOCATION 's3://%(bucket_name)s/acknowledgements/'
TBLPROPERTIES ('has_encrypted_data'='false');
