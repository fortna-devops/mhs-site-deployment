CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`alarms` (
    `sensor` STRING,
    `conveyor` STRING,
    `equipment` STRING,
    `severity` INT,
    `type` STRING,
    `key` STRING,
    `value` FLOAT,
    `criteria` STRING,
    `data_timestamp` TIMESTAMP,
    `timestamp` TIMESTAMP
)
PARTITIONED BY (`date` DATE)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES ('separatorChar' = ',', 'quoteChar' = '`', 'skip.header.line.count' = '1')
LOCATION 's3://%(bucket_name)s/alarms/'
TBLPROPERTIES ('has_encrypted_data'='false');
