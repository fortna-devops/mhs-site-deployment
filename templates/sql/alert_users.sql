CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`alert_users` (
    email STRING,
    phone STRING,
    active BOOLEAN,
    roles STRING
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
    'separatorChar' = ',',
    'quoteChar' = '`',
    'skip.header.line.count' = '1'
)
LOCATION 's3://%(bucket_name)s/alert_users/'
TBLPROPERTIES ('has_encrypted_data'='false');
