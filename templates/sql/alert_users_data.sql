CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`alert_users_data` (
    email STRING,
    sensor_black_list STRING
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
    'separatorChar' = ',',
    'quoteChar' = '`',
    'skip.header.line.count' = '1'
)
LOCATION 's3://%(bucket_name)s/alert_users_data/'
TBLPROPERTIES ('has_encrypted_data'='false');
