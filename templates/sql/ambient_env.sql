CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`ambient_env` (
  `timestamp` TIMESTAMP,
  `gateway` STRING,
  `port` STRING,
  `sensor` STRING,
  `humidity` DOUBLE,
  `temperature` DOUBLE
)
PARTITIONED BY (`date` DATE)
ROW FORMAT SERDE 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
WITH SERDEPROPERTIES ('serialization.format'='1')
LOCATION 's3://%(bucket_name)s/%(site_name)s/ambient_env/'
TBLPROPERTIES ('has_encrypted_data'='true');
