CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`conveyor_status` (
    timestamp TIMESTAMP,
    conveyor STRING, 
    status STRING
)
PARTITIONED BY (date DATE)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
    'separatorChar' = ',',
    'quoteChar' = '"',
    'skip.header.line.count' = '1'
)
LOCATION 's3://%(bucket_name)s/analytics_output/conveyor_status/' TBLPROPERTIES ('has_encrypted_data'='false')