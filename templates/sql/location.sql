CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`location` (
  `sensor` STRING,
  `description` STRING,
  `manufacturer` STRING,
  `conveyor` STRING,
  `location` STRING,
  `equipment` STRING,
  `m_sensor` STRING,
  `standards_score` INT,
  `temp_score` INT,
  `vvrms_score` INT,
  `thresholds` STRING,
  `facility_organization` STRING,
  `residual_thresholds` STRING,
  `alarms_confidence` STRING
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
    'separatorChar' = ',',
    'quoteChar' = '`',
    'skip.header.line.count' = '1'
)
LOCATION 's3://%(bucket_name)s/%(site_name)s/location/'
TBLPROPERTIES ('has_encrypted_data'='false');
