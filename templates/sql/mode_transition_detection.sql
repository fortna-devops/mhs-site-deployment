CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`mode_transition_detection` (
    timestamp TIMESTAMP,
    conveyor STRING, 
    num_of_transitions INT
)
PARTITIONED BY (date DATE)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
    'separatorChar' = ',',
    'quoteChar' = '"',
    'skip.header.line.count' = '1'
)
LOCATION 's3://%(bucket_name)s/analytics_output/mode_transition_detection/' TBLPROPERTIES ('has_encrypted_data'='false')