CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`plc` (
  `timestamp` TIMESTAMP,
  `gateway` STRING,
  `port` STRING,
  `sensor` STRING,
  `belt_speed` BIGINT,
  `vfd_current` DOUBLE,
  `vfd_frequency` DOUBLE,
  `vfd_voltage` DOUBLE,
  `vfd_fault_code` BIGINT,
  `sorter_induct_rate` BIGINT,
  `sorter_reject_rate` BIGINT,
  `status` STRING,
  `torque` BIGINT,
  `hours_running` BIGINT,
  `power` BIGINT,
  `rpm` BIGINT,
  `horsepower_nameplate` BIGINT,
  `voltage_nameplate` BIGINT,
  `run_speed_nameplate` BIGINT,
  `current_nameplate` DOUBLE,
  `num_phases_nameplate` BIGINT
)
PARTITIONED BY (`date` DATE)
ROW FORMAT SERDE 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
WITH SERDEPROPERTIES ('serialization.format'='1')
LOCATION 's3://%(bucket_name)s/%(site_name)s/plc/'
TBLPROPERTIES ('has_encrypted_data'='true');
