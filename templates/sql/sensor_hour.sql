CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`sensor_hour` (
  `timestamp` TIMESTAMP,
  `gateway` STRING,
  `port` STRING,
  `sensor` STRING,
  `rms_velocity_z` DOUBLE,
  `temperature` DOUBLE,
  `rms_velocity_x` DOUBLE,
  `peak_acceleration_z` DOUBLE,
  `peak_acceleration_x` DOUBLE,
  `peak_frequency_z` DOUBLE,
  `peak_frequency_x` DOUBLE,
  `rms_acceleration_z` DOUBLE,
  `rms_acceleration_x` DOUBLE,
  `kurtosis_z` DOUBLE,
  `kurtosis_x` DOUBLE,
  `crest_acceleration_z` DOUBLE,
  `crest_acceleration_x` DOUBLE,
  `peak_velocity_z` DOUBLE,
  `peak_velocity_x` DOUBLE,
  `hf_rms_acceleration_z` DOUBLE,
  `hf_rms_acceleration_x` DOUBLE
)
PARTITIONED BY (`date` DATE)
ROW FORMAT SERDE 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
WITH SERDEPROPERTIES ('serialization.format'='1')
LOCATION 's3://%(bucket_name)s/%(site_name)s/sensor-hour/'
TBLPROPERTIES ('has_encrypted_data'='true');
