CREATE EXTERNAL TABLE IF NOT EXISTS `%(bucket_name)s`.`timeseries_predictions_hour` (
  `timestamp` TIMESTAMP,
  `rms_velocity_x` DOUBLE,
  `rms_velocity_x_residuals` DOUBLE,
  `rms_velocity_z` DOUBLE,
  `rms_velocity_z_residuals` DOUBLE,
  `temperature` DOUBLE,
  `temperature_residuals` DOUBLE,
  `sensor` STRING,
  `conveyor` STRING,
  `equipment_type` STRING
)
PARTITIONED BY (`date` DATE)
ROW FORMAT SERDE 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
WITH SERDEPROPERTIES ('serialization.format'='1')
LOCATION 's3://%(bucket_name)s/analytics_output/timeseries_predictions_hour/'
TBLPROPERTIES ('has_encrypted_data'='true');
