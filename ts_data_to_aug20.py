import argparse
import logging
from collections import defaultdict
from datetime import datetime

import sqlalchemy as sa

from mhs_rdb import Connector

logging.basicConfig(
    level=logging.INFO,
    format='[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
)
logger = logging.getLogger()


DATA_QUERY_BATCH_SIZE = 10000
DATA_START_DATE = datetime(year=2020, month=10, day=17)
DATA_END_DATE = datetime(year=2020, month=11, day=17)


def migrate_data(connection, source_table, type_table_map, metrics_map, on_off_field=None, on_off_threshold=None):
    for sensor_id in metrics_map.keys():
        logger.info(f'Processing sensor_id={sensor_id}')
        data_count = sa.select([sa.func.count(source_table.c.id)])\
            .where(source_table.c.sensor_id == sensor_id)\
            .scalar()
        logger.info(f'Total number of {source_table} data points for sensor_id={sensor_id}: {data_count}')
        batches = range(0, data_count, DATA_QUERY_BATCH_SIZE)
        for i in batches:
            offset = i
            limit = DATA_QUERY_BATCH_SIZE
            data_query = sa.select([source_table])\
                .where(source_table.c.sensor_id == sensor_id and source_table.c.timestamp.between(DATA_START_DATE, DATA_END_DATE))\
                .offset(offset).limit(limit)

            cur = connection.execute(data_query)
            keys = cur.keys()

            data_to_insert = {
                float: [],
                int: [],
                str: [],
            }
            src_rows = cur.fetchall()
            for row in src_rows:
                data = dict(zip(keys, row))
                _id = data.pop('id')
                timestamp = data.pop('timestamp')
                created_at = data.pop('created_at')
                _sensor_id = data.pop('sensor_id')

                if on_off_field is None or on_off_threshold is None or data.get(on_off_field) is None:
                    flags = 0
                else:
                    flags = int(data[on_off_field] < on_off_threshold)

                for metric_name, value in data.items():
                    metric_id = metrics_map[sensor_id].get(metric_name)
                    if not metric_id:
                        continue

                    value_type = type(value)
                    if value_type not in data_to_insert:
                        continue

                    data_to_insert[value_type].append({
                        'metric_id': metric_id,
                        'timestamp': timestamp,
                        'created_at': created_at,
                        'value': value,
                        'flags': flags
                    })

            for value_type, rows in data_to_insert.items():
                if not rows:
                    continue
                dest_table = type_table_map[value_type]
                logger.info(f'Inserting {len(rows)} rows into {dest_table}')
                connection.execute(dest_table.insert(rows))

            cur.close()


def upgrade(connection, schema):
    connection.execute(f'SET search_path TO {schema}')
    meta = sa.MetaData(bind=connection, schema=schema)
    meta.reflect(only=(
        # service tables
        'metrics',
        'data_sources',

        # source tables
        'sensor_data_minute',
        'plc_data_minute',
        'vfd_data',
        'vfd_lifetime_data',
        'vfd_temperature_data',

        # dest tables
        'raw_float_data',
        'raw_int_data',
        'raw_string_data',
    ))
    metrics_table = sa.Table('metrics', meta)
    data_sources_table = sa.Table('data_sources', meta)

    sensor_data_minute_table = sa.Table('sensor_data_minute', meta)
    plc_data_minute_table = sa.Table('plc_data_minute', meta)
    vfd_data_table = sa.Table('vfd_data', meta)
    vfd_lifetime_data_table = sa.Table('vfd_lifetime_data', meta)
    vfd_temperature_data_table = sa.Table('vfd_temperature_data', meta)

    src_table_flags_map = (
        (sensor_data_minute_table, 'rms_acceleration_x', .02),
        (plc_data_minute_table, None, None),
        (vfd_data_table, 'speed', 0),
        (vfd_lifetime_data_table, None, None),
        (vfd_temperature_data_table, None, None),
    )

    type_dest_table_map = {
        float: sa.Table('raw_float_data', meta),
        int: sa.Table('raw_int_data', meta),
        str: sa.Table('raw_string_data', meta),
    }

    query = sa.select([
            metrics_table.c.id,
            metrics_table.c.name,
            data_sources_table.c.id,
        ])\
        .where(data_sources_table.c.id == metrics_table.c.data_source_id)\
        .order_by(data_sources_table.c.id.asc())

    cur = connection.execute(query)
    res = cur.fetchall()
    cur.close()

    metrics_map = defaultdict(dict)
    for metric_id, metric_name, data_source_id in res:
        metrics_map[data_source_id][metric_name] = metric_id

    tables = ('raw_float_data', 'raw_int_data', 'raw_string_data')
    indexes = ('minute', 'hour')

    # Remove indexes
    # It's possible to temporary disable indexing, but AWS RDS doesn't want us to do that.
    # Drop and recreate index is a fast an good enough solution for one time operation
    logger.info('Temporary removing indexes')
    for table in tables:
        for index in indexes:
            connection.execute(f'DROP INDEX IF EXISTS {table}_metric_id_timestamp_{index}_flags_index;')

    try:
        for source_table, on_off_field, on_off_threshold in src_table_flags_map:
            logger.info(f'Processing {source_table}')
            migrate_data(connection, source_table, type_dest_table_map, metrics_map, on_off_field, on_off_threshold)
    finally:
        # Recreate indexes
        logger.info('Recreating indexes')
        for table in tables:
            for index in indexes:
                connection.execute(f"""
                    CREATE INDEX {table}_metric_id_timestamp_{index}_flags_index
                    ON {table} USING btree
                    (metric_id ASC NULLS LAST, date_trunc('{index}'::text, "timestamp") ASC NULLS LAST, flags ASC NULLS LAST)
                    """)


HOST = 'mhspredict-prod.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
USERNAME = 'developer'


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('schema', help='Site schema name')
    args = parser.parse_args()

    schema = args.schema
    database = schema.split('_')[0]

    connector = Connector(host=HOST, username=USERNAME)
    engine = connector.get_engine(database)

    with engine.connect() as c:
        upgrade(c, schema)
