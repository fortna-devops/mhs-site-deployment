import argparse
import logging
from collections import defaultdict
from datetime import datetime, timedelta

import sqlalchemy as sa

from mhs_rdb import Connector

logging.basicConfig(
    level=logging.INFO,
    format='[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
)
logger = logging.getLogger()


DS2SENSOR_MAPPINGS = {
    'dhl_brescia': {1: 1, 3: 2, 5: 3, 7: 4, 9: 5, 11: 6, 13: 7, 15: 8, 17: 9, 19: 10, 21: 11, 23: 12, 25: 13, 27: 14, 29: 15, 31: 16, 33: 17, 35: 18, 37: 19, 39: 20, 41: 21, 43: 22, 45: 23, 47: 24, 49: 25, 51: 26, 53: 27, 55: 28, 57: 29, 59: 30, 61: 31, 63: 32, 65: 33, 67: 34, 69: 35, 71: 36, 73: 37, 75: 38, 77: 39, 79: 40, 81: 41, 83: 42, 85: 43, 87: 44, 89: 45, 91: 46, 93: 47, 95: 48, 97: 49, 99: 50, 101: 51, 103: 52, 105: 53, 107: 54, 109: 55, 111: 56, 113: 57, 115: 58, 117: 59, 119: 60, 121: 61, 123: 62, 125: 63, 127: 64, 129: 65, 131: 66, 133: 67, 135: 68, 137: 69, 139: 70, 141: 71, 143: 72, 145: 73, 147: 74, 149: 75, 151: 76, 153: 77, 155: 78, 157: 79, 159: 80, 161: 81, 163: 82, 165: 83, 167: 84, 169: 85, 171: 86, 173: 87, 175: 88, 177: 89, 179: 90, 181: 91, 183: 92, 185: 93, 187: 94, 189: 95, 191: 96, 2: 97, 4: 98, 6: 99, 8: 100, 10: 101, 12: 102, 14: 103, 16: 104, 18: 105, 20: 106, 22: 107, 24: 108, 26: 109, 28: 110, 30: 111, 32: 112, 34: 113, 36: 114, 38: 115, 40: 116, 42: 117, 44: 118, 46: 119, 48: 120, 50: 121, 52: 122, 54: 123, 56: 124, 58: 125, 60: 126, 62: 127, 64: 128, 66: 129, 68: 130, 70: 131, 72: 132, 74: 133, 76: 134, 78: 135, 80: 136, 82: 137, 84: 138, 86: 139, 88: 140, 90: 141, 92: 142, 94: 143, 96: 144, 98: 145, 100: 146, 102: 147, 104: 148, 106: 149, 108: 150, 110: 151, 112: 152, 114: 153, 116: 154, 118: 155, 120: 156, 122: 157, 124: 158, 126: 159, 128: 160, 130: 161, 132: 162, 134: 163, 136: 164, 138: 165, 140: 166, 142: 167, 144: 168, 146: 169, 148: 170, 150: 171, 152: 172, 154: 173, 156: 174, 158: 175, 160: 176, 162: 177, 164: 178, 166: 179, 168: 180, 170: 181, 172: 182, 174: 183, 176: 184, 178: 185, 180: 186, 182: 187, 184: 188, 186: 189, 188: 190, 190: 191, 192: 192}
}


DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
# DATA_START_DATE = datetime(year=2020, month=9, day=19)
DATA_START_DATE = datetime(year=2020, month=10, day=20)
DATA_END_DATE = datetime(year=2020, month=12, day=1, hour=23, minute=59)


def date_range(start: datetime, stop: datetime, step: timedelta):
    current = start
    next = start
    while next < stop:
        next = current + step

        if next > stop:
            next = stop

        yield current, next
        current = next


VT_METRICS = [
    ('rms_velocity_z', 'raw_float_data'),
    ('temperature', 'raw_float_data'),
    ('rms_velocity_x', 'raw_float_data'),
    ('peak_acceleration_z', 'raw_float_data'),
    ('peak_acceleration_x', 'raw_float_data'),
    ('peak_frequency_z', 'raw_float_data'),
    ('peak_frequency_x', 'raw_float_data'),
    ('rms_acceleration_z', 'raw_float_data'),
    ('rms_acceleration_x', 'raw_float_data'),
    ('kurtosis_z', 'raw_float_data'),
    ('kurtosis_x', 'raw_float_data'),
    ('crest_acceleration_z', 'raw_float_data'),
    ('crest_acceleration_x', 'raw_float_data'),
    ('peak_velocity_z', 'raw_float_data'),
    ('peak_velocity_x', 'raw_float_data'),
    ('hf_rms_acceleration_z', 'raw_float_data'),
    ('hf_rms_acceleration_x', 'raw_float_data'),
]

PLC_METRICS = [
    ('belt_speed', 'raw_int_data'),
    ('vfd_current', 'raw_int_data'),
    ('vfd_frequency', 'raw_int_data'),
    ('vfd_voltage', 'raw_int_data'),
    ('vfd_fault_code', 'raw_int_data'),
    ('sorter_induct_rate', 'raw_int_data'),
    ('sorter_reject_rate', 'raw_int_data'),
    ('status', 'raw_string_data'),
    ('torque', 'raw_int_data'),
    ('hours_running', 'raw_int_data'),
    ('power', 'raw_int_data'),
    ('rpm', 'raw_int_data'),
    ('horsepower_nameplate', 'raw_int_data'),
    ('voltage_nameplate', 'raw_int_data'),
    ('run_speed_nameplate', 'raw_int_data'),
    ('current_nameplate', 'raw_float_data'),
    ('num_phases_nameplate', 'raw_int_data'),
]

VFD_DATA_METRICS = [
    ('speed', 'raw_float_data'),
    ('current', 'raw_float_data'),
    ('voltage', 'raw_float_data'),
    ('mechanical_power', 'raw_float_data'),
    ('cos_phi', 'raw_float_data'),
]

VFD_LIFETIME_DATA_METRICS = [
    ('operating_time', 'raw_float_data'),
    ('running_time', 'raw_float_data'),
]

VFD_TEMPERATURE_DATA_METRICS = [
    ('temperature', 'raw_float_data'),
]


def migrate_data(connection, source_table, source_table_metrics, metrics_map, sensor_id_mapping, on_off_field=None, on_off_threshold=None):
    if on_off_field is None or on_off_threshold is None:
        flags = '0'
    else:
        flags = f'({on_off_field} < {on_off_threshold})::int'

    for start_date, end_date in date_range(DATA_START_DATE, DATA_END_DATE, timedelta(days=7)):
        for data_source_id in metrics_map.keys():
            logger.info(f'Processing data_source_id={data_source_id}, start={start_date}, end={end_date}')

            if sensor_id_mapping:
                sensor_id = sensor_id_mapping.get(data_source_id)
                if not sensor_id:
                    continue
            else:
                sensor_id = data_source_id

            for metric_name, dst_table in source_table_metrics:
                metric_id = metrics_map[data_source_id].get(metric_name)
                if not metric_id:
                    continue

                query = f"""
                INSERT INTO {dst_table} (metric_id, timestamp, value, created_at, flags)
                select
                    {metric_id} as metric_id,
                    timestamp as timestamp,
                    {metric_name} as value,
                    created_at as created_at,
                    {flags} as flags
                from {source_table.name}
                where sensor_id = {sensor_id} and
                timestamp >= '{start_date.strftime(DATE_FORMAT)}' and timestamp < '{end_date.strftime(DATE_FORMAT)}' and
                {metric_name} is not null
                """

                connection.execute(query)


def upgrade(connection, schema):
    connection.execute(f'SET search_path TO {schema}')
    meta = sa.MetaData(bind=connection, schema=schema)
    meta.reflect(only=(
        # service tables
        'metrics',
        'data_sources',

        # source tables
        'sensor_data_minute',
        'plc_data_minute',
        'vfd_data',
        'vfd_lifetime_data',
        'vfd_temperature_data',

        # dest tables
        'raw_float_data',
        'raw_int_data',
        'raw_string_data',
    ))
    metrics_table = sa.Table('metrics', meta)
    data_sources_table = sa.Table('data_sources', meta)

    sensor_data_minute_table = sa.Table('sensor_data_minute', meta)
    plc_data_minute_table = sa.Table('plc_data_minute', meta)
    vfd_data_table = sa.Table('vfd_data', meta)
    vfd_lifetime_data_table = sa.Table('vfd_lifetime_data', meta)
    vfd_temperature_data_table = sa.Table('vfd_temperature_data', meta)

    src_table_flags_map = (
        (sensor_data_minute_table, VT_METRICS, 'rms_acceleration_x', .02),
        (plc_data_minute_table, PLC_METRICS, None, None),
        (vfd_data_table, VFD_DATA_METRICS, 'speed', 0),
        (vfd_lifetime_data_table, VFD_LIFETIME_DATA_METRICS, None, None),
        (vfd_temperature_data_table, VFD_TEMPERATURE_DATA_METRICS, None, None),
    )

    query = sa.select([
            metrics_table.c.id,
            metrics_table.c.name,
            data_sources_table.c.id,
        ])\
        .where(data_sources_table.c.id == metrics_table.c.data_source_id)\
        .order_by(data_sources_table.c.id.asc())

    cur = connection.execute(query)
    res = cur.fetchall()
    cur.close()

    metrics_map = defaultdict(dict)
    for metric_id, metric_name, data_source_id in res:
        metrics_map[data_source_id][metric_name] = metric_id

    tables = ('raw_float_data', 'raw_int_data', 'raw_string_data')
    indexes = ('minute', 'hour')

    sensor_id_mapping = DS2SENSOR_MAPPINGS.get(schema, {})

    # Remove indexes
    # It's possible to temporary disable indexing, but AWS RDS doesn't want us to do that.
    # Drop and recreate index is a fast an good enough solution for one time operation
    # logger.info('Temporary removing indexes')
    for table in tables:
        for index in indexes:
            connection.execute(f'DROP INDEX IF EXISTS {table}_metric_id_timestamp_{index}_flags_index;')

    try:
        for source_table, metrics, on_off_field, on_off_threshold in src_table_flags_map:
            logger.info(f'Processing {source_table}')
            migrate_data(connection, source_table, metrics, metrics_map, sensor_id_mapping, on_off_field, on_off_threshold)
    finally:
        # Recreate indexes
        logger.info('Recreating indexes')
        for table in tables:
            for index in indexes:
                connection.execute(f"""
                    CREATE INDEX {table}_metric_id_timestamp_{index}_flags_index
                    ON {table} USING btree
                    (metric_id ASC NULLS LAST, date_trunc('{index}'::text, "timestamp") ASC NULLS LAST, flags ASC NULLS LAST)
                    """)


HOST = 'mhspredict-prod.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
USERNAME = 'developer'


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('schema', help='Site schema name')
    args = parser.parse_args()

    schema = args.schema
    database = schema.split('_')[0]

    connector = Connector(host=HOST, username=USERNAME)
    engine = connector.get_engine(database)

    with engine.connect() as c:
        upgrade(c, schema)
