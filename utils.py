import json
from operator import getitem
from functools import reduce


__all__ = ['Config', 'StopDeploy']


class Config(object):

    def __init__(self, path):
        self._path = path
        self.__config = {}

    def save(self):
        with open(self._path, 'w') as f:
            json.dump(self.__config, f, indent=2)

    @classmethod
    def load(cls, path):
        config = cls(path)
        config._load()
        return config

    def _load(self):
        with open(self._path) as f:
            self.__config = json.load(f)

    def get(self, *args):
        return reduce(getitem, args, self.__config)

    def set(self, *args, value):
        *chain, key = args
        dest = reduce(getitem, chain, self.__config)
        dest[key] = value

    def __getitem__(self, item):
        return self.__config.__getitem__(item)

    def __setitem__(self, key, value):
        return self.__config.__setitem__(key, value)


class StopDeploy(Exception):
    pass
